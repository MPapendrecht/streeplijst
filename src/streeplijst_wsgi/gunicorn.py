import subprocess

from streeplijst.configs.config import config
from streeplijst.util.debug import get_child_logger

logger = get_child_logger(__name__)


# flake8: noqa
def _check_imports():
    try:
        import gunicorn
        import uvicorn
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install gunicorn and uvicorn. pip install .[gunicorn]"
        )


def run(host: str, port: int):
    host = config().server.ip if host is None else host
    port = config().server.port if port is None else port
    _check_imports()
    logger.info(f"Running streeplijst using gunicorn on {host}:{port}.")
    subprocess.run(
        [
            "gunicorn",
            "streeplijst.server.app:app()",
            "-k",
            "uvicorn.workers.UvicornWorker",
            "--workers",
            "1",
            "--bind",
            f"{host}:{port}",
        ]
    )
