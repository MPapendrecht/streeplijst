import subprocess


# flake8: noqa
def _check_imports():
    try:
        import uvicorn
        import watchfiles
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install uvicorn and watchfiles. pip install .[uvicorn]"
        )


def run(host: str, port: int):
    _check_imports()
    host = "127.0.0.1" if host is None else host
    port = 8000 if port is None else port
    print(f"running uvicorn on {host}:{port}.")
    subprocess.run(
        [
            "uvicorn",
            "streeplijst.server.app:dev_app",
            "--reload",
            "--reload-include",
            "src",
            "--factory",
            "--workers",
            "1",
            "--host",
            f"{host}",
            "--port",
            f"{port}",
        ]
    )
