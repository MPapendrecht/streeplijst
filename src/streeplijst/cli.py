import shutil
import subprocess
import time
import webbrowser
from importlib.metadata import entry_points
from pathlib import Path

import click

from factmanagement.backup import backup as fm_backup
from factmanagement.upcast import upcast as fm_upcast
from streeplijst.configs.config import config, config_dir, init_config_file
from streeplijst.util import debug as dbg

COVFILE = Path(".coverage")
UNITCOVFILE = Path(".coverage_unit")
INTCOVFILE = Path(".coverage_int")
ENTRYPOINTS = entry_points()
WSGI_RUNNERS = {
    ep.name: ep for ep in set(entry_points().select(group="streeplijst.wsgi"))
}


def check_test_imports():
    try:
        import black
        import flake8
        import isort
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install the optional 'lint' dependencies. "
            "'pip install -e .[lint]'"
        )
    return black, flake8, isort


def check_rotate_imports():
    try:
        import rotate_backups
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install the optional 'backup' dependencies. "
            "'pip install -e .[backup]'"
        )
    return rotate_backups


def check_edit_imports():
    try:
        from factmanagement.exports import editing as fm_edit
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install the optional 'excel' depencencies. "
            "'pip install -e .[excel]'"
        )
    return fm_edit


def check_transaction_imports():
    try:
        from factmanagement.exports import transactions as fm_transactions
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install the optional 'excel' depencencies. "
            "'pip install -e .[excel]'"
        )
    return fm_transactions


def check_payments_imports():
    try:
        from factmanagement.exports import payments as fm_payments
    except ModuleNotFoundError:
        raise SystemExit(
            "You need to install the optional 'excel' depencencies. "
            "'pip install -e .[excel]'"
        )
    return fm_payments


def check_metrics_imports():
    try:
        from factmanagement.exports import metrics as fm_metrics
    except ModuleNotFoundError:
        raise SystemExit(
            "you need to install the optional 'excel' dependencies. "
            "'pip install -e .[excel]"
        )
    return fm_metrics


def check_debug_imports():
    try:
        from factmanagement.exports import debug as fm_debug
    except ModuleNotFoundError:
        raise SystemExit(
            "you need to install the optional 'excel' dependencies. "
            "'pip install -e .[excel]"
        )
    return fm_debug


def check_billing_imports():
    try:
        from factmanagement.billing import billing as fm_billing
    except ModuleNotFoundError:
        raise SystemExit(
            "you need to install the optional 'billing' dependencies. "
            "'pip install -e .[billing]"
        )
    return fm_billing


def run_safely(*args, msg: str, capture_output: bool = True) -> str:
    try:
        if capture_output:
            result = subprocess.run(
                *args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True
            )  # type: ignore
        else:
            result = subprocess.run(*args, text=True)
        if result.returncode != 0:
            if result.stdout is not None:
                raise SystemExit(result.stdout)
            raise SystemExit(result.returncode)
        return result.stdout
    except FileNotFoundError:
        raise SystemExit(msg)


@click.group()
@click.option("-v", "--verbose", is_flag=True, default=False)
def main(verbose):
    if verbose:
        dbg.enable_logging()


@main.command()
def init():
    _init()


@main.command()
def lint():
    _lint()


@main.command()
@click.option("-f", "--force", is_flag=True, default=False)
def unittest(force: bool):
    _unittest(force)


@main.command()
@click.option("-f", "--force", is_flag=True, default=False)
def inttest(force: bool):
    _inttest(force)


@main.command()
def format():
    _format()


@main.command()
@click.option("-f", "--force", is_flag=True, default=False)
def test(force: bool):
    _test(force)


@main.command()
@click.option("-f", "--force", is_flag=True, default=False)
@click.option("-o", "--open-report", is_flag=True, default=False)
def coverage(force: bool, open_report: bool):
    _coverage(force, open_report)


@main.command()
def verify():
    _lint()
    _test(True)
    run_safely(
        ["coverage", "report", "--fail-under=100"],
        msg="coverage not found! Install coverage via pip install .[test]",
        capture_output=False,
    )


@main.command()
@click.option("--wsgi-server", type=click.Choice(list(WSGI_RUNNERS.keys())))
@click.option("--dev", is_flag=True, default=False)
@click.option("--host", type=str)
@click.option("--port", type=int)
def run(wsgi_server, dev, host, port):
    if dev:
        wsgi_server = "develop"
    if len(WSGI_RUNNERS) == 0:
        raise SystemExit("No runners")
    elif len(WSGI_RUNNERS) == 1:
        wsgi_server = next(iter(WSGI_RUNNERS.values()))
    elif len(WSGI_RUNNERS) == 2 and not dev:
        wsgi_server = next(filter(lambda x: x.name != "develop", WSGI_RUNNERS.values()))
    else:
        wsgi_server = WSGI_RUNNERS[wsgi_server]
    init_config_file()
    wsgi_server.load()(host, port)


@main.command()
@click.option(
    "-s", "--skip-rotation", is_flag=True, default=False, help="Skip the rotation."
)
def backup(skip_rotation: bool):
    _backup(skip_rotation)


@main.command()
def upcast():
    _backup(skip_rotation=True)
    _upcast()


@main.command()
@click.option(
    "--start-date",
    default=None,
    help="First date to filter payments on.",
    type=click.DateTime(formats=["%Y-%m-%d"]),
)
@click.option(
    "--end-date",
    default=None,
    help="Last date to filter payments on.",
    type=click.DateTime(formats=["%Y-%m-%d"]),
)
@click.option(
    "--dry-run",
    default=True,
    help="Only print results instead of sending them to an entrypoint.",
    type=bool,
)
def billing(start_date, end_date, dry_run):
    if start_date is not None:
        start_date = start_date.date()
    if end_date is not None:
        end_date = end_date.date()
    _billing(start_date, end_date, dry_run)


@main.group()
def excel():
    pass


@excel.command()
def export():
    _export()


@excel.command()
def transactions():
    _transactions()


@excel.command()
def payments():
    _payments()


@excel.command()
def metrics():
    _metrics()


@excel.command()
def debug():
    _debug()


@excel.command("import")
def import_facts():
    _import()


def _init():
    init_config_file()
    print("You can now modify the config files in the ./config directory.")


def _upcast():
    fm_upcast.main()


def _billing(start_date, end_date, dry_run):
    fm_billing = check_billing_imports()
    fm_billing.invoice(start_date, end_date, dry_run)


def _backup(skip_rotation):
    fm_backup.main()
    if skip_rotation:
        return
    check_rotate_imports()
    config_file = config_dir() / "rotate-backup.ini"
    if not config_file.exists():
        shutil.copyfile(config_dir() / "rotate-backup.example.ini", config_file)
    run_safely(
        ["rotate-backups", "-q", "--config", config_file],
        msg="rotate-backups not found! Install it via pip install .[backup]",
    )
    print("Made a backup successfully.")


def _format():
    start_time = time.time()
    print("Running linting:")
    run_safely(
        ["isort", "."],
        msg="isort not found! Install isort via pip install .[lint]",
    )
    print(
        f"  The python imports are formatted nicely. ({time.time() - start_time:.3f} s)"
    )
    run_safely(
        ["black", "."],
        msg="black not found! Install black via pip install .[lint]",
    )
    print(f"  The python code is formatted nicely. ({time.time() - start_time:.3f} s)")
    run_safely(["npm", "run", "fix"], msg="npm not found! Install npm first")
    print(
        f"  The javascript code is formatted nicely. ({time.time() - start_time:.3f} s)"
    )
    print(f"Linting was performed successfully! ({time.time() - start_time:.3f} s)")


def _lint():
    start_time = time.time()
    print("Running linting:")
    run_safely(
        ["isort", ".", "--check"],
        msg="isort not found! Install isort via pip install .[lint]",
    )
    print(
        f"  The python imports are formatted nicely. ({time.time() - start_time:.3f} s)"
    )
    run_safely(
        ["black", ".", "--check"],
        msg="black not found! Install black via pip install .[lint]",
    )
    print(f"  The python code is formatted nicely. ({time.time() - start_time:.3f} s)")
    run_safely(
        ["flake8", "."], msg="flake8 not found! Install flake8 via pip install .[lint]"
    )
    print(f"  The python code is styled nicely. ({time.time() - start_time:.3f} s)")
    run_safely(["mypy"], msg="mypy not found! Install mypy via pip install .[lint]")
    print("  The python code is typed nicely.")
    run_safely(["npm", "run", "lint"], msg="npm not found! Install npm first")
    print(
        f"  The javascript code is formatted nicely. ({time.time() - start_time:.3f} s)"
    )
    print(f"Linting was performed successfully! ({time.time() - start_time:.3f} s)")


def _unittest(force: bool):
    if not force and UNITCOVFILE.exists():
        print("The report for unittests already exists, continuing.")
        return
    print("Running the unittests.")
    run_safely(
        ["pytest", "-m", "not integtest", "--cov", "src", "--cov-report="],
        msg="pytest not found! Install pytest via pip install .[test]",
        capture_output=False,
    )
    shutil.move(COVFILE, UNITCOVFILE)


def _inttest(force: bool):
    # TODO: Make nices feedback during tests. Something like running tests: ....F...xF..
    if not force and INTCOVFILE.exists():
        print("The report for integration tests already exists, continuing.")
        return
    run_safely(
        ["pytest", "-m=integtest", "--cov=src", "--cov-report="],
        msg="pytest not found! Install pytest via pip install .[test]",
        capture_output=False,
    )
    shutil.move(COVFILE, INTCOVFILE)


def _test(force: bool):
    if not force and COVFILE.exists():
        print("The report for the tests already exists, continuing.")
        return
    if not UNITCOVFILE.exists() and not INTCOVFILE.exists():
        run_safely(
            ["pytest", "--cov=src", "--cov-report="],
            msg="pytest not found! Install pytest via pip install .[test]",
            capture_output=False,
        )
    else:
        if not INTCOVFILE.exists():
            _inttest(force)
        elif not UNITCOVFILE.exists():
            _unittest(force)
        run_safely(
            ["coverage", "combine", f"{UNITCOVFILE}", f"{INTCOVFILE}"],
            msg="coverage not found! Install coverage via pip install .[test]",
        )


def _coverage(force: bool, open_report: bool):
    _test(force)
    run_safely(
        ["coverage", "xml"],
        msg="coverage not found! Install coverage via pip install .[test]",
    )
    run_safely(
        ["coverage", "html"],
        msg="coverage not found! Install coverage via pip install .[test]",
    )
    if open_report:
        webbrowser.open_new(f"file://{Path('./htmlcov/index.html').absolute()}")
    run_safely(
        ["coverage", "report", "--fail-under=100"],
        msg="coverage not found! Install coverage via pip install .[test]",
        capture_output=False,
    )


def _export():
    fm_edit = check_edit_imports()
    fm_edit.to_excel(config().server.data_file_path, fm_edit.EXCEL_PATH)


def _transactions():
    fm_transactions = check_transaction_imports()
    fm_transactions.main()


def _payments():
    fm_payments = check_payments_imports()
    fm_payments.main()


def _metrics():
    fm_metrics = check_metrics_imports()
    fm_metrics.main()


def _debug():
    fm_debug = check_debug_imports()
    fm_debug.export()


def _import():
    _backup(skip_rotation=True)
    fm_edit = check_edit_imports()
    fm_edit.to_facts(config().server.data_file_path, fm_edit.EXCEL_PATH)


if __name__ == "__main__":
    main()
