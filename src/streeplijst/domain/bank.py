"""

This module handles payments for users.
It keeps track of payments per user.

"""
from __future__ import annotations

import datetime as dt
import logging
from collections import defaultdict
from copy import deepcopy
from typing import Dict, List, Optional, Tuple, Union

from streeplijst.domain import payment as pay
from streeplijst.domain import user as usr
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES

logger = logging.getLogger(__name__)


class UnknownPayment(exc.MultiLanguageException):
    """Payment not known to the bank."""


class Bank:
    def __init__(self) -> None:
        # TODO: It is debatable if we should store all payments in memory.
        self._payments_by_user_id: Dict[
            idd.Identifier, List[pay.Payment]
        ] = defaultdict(list)
        self._payments_by_id: Dict[idd.Identifier, pay.Payment] = {}
        self._users_by_id: Dict[idd.Identifier, usr.User] = {}

    def get_payment(self, identifier: idd.Identifier) -> pay.Payment:
        return deepcopy(self._get_payment(identifier))

    def _get_payment(self, identifier: idd.Identifier) -> pay.Payment:
        if identifier not in self._payments_by_id:
            raise UnknownPayment().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The payment {identifier} is not known to this bank.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De betaling {identifier} is niet bekend bij deze bank.",
            )
        return self._payments_by_id[identifier]

    def prepare_payment(
        self,
        user: usr.User,
        amount: int,
        identifier: Optional[idd.Identifier] = None,
        datetime: Optional[dt.datetime] = None,
    ) -> pay.Payment:
        """Quickly create a payment object."""
        if user.balance + amount < 0 and not user.allow_negative:
            raise pay.InvalidPayment().with_error_msg(
                LANGUAGES.ENGLISH, "Cannot create a payment that would put user in debt"
            ).with_error_msg(
                LANGUAGES.DUTCH,
                "Deze betaling is niet toegestaan omdat je niet rood mag staan.",
            )
        return pay.Payment(amount, identifier=identifier, creation_datetime=datetime)

    def process_payment(self, user: usr.User, payment: pay.Payment):
        """Registers this payment for this user at this bank."""
        self._payments_by_id[payment.identifier] = payment
        self._payments_by_user_id[user.identifier].append(payment)
        self._users_by_id[payment.identifier] = user
        user.pay(payment.amount)

    def verify_payment(
        self, payment: Union[pay.Payment, idd.Identifier], datetime: dt.datetime
    ):
        """Mark a payment as paid to the bank.

        Parameters
        ----------
        payment : Payment or Identifier
            A payment or identifier of the payment
        datetime : datetime
            The date the payment was received by the bank.

        Returns
        -------

        """
        payment = (
            payment if isinstance(payment, pay.Payment) else self._get_payment(payment)
        )
        payment.verify(datetime)

    def user(self, payment: Union[pay.Payment, idd.Identifier]) -> usr.User:
        """Retrieve the user for a payment.

        Parameters
        ----------
        payment : Payment or Identifier
            A payment or identifier to find the user for.

        Returns
        -------
        User :
            The user if found, otherwise None.

        """
        identifier = payment.identifier if isinstance(payment, pay.Payment) else payment
        return self._users_by_id[identifier]

    def delete_user(self, user):
        unverified_payments = self.unverified_payments(user)
        if len(unverified_payments) > 0:
            total = sum(payment.amount for payment in unverified_payments)
            if total != 0:
                raise usr.CannotDeleteUser().with_error_msg(
                    LANGUAGES.ENGLISH, f"User {user} still has unverified payments"
                ).with_error_msg(
                    LANGUAGES.DUTCH,
                    f"De gebruiker {user.name} heeft nog open betalingen.",
                )
            # Automatically verify all payments if the balance is 0.
            now = dt.datetime.now()
            for payment in unverified_payments:
                payment.verify(now)
        payments = self.payments(user)
        for payment in payments:
            del self._payments_by_id[payment.identifier]
            del self._users_by_id[payment.identifier]
        del self._payments_by_user_id[user.identifier]

    def payments(self, user: Optional[usr.User] = None) -> Tuple[pay.Payment, ...]:
        """All the payments, possibly filtered by user"""
        if user is not None:
            return tuple(self._payments_by_user_id[user.identifier])
        return tuple(self._payments_by_id.values())

    def unverified_payments(
        self, user: Optional[usr.User] = None
    ) -> Tuple[pay.Payment, ...]:
        """All payments not yet verified, possibly filtered by user."""
        return tuple(filter(lambda x: not x.is_verified, self.payments(user)))
