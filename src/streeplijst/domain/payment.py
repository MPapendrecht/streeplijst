"""

This module contains a payment to the system.
Payments can be made by anyone.
They must be verified by the system administrator afterwards.

"""
import datetime as dt
from typing import Optional

from streeplijst.configs.config import config
from streeplijst.domain import entity as ntt
from streeplijst.util import exception as exc
from streeplijst.util import frozen_property as fpt
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES, as_currency


class InvalidPayment(exc.MultiLanguageException):
    """Exception when a payment is invalid."""


class InvalidPaymentDate(exc.MultiLanguageException):
    """Exception when a date is invalid for a payment."""


class InvalidPaymentVerification(exc.MultiLanguageException):
    """Exception when a payment could not be verified."""


class Payment(ntt.Entity):
    """A payment of a certain amount that can be verified."""

    def __init__(
        self,
        amount: int,
        identifier: Optional[idd.Identifier] = None,
        creation_datetime: Optional[dt.datetime] = None,
        validation_datetime: Optional[dt.datetime] = None,
    ):
        if identifier is None:
            identifier = idd.Identifier()
        if amount > config().payments.maximum_amount:
            max_amount = config().payments.maximum_amount / 100
            raise InvalidPayment().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The amount {amount} is larger than the "
                f"maximum allowed {max_amount}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"Het bedrag {as_currency(LANGUAGES.DUTCH, amount / 100)} is "
                f"groter dan het maximale bedrag "
                f"{as_currency(LANGUAGES.DUTCH, max_amount)}",
            )
        super(Payment, self).__init__(identifier)
        self._datetime = (
            creation_datetime if creation_datetime is not None else dt.datetime.now()
        )
        self._validation_datetime = validation_datetime
        self._amount = amount

    @fpt.frozen_property
    def amount(self) -> int:
        return self._amount

    @fpt.frozen_property
    def datetime(self) -> dt.datetime:
        return self._datetime

    # TODO: Only the bank should use this method
    def verify(self, validation_datetime: dt.datetime):
        if self.is_verified:
            raise InvalidPaymentVerification().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The payment {self.identifier.value} has already "
                f"been verified on {self.validated_on}.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De betaling is al goedgekeurd op {self.validated_on}.",
            )
        if not isinstance(validation_datetime, dt.datetime):
            raise InvalidPaymentDate().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The date must be a valid date, not {validation_datetime}.",
            ).with_error_msg(
                LANGUAGES.DUTCH, f"De datum {validation_datetime} is niet geldig."
            )
        self._validation_datetime = validation_datetime

    @property
    def is_verified(self):
        return self._validation_datetime is not None

    @property
    def validated_on(self):
        return self._validation_datetime

    def __repr__(self):
        return (
            f"Payment(identifier={self.identifier}, "
            f"amount={self._amount}, "
            f"validated_on={self._validation_datetime})"
        )
