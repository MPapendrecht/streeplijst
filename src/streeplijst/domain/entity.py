"""
This module describes an entity in the domain.
An entity is any identifiable object.
"""
from streeplijst.util import frozen_property as fpt
from streeplijst.util import identifier as idd


class InvalidIdentifierType(Exception):
    """An exception related to the type of the entity identifier."""


class Entity:
    def __init__(self, identifier: idd.Identifier):
        """
        An uniquely identifiable object in our domain.

        Parameters
        ----------
        identifier : uuid.UUID
            The identifier of this object.

        """
        if not isinstance(identifier, idd.Identifier):
            raise InvalidIdentifierType(f"{identifier}")
        self._identifier = identifier

    @fpt.frozen_property
    def identifier(self) -> idd.Identifier:
        """The identifier of this object."""
        return self._identifier
