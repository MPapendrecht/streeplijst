"""

The domain package contains all the domain specific constructs.
These should be completely decoupled from external services. For example storage or
communication to databases.
The tests should avoid mocking at all costs.

"""
