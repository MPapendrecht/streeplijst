"""

This module describes the user and all rules a user must comply with.

A user is an actor in the system.
They can buy products and make payments.
Each user keeps (limited) track of their product transactions and payments.

"""
import datetime as dt
import logging
import re
from collections import defaultdict, deque
from typing import Any, Deque, Iterator, Optional, Tuple

from streeplijst.configs.config import config
from streeplijst.domain import entity as ntt
from streeplijst.domain import product as prd
from streeplijst.util import exception as exc
from streeplijst.util import secrets
from streeplijst.util.identifier import Identifier
from streeplijst.util.translate import LANGUAGES, as_currency

logger = logging.getLogger(__name__)


class InvalidUserName(exc.MultiLanguageException):
    """Exception when the name of a User is invalid."""


class InvalidBalance(exc.MultiLanguageException):
    """Exception when the balance of a User is invalid."""


class InvalidBalanceLimit(exc.MultiLanguageException):
    """Exception when the balance limit of a User is invalid."""


class InvalidAge(exc.MultiLanguageException):
    """Exception when the age of a User is invalid."""


class InvalidTransaction(exc.MultiLanguageException):
    """Exception when a transaction is invalid."""


class InvalidAllowNegative(exc.MultiLanguageException):
    """Exception when the allow_negative flag is not valid."""


class CannotDeleteUser(exc.MultiLanguageException):
    """Exception when a user cannot be deleted."""


class FifoCache:
    """Stores a limited number of items."""

    def __init__(self, maxsize: int):
        self._storage: Deque[Any] = deque()
        self._maxsize = maxsize

    def add(self, value: Any):
        if len(self._storage) == self._maxsize:
            self._storage.pop()
        self._storage.appendleft(value)

    def __iter__(self) -> Iterator[Any]:
        return iter(self._storage)


class Transaction:
    """Represents buying a product."""

    def __init__(
        self, product_id: Identifier, amount: int, date: Optional[dt.datetime] = None
    ):
        self._product_id = product_id
        self._amount = amount
        if date is None:
            date = dt.datetime.now()
        self._datetime = date

    @property
    def product_id(self):
        return self._product_id

    @property
    def amount(self):
        return self._amount

    @property
    def date(self):
        return self._datetime


class TransactionCache:
    """Stores a limited number of transactions per product."""

    def __init__(self):
        self._storage = defaultdict(
            lambda: FifoCache(maxsize=config().users.transaction_cache_size)
        )

    def append(self, transaction: Transaction):
        self._storage[transaction.product_id].add(transaction)

    def get_transactions_for(self, product: prd.Product) -> Tuple[Transaction, ...]:
        return tuple(t for t in self._storage[product.identifier])


class User(ntt.Entity):
    def __init__(
        self,
        identifier: Identifier,
        name: str,
        balance: int,
        balance_limit: int,
        age: int,
        allow_negative: bool,
        mail: str,
        vault: secrets.Vault,
    ):
        super(User, self).__init__(identifier)
        self._name = name
        self._balance = balance
        self._balance_limit = balance_limit
        self._age = age
        self._mail: bytes = b""
        self._allow_negative = allow_negative
        self._product_transactions = TransactionCache()
        self._transactions = FifoCache(maxsize=config().users.transaction_cache_size)
        self._vault = vault
        # See design notes for this pattern.
        self.name = name
        self.balance = balance
        self.balance_limit = balance_limit
        self.age = age
        self.allow_negative = allow_negative
        self.mail = mail

    @property
    def name(self) -> str:
        """str: The name of this user."""
        return self._name

    @name.setter
    def name(self, name):
        if not isinstance(name, str):
            raise InvalidUserName().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the name must be a string, not {type(name)}.",
            ).with_error_msg(
                LANGUAGES.DUTCH, f"De naam moet een tekst zijn, niet {type(name)}."
            )
        expr = r"^[a-zA-Z0-9][a-zA-Z0-9. ]*[a-zA-Z0-9.]?$"
        if not re.match(expr, name):
            raise InvalidUserName().with_error_msg(
                LANGUAGES.ENGLISH, f"The name {name} must match {expr}."
            ).with_error_msg(
                LANGUAGES.DUTCH, f"De naam {name} moet overeen komen met {expr}."
            )
        self._name = name

    @property
    def balance(self) -> int:
        """int: The amount of money this user can spend."""
        return self._balance

    @balance.setter
    def balance(self, balance):
        if not isinstance(balance, int):
            raise InvalidBalance().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the balance must an integer, not {type(balance)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De balans moet een geheel getal zijn, niet {type(balance)}.",
            )
        if balance < 0 and not self.allow_negative:
            raise InvalidBalance().with_error_msg(
                LANGUAGES.ENGLISH,
                "The user balance cannot be negative if this is not explicitly set.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                "De balans moet positief zijn wanneer "
                "de gebruiker niet in het rood mag staan.",
            )
        self._balance = balance

    @property
    def balance_limit(self) -> int:
        """int: The amount of money this users should have."""
        return self._balance_limit

    @balance_limit.setter
    def balance_limit(self, balance_limit):
        if not isinstance(balance_limit, int):
            raise InvalidBalanceLimit().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the balance limit must be an integer, "
                f"not {type(balance_limit)}.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De balans limiet moet een geheel getal zijn, "
                f"niet {type(balance_limit)}.",
            )
        self._balance_limit = balance_limit

    @property
    def age(self) -> int:
        """int: The number of years this user has been alive."""
        return self._age

    @age.setter
    def age(self, age):
        if not isinstance(age, int):
            raise InvalidAge().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the age must be an integer, not {type(age)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De leeftijd moet een geheel getal zijn, niet {type(age)}",
            )
        if age < 0:
            raise InvalidAge().with_error_msg(
                LANGUAGES.ENGLISH, f"The age {age} must be positive."
            ).with_error_msg(LANGUAGES.DUTCH, f"De leeftijd {age} moet positief zijn.")
        self._age = age

    @property
    def is_adult(self) -> bool:
        """bool: Whether this user is considered an adult."""
        return self._age >= config().users.adult_age

    def acquire_lock(self) -> secrets.Lock:
        return secrets.Lock(self._vault, self.identifier.value)

    @property
    def mail(self) -> Optional[str]:
        """str: The mail of this user."""
        try:
            return self.acquire_lock().unlock(self._mail)
        except secrets.CannotUnlockException:
            return None

    @mail.setter
    def mail(self, value) -> None:
        if not isinstance(value, str):
            raise TypeError(f"Mail must be of type str, not {type(value)}")
        self._mail = self.acquire_lock().lock(value)

    @property
    def locked_mail(self) -> bytes:
        return self._mail

    @locked_mail.setter
    def locked_mail(self, value) -> None:
        self._mail = value

    @property
    def transactions(self) -> Tuple[Transaction, ...]:
        return tuple(t for t in self._transactions)

    @property
    def allow_negative(self):
        return self._allow_negative

    @allow_negative.setter
    def allow_negative(self, value):
        if not isinstance(value, bool):
            raise InvalidAllowNegative().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the allow_negative flag "
                f"must be boolean, not {type(value)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"Of je rood mag staan is waar of niet waar, "
                f"maar niet {type(value)}",
            )
        if value is False and self.balance < 0:
            raise InvalidAllowNegative().with_error_msg(
                LANGUAGES.ENGLISH,
                "The balance cannot be negative when this flag is set to false.",
            ).with_error_msg(
                LANGUAGES.DUTCH, "Je mag niet rood staan en dit aanpassen."
            )
        self._allow_negative = value

    def pay(self, amount: int):
        """
        Pay an amount to the system.

        Parameters
        ----------
        amount : int
            The amount of money to pay to the system.

        Returns
        -------
        Payment :
            A reference to the payment this user made.

        """
        self.balance += amount

    def buy(
        self,
        product: prd.Product,
        amount: int,
        price: int,
        datetime: Optional[dt.datetime] = None,
    ) -> None:
        """
        Buy products from the system.

        Parameters
        ----------
        product : Product
            The product to buy.
        amount : int
            The number of items to buy.
        price : int
            The amount of money paid to buy these items.
        datetime : datetime, optional
            The date the product was bought. Default now.

        """
        if not self.allow_negative and self.balance < price:
            raise InvalidTransaction().with_error_msg(
                LANGUAGES.ENGLISH,
                f"User balance ({self.balance}) is insufficient to buy for {price}.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De balans van {self.name} is "
                f"({as_currency(LANGUAGES.DUTCH, self.balance / 100)}), "
                f"dat is niet voldoende om iets te kopen "
                f"voor {as_currency(LANGUAGES.DUTCH, price / 100)}.",
            )
        if amount > config().transactions.maximum_amount:
            max_amount = config().transactions.maximum_amount / 100
            raise InvalidTransaction().with_error_msg(
                LANGUAGES.ENGLISH,
                f"Cannot buy more than {max_amount} " f"in a single transaction.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"Je kan niet meer kopen dat het maximum van "
                f"{as_currency(LANGUAGES.DUTCH, max_amount)} "
                f"per keer.",
            )
        if product.stock < amount:
            shortage = amount - product.stock
            raise InvalidTransaction().with_error_msg(
                LANGUAGES.ENGLISH,
                f"Product stock of {product.name} is insufficient to buy {shortage}.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De voorraad van {product.name} is onvoldoende "
                f"om {shortage} te kopen.",
            )
        bought_in_past = sum(
            map(
                lambda x: x.amount,
                self._product_transactions.get_transactions_for(product),
            )
        )
        if bought_in_past + amount < 0:
            raise InvalidTransaction().with_error_msg(
                LANGUAGES.ENGLISH,
                f"Refunding more ({amount}) than bought "
                f"in the recent past ({bought_in_past}).",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"{self.name} retourneert meer dan in het recente verleden is gekocht.",
            )
        product.exchange(amount, price)
        transaction = Transaction(product.identifier, amount, datetime)
        self._product_transactions.append(transaction)
        self._transactions.add(transaction)
        self.balance -= price

    def transfer(self, amount: int, other_user: "User"):
        if other_user is self:
            return
        if not self.allow_negative and self.balance < amount:
            raise InvalidTransaction().with_error_msg(
                LANGUAGES.ENGLISH,
                f"User balance ({self.balance}) is insufficient "
                f"to transfer for {amount}.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De balans van {self.name} is "
                f"({as_currency(LANGUAGES.DUTCH, self.balance / 100)}), "
                f"dat is niet voldoende om "
                f"voor {as_currency(LANGUAGES.DUTCH, amount / 100)} over te maken.",
            )
        self.balance -= amount
        other_user.balance += amount

    def __eq__(self, other):
        if other is self:
            return True
        if not isinstance(other, User):
            return False
        if self.identifier != other.identifier:
            return False
        if self.name != other.name:
            return False
        if self.balance != other.balance:
            return False
        if self.balance_limit != other._balance_limit:
            return False
        if self.age != other.age:
            return False
        if self.allow_negative != other.allow_negative:
            return False
        if self.mail != other.mail:
            return False
        return True

    def __hash__(self):
        return hash(self._identifier)

    def delete(self):
        if self.balance != 0:
            remainder = self.balance / 100
            raise CannotDeleteUser().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The user still has " f"{remainder} on their balance.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De gebruiker heeft nog "
                f"{as_currency(LANGUAGES.DUTCH, remainder)} op zijn balans staan.",
            )
        self._vault.delete_owner(self.identifier.value)

    def __repr__(self):
        return f"User({self.identifier!r}, {self.name=})"
