"""

This module contains a product and its logic.
A product describes the inventory of a particular item.
How many items there are, how much each is worth,
and how much profit has been made with the current items.

"""

import re

from streeplijst.domain import entity as ntt
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES


class InvalidProductName(exc.MultiLanguageException):
    """A product name is invalid."""


class InvalidProductStock(exc.MultiLanguageException):
    """A product stock value is invalid."""


class InvalidProductStockLimit(exc.MultiLanguageException):
    """A product stock limit value is invalid."""


class InvalidProductAdultRequirement(exc.MultiLanguageException):
    """A product adult requirement setting is invalid."""


class InvalidProductPrice(exc.MultiLanguageException):
    """A product price requirement is invalid."""


class InvalidProductProfit(exc.MultiLanguageException):
    """A product profit must be an integer."""


class CannotDeleteProduct(exc.MultiLanguageException):
    """A product cannot be deleted."""


class Product(ntt.Entity):
    def __init__(
        self,
        identifier: idd.Identifier,
        name: str,
        stock: int,
        stock_limit: int,
        req_adult: bool,
        price: int,
        profit: int,
    ):
        super(Product, self).__init__(identifier)
        self._name = name
        self._stock = stock
        self._stock_limit = stock_limit
        self._req_adult = req_adult
        self._price = price
        self._profit = profit
        # See design notes for the pattern
        self.name = name
        self.stock = stock
        self.stock_limit = stock_limit
        self.req_adult = req_adult
        self.price = price
        self.profit = profit

    @property
    def name(self) -> str:
        """str: The name of this product."""
        return self._name

    @name.setter
    def name(self, name):
        if not isinstance(name, str):
            raise InvalidProductName().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The type of the name must be a string, not {type(name)}.",
            ).with_error_msg(
                LANGUAGES.DUTCH, f"De naam moet tekst zijn, niet {type(name)}"
            )
        expr = r"^[a-zA-Z0-9][a-zA-Z0-9. ]*[a-zA-Z0-9.]?"
        if not re.match(expr, name):
            raise InvalidProductName().with_error_msg(
                LANGUAGES.ENGLISH, f"The name {name} must match {expr}."
            ).with_error_msg(
                LANGUAGES.DUTCH, f"De naam {name} moet voldoen aan {expr}."
            )
        self._name = name

    @property
    def stock(self) -> int:
        """int: The amount of items in stock."""
        return self._stock

    @stock.setter
    def stock(self, stock):
        if not isinstance(stock, int):
            raise InvalidProductStock().with_error_msg(
                LANGUAGES.ENGLISH, f"The stock must be an integer, not {type(stock)}"
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De voorraad moet een geheel getal zijn, niet {type(stock)}",
            )
        self._stock = stock

    @property
    def stock_limit(self) -> int:
        """int: How much items should be in stock."""
        return self._stock_limit

    @stock_limit.setter
    def stock_limit(self, stock_limit):
        if not isinstance(stock_limit, int):
            raise InvalidProductStockLimit().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The stock limit must be an integer, not {type(stock_limit)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De minimale voorraad moet een geheel getal zijn, "
                f"niet {type(stock_limit)}",
            )
        self._stock_limit = stock_limit

    @property
    def req_adult(self) -> bool:
        """bool: Whether this product can only be bought by an adult."""
        return self._req_adult

    @req_adult.setter
    def req_adult(self, req_adult):
        if not isinstance(req_adult, bool):
            raise InvalidProductAdultRequirement().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The adult requirement must be a boolean, not {type(req_adult)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"Of je volwassen moet zijn is of waar, of niet waar, "
                f"maar niet {type(req_adult)}",
            )
        self._req_adult = req_adult

    @property
    def price(self) -> int:
        """int: The amount of money a single item is worth."""
        return self._price

    @price.setter
    def price(self, price: int):
        if not isinstance(price, int) or price < 0:
            raise InvalidProductPrice().with_error_msg(
                LANGUAGES.ENGLISH, "The product price must be an non-negative integer."
            ).with_error_msg(
                LANGUAGES.DUTCH,
                "De prijs van een product moet een niet-negatief geheel getal zijn.",
            )
        self._price = price

    @property
    def profit(self) -> int:
        """int: The amount of money profit in this product."""
        return self._profit

    @profit.setter
    def profit(self, profit):
        if not isinstance(profit, int):
            raise InvalidProductProfit().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The product investment must be an integer, not {type(profit)}",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                f"De investering in het product moet een geheel getal zijn, "
                f"niet {type(profit)}",
            )
        self._profit = profit

    @property
    def value(self):
        return self.profit + self.stock * self.price

    @property
    def has_low_stock(self) -> bool:
        """bool: If the number of items in stock is below the limit."""
        return self.stock <= self.stock_limit

    def exchange(self, amount: int, price: int) -> None:
        """Exchange some amount of items for a particular price.

        Parameters
        ----------
        amount: int
            The amount of stock to sell. Use negative to add to stock.
        price : int
            The currency profit in this product.

        """
        self.stock -= amount
        self.profit += price

    def __eq__(self, other):
        if other is self:
            return True
        if not isinstance(other, Product):
            return False
        if self.identifier != other.identifier:
            return False
        if self.name != other.name:
            return False
        if self.stock != other.stock:
            return False
        if self.stock_limit != other.stock_limit:
            return False
        if self.req_adult != other.req_adult:
            return False
        if self.price != other.price:
            return False
        if self.profit != other.profit:
            return False
        return True

    def delete(self):
        if self.stock != 0:
            raise CannotDeleteProduct().with_error_msg(
                LANGUAGES.ENGLISH, "The product still has nonzero stock."
            ).with_error_msg(LANGUAGES.DUTCH, "Het product heeft nog voorraad.")
        if self.profit != 0:
            raise CannotDeleteProduct().with_error_msg(
                LANGUAGES.ENGLISH, "The product still has nonzero profit."
            ).with_error_msg(LANGUAGES.DUTCH, "Het product heeft nog winst.")
