"""

The configs package contains a config object, consisting of multiple modules.
All files that are intended to be edited by the user on implementation
must define an example file that can be copied and edited.

"""
