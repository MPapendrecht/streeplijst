import logging
import os
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, Optional

import dacite
import yaml
from dacite import exceptions as dexc

from streeplijst.configs.modules.backup import BackupConfig
from streeplijst.configs.modules.billing import BillingConfig
from streeplijst.configs.modules.logs import LoggingLevel, LogsConfig
from streeplijst.configs.modules.payments import PaymentsConfig
from streeplijst.configs.modules.server import ServerConfig
from streeplijst.configs.modules.transactions import TransactionsConfig
from streeplijst.configs.modules.users import UsersConfig
from streeplijst.util import exception as exc
from streeplijst.util import extensions as ext
from streeplijst.util.translate import LANGUAGES

_CONFIG = None

logger = logging.getLogger(__name__)


class InvalidConfig(exc.MultiLanguageException):
    """Exception when the config is invalid."""


@dataclass(frozen=True)
class Config:
    server: ServerConfig
    users: UsersConfig
    logs: LogsConfig
    payments: PaymentsConfig
    transactions: TransactionsConfig
    backup: Optional[BackupConfig]
    billing: Optional[BillingConfig]


def project_dir():
    return Path(os.getcwd()).absolute()


def config_dir():
    return project_dir() / "config"


def config_file():
    return config_dir() / "config.yaml"


def _load_yaml(file_path: Path) -> dict:
    with open(file_path, "r") as file:
        return yaml.safe_load(file)


def _dump_yaml(filepath: Path, data: dict):
    with open(filepath, "w") as f:
        yaml.safe_dump(data, f)


def _path_resolver(input, root=None):
    if root is None:
        root = project_dir()
    path = Path(input)
    if not path.is_absolute():
        return root.joinpath(path).absolute().resolve()
    return path.resolve()


def make_config(
    parameters: dict,
    path_resolver: Callable[[Any], Path] = _path_resolver,
) -> Config:
    try:
        return dacite.from_dict(
            Config,
            parameters,
            dacite.Config(type_hooks={Path: path_resolver, LoggingLevel: LoggingLevel}),
        )
    except (dexc.WrongTypeError, dexc.MissingValueError) as e:
        raise InvalidConfig().with_error_msg(LANGUAGES.ENGLISH, str(e))


def load_parameters() -> dict:
    if not config_dir().exists():
        config_dir().mkdir()
    try:
        parameters = _load_yaml(config_file())
    except FileNotFoundError:
        parameters = {}
    defaults = _load_yaml(Path(__file__).parent.absolute() / "config.example.yaml")
    return ext.deepmerge(defaults, parameters)


def extend_config(parameters: dict) -> None:
    _dump_yaml(config_file(), {**parameters, **load_parameters()})
    config(rebuild=True)


def init_config_file() -> None:
    print(f"Initializing {config_file()}")
    _dump_yaml(config_file(), load_parameters())


def config(rebuild=False) -> Config:
    global _CONFIG
    if _CONFIG is None or rebuild:
        _CONFIG = make_config(load_parameters())
    return _CONFIG
