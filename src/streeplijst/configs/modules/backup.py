from dataclasses import dataclass
from pathlib import Path
from typing import List


@dataclass
class BackupConfig:
    file_prefix: str
    save_locations: List[Path]
