import logging
from dataclasses import dataclass
from enum import Enum
from pathlib import Path


class LoggingLevel(Enum):
    CRITICAL = logging.getLevelName(logging.CRITICAL)
    ERROR = logging.getLevelName(logging.ERROR)
    WARNING = logging.getLevelName(logging.WARNING)
    INFO = logging.getLevelName(logging.INFO)
    DEBUG = logging.getLevelName(logging.DEBUG)


@dataclass
class LogsConfig:
    logs_file_path: Path
    max_file_size_in_bytes: int
    backup_count: int
    min_level: LoggingLevel
