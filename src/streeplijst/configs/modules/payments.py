from dataclasses import dataclass


@dataclass(frozen=True)
class PaymentsConfig:
    maximum_amount: int

    def __post_init__(self):
        if self.maximum_amount < 0:
            raise ValueError("The maximum amount of the payment must be positive.")
