"""

This package contains the configuration modules.
Each module file contains a dataclass that must be frozen.
Each class is then used in the Config class.
A class is responsible for checking its variables for validity.

"""
