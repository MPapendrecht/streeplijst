from dataclasses import dataclass


@dataclass(frozen=True)
class BillingConfig:
    username: str
    password: str
    entrypoint: str
    grace_period: int
