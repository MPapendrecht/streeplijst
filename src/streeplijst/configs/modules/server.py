import re
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class ServerConfig:
    data_file_path: Path
    vault_file_path: Path
    port: int
    ip: str

    def _check_ip(self):
        if not re.match(r"(\d{1,3}\.){3}\d{1,3}", self.ip):
            raise TypeError(f"The server IP is not valid {self.ip}.")
        values = [int(x) for x in self.ip.split(".")]
        if any(v > 255 or v < 0 for v in values):
            raise ValueError(f"The server IP is not valid {self.ip}.")

    def _check_port(self):
        if self.port < 0 or self.port > 65535:
            raise ValueError(f"The server port {self.port} is not between 0 and 65535.")

    def __post_init__(self):
        self._check_ip()
        self._check_port()
