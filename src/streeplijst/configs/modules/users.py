from dataclasses import dataclass


@dataclass(frozen=True)
class UsersConfig:
    adult_age: int
    transaction_cache_size: int

    def __post_init__(self):
        if self.transaction_cache_size < 0:
            raise ValueError("The transaction cache size must be positive.")
        if self.adult_age < 0:
            raise ValueError("The age of an adult must be positive.")
