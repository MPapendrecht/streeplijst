"""

This module contains the aggregate class, an object that subscribes to a Stream,
and maintains a state based on the facts of the stream.

"""
import abc
import logging
from typing import Iterable

from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.eventsourcing import messaging as msg
from streeplijst.util import exception as exc

logger = logging.getLogger(__name__)


class Aggregate(msg.Subscriber):
    def notify(self, subscription: msg.Subscription, notification: tuple) -> None:
        """Entrypoint for the subscription updates."""


class SingleStreamAggregate(Aggregate, abc.ABC):
    def __init__(
        self, factstore: fcs.FactStore, factdescriptors: Iterable[fcs.FactDescriptor]
    ):
        self._factstore = factstore
        self._factdescriptors = factdescriptors
        self._subscription = self.subscribe()
        self._subscription.update(fcs.Notification(None))

    def subscribe(self):
        return self._factstore.add_subscription(self, *self._factdescriptors)

    def unsubscribe(self):
        if self._subscription is not None:
            self._factstore.remove_subscription(self._subscription)
            self._subscription = None

    def __str__(self):
        return f"{type(self).__name__}"

    def __repr__(self):
        return (
            f"{type(self).__name__}(factstore={self._factstore}, "
            f"fact_descriptors={self._factdescriptors})"
        )


class AutoAggregate(SingleStreamAggregate):
    def __init__(
        self, factstore: fcs.FactStore, factdescriptors: Iterable[fcs.FactDescriptor]
    ):
        self._last_update_index = 0
        super(AutoAggregate, self).__init__(factstore, factdescriptors)

    def notify(self, subscription: msg.Subscription, notification: tuple) -> None:
        logger.debug(f"{self}: Got notification {notification} from {subscription}.")
        if not isinstance(subscription, fcs.StreamSubscription):
            return
        if not isinstance(notification, fcs.Notification):
            return
        last_index = notification.last_index
        logger.debug(f"Retrieving facts from {self._last_update_index} to {last_index}")
        facts = subscription.get_facts(start=self._last_update_index, stop=last_index)
        logger.debug(f"Retrieved {facts}.")
        self._last_update_index += len(facts)
        try:
            for fact in facts:
                logger.debug(f"{self}: Handling {fact}.")
                self.handle_fact(fact)
        except FactHandlerException as e:
            logger.error(e)
            raise e

    @abc.abstractmethod
    def handle_fact(self, fact: fct.Fact):
        """
        Update the state based on the new fact.

        Parameters
        ----------
        fact : streeplijst.eventsourcing.fact.Fact
            A fact that has occurred.

        Returns
        -------

        """


class FactHandlerException(exc.MultiLanguageException):
    """
    An exception raised indicating a fact was not handled.
    """
