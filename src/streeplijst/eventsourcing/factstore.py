"""

This module contains a factstore.
The facts we are talking about are typically used in an eventsourced system.
The factstore is responsible for distributing facts to its streams.
Each stream has a particular set of facts it will store, and these facts
are related to a particular subject.

- Factstore is the entry point for facts, and the source of order.
- Factstore pushes notifications to the subscribers
- The subscribers retrieve facts that they want.
- When adding a subscription, if facts match, a notification is sent.
- Relationship Factstore-Subscriber is Observer pattern.
- A subscriber can have multiple subscriptions.

"""
import logging
from collections import defaultdict, namedtuple
from functools import reduce
from typing import Any, Dict, FrozenSet, Generator, Iterable, List, Optional, Set, Type

from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import messaging as msg
from streeplijst.eventsourcing import repository as rpy

logger = logging.getLogger(__name__)

__all__ = ["FactDescriptor", "FactStore"]


class FactDescriptor:
    """A descriptor of properties of a fact."""

    def __init__(self, fact_type: Type[fct.Fact] = fct.Fact, **kwargs):
        if not isinstance(fact_type, type) or not issubclass(fact_type, fct.Fact):
            raise TypeError(f"The type must be a subclass of Fact, not {fact_type}.")
        self._type = fact_type
        self._requirements = kwargs
        self._hash: int = None  # type: ignore

    def match(self, fact: fct.Fact) -> bool:
        """
        Check whether the fact matches this description.

        Parameters
        ----------
        fact : Fact
            The fact we want to match against.

        Returns
        -------
        bool :
            Whether the fact matches this description.

        """
        if not isinstance(fact, self._type):
            return False
        if any(
            not self._is_requirement_met(getattr(fact, name), requirement)
            for name, requirement in self._requirements.items()
        ):
            return False
        return True

    @staticmethod
    def _is_requirement_met(field_value: Any, requirement: Any) -> bool:
        if callable(requirement):
            return requirement(field_value)
        return field_value == requirement

    def __eq__(self, other):
        if other is self:
            return True
        if not isinstance(other, FactDescriptor):
            return False
        if other._requirements != self._requirements:
            return False
        return True

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = hash((self._type, *tuple(sorted(self._requirements.items()))))
        return self._hash

    def __repr__(self) -> str:
        arguments = ", ".join(
            [f"{key}={val}" for key, val in self._requirements.items()]
        )
        return f"FactDescriptor({self._type.__name__}, {arguments})"


Notification = namedtuple("Notification", ["last_index"])


class Stream(msg.Publisher):
    """A filtered subset of facts in a fact repository."""

    def __init__(
        self, repository: rpy.FactRepository, filters: Iterable[FactDescriptor]
    ):
        super(Stream, self).__init__()
        self._repository = repository
        self._filters = filters
        self._indices: List[int] = []
        self._init_indices()

    def _matches(self, fact: fct.Fact):
        return any(fd.match(fact) for fd in self._filters)

    def _init_indices(self):
        self._indices.extend(
            i for i, j in enumerate(self._repository.get_all()) if self._matches(j)
        )
        if len(self._indices) > 0:
            self._update_subscriptions()

    def _update_subscriptions(self, notification=None):
        super(Stream, self)._update_subscriptions(Notification(len(self._indices)))

    def append(self, index: int):
        logger.debug(f"Appending {index} to {self}.")
        self._indices.append(index)
        self._update_subscriptions()

    def get(self, stop: Optional[int] = None, start: Optional[int] = None):
        logger.debug(f"{self}: Getting facts from {start} to {stop}.")
        if start is None and stop is None:
            return self._repository.get(*self._indices)
        if start is None:
            start = 0
        if stop is None:
            return self._repository.get(*self._indices[start:])
        return self._repository.get(*self._indices[start:stop])


class StreamSubscription(msg.Subscription):
    """A subscription to a stream."""

    def __init__(self, subscriber: msg.Subscriber, stream: Stream):
        super(StreamSubscription, self).__init__(subscriber)
        self._stream = stream
        self._stream.add_subscription(self)

    @property
    def stream(self):
        return self._stream

    def get_facts(self, stop: Optional[int] = None, start: Optional[int] = None):
        """Retrieve a range of facts from a stream."""
        return self._stream.get(stop, start)

    def __eq__(self, other):
        if not super(StreamSubscription, self).__eq__(other):
            return False
        if not isinstance(other, StreamSubscription):
            return False
        if self._stream != other._stream:
            return False
        return True

    def __hash__(self):
        return hash((self._subscriber, self._stream))


class FactStore:
    def __init__(self, repository: rpy.FactRepository):
        self._repository = repository
        self._stream_of_filters: Dict[FrozenSet[FactDescriptor], Stream] = {}
        self._streams_of_filter: Dict[FactDescriptor, Set[Stream]] = defaultdict(set)
        self._filters_of_stream: Dict[Stream, FrozenSet[FactDescriptor]] = defaultdict(
            frozenset
        )

    def add_subscription(
        self, subscriber: msg.Subscriber, *filters: FactDescriptor
    ) -> StreamSubscription:
        if len(filters) == 0:
            fact_descriptors = frozenset((FactDescriptor(),))
        else:
            fact_descriptors = frozenset(filters)
        if fact_descriptors not in self._stream_of_filters:
            self._stream_of_filters[fact_descriptors] = Stream(
                self._repository, fact_descriptors
            )
        stream = self._stream_of_filters[fact_descriptors]
        subscription = StreamSubscription(subscriber, stream)
        self._filters_of_stream[stream] = fact_descriptors
        for fact_descriptor in fact_descriptors:
            self._streams_of_filter[fact_descriptor].add(stream)
        return subscription

    def remove_subscription(self, subscription: StreamSubscription):
        stream = subscription.stream
        stream.del_subscription(subscription)
        if stream.num_subscribers > 0:
            return
        for filter_ in self._filters_of_stream[stream]:
            self._streams_of_filter[filter_].remove(stream)

    def _get_matching_fact_descriptors(
        self, fact: fct.Fact
    ) -> Generator[FactDescriptor, Any, None]:
        return (desc for desc in self._streams_of_filter.keys() if desc.match(fact))

    def _get_matching_streams(self, fact: fct.Fact):
        streams = (
            self._streams_of_filter[f]
            for f in self._get_matching_fact_descriptors(fact)
        )
        return reduce(set.union, streams, set())  # type: ignore

    def add_fact(self, fact: fct.Fact):
        """
        Add the fact to the repository, update streams and notify subscribers.

        Parameters
        ----------
        fact : streeplijst.eventsourcing.fact.Fact
            The fact to add

        """
        logger.debug(f"Adding fact {fact}.")
        if not isinstance(fact, fct.Fact):
            raise TypeError(
                f"Cannot add {fact}, as it is not of type Fact, but {type(fact)}."
            )
        index = self._repository.add(fact)
        for subscriber in self._get_matching_streams(fact):
            subscriber.append(index)

    def replay(self, stop: Optional[int] = None):
        for index, fact in self._repository.replay(stop):
            for subscriber in self._get_matching_streams(fact):
                logger.debug(f"Replaying {index}.")
                subscriber.append(index)
