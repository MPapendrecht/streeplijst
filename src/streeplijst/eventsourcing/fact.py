"""

This module contains the typical event used in event sourcing, called a fact.
A fact is a true statement about the world, and always has a single subject.
This subject is represented using a UUID.

"""
import contextlib
import datetime as dt
import logging
from typing import Dict, Optional, Tuple, Type

from streeplijst.util import exception as exc
from streeplijst.util import frozen_property as fpr
from streeplijst.util import identifier as idd

__all__ = ["Fact", "get_fact_definition"]

logger = logging.getLogger(__name__)


class IncompleteDataForCasting(exc.MultiLanguageException):
    """Data missing in order to cast a fact."""


class Fact:
    _subject: idd.Identifier
    __label: str = "Fact"
    __version: int = 0

    @classmethod
    def __set_label(cls, label):
        if not isinstance(label, str):
            raise TypeError(
                f"The label must be of type string, not {type(cls.__label)}."
            )
        if len(label) == 0:
            raise ValueError("The label cannot be empty.")
        cls.__label = label

    @classmethod
    def __set_version(cls, version):
        if not isinstance(version, int):
            raise TypeError(
                f"The version must be of type integer, not {type(cls.__version)}."
            )
        if version < 0:
            raise ValueError("The version number cannot be negative.")
        cls.__version = version

    def __init_subclass__(cls, *args, label=None, version=None, **kwargs):
        if label is not None:
            cls.__set_label(label)
        if version is not None:
            cls.__set_version(version)
        else:
            cls.__set_version(cls.version + 1)
        if cls.__label == "Fact":
            raise ValueError(
                "The label of a fact cannot be 'Fact'. "
                "Please provide a 'label=...' like you would specify a metaclass."
            )

    def __init__(self, subject=None, creation_datetime: Optional[dt.datetime] = None):
        if subject is None:
            self._subject = idd.Identifier()
        elif isinstance(subject, idd.Identifier):
            self._subject = subject
        else:
            self._subject = idd.Identifier(subject)
        if creation_datetime is None:
            self._datetime = dt.datetime.now()
        elif isinstance(creation_datetime, dt.datetime):
            self._datetime = creation_datetime
        else:
            raise TypeError("The creation_datetime parameter must be a datetime object")
        self._datetime.replace(microsecond=0)

    @classmethod
    def upcast(cls, fact: "Fact", **kwargs) -> "Fact":
        """Cast a fact into another fact.

        Parameters
        ----------
        fact : Fact
            The fact to use as a base.
        kwargs : dict
            Additional parameters that might be necessary for casting.

        Returns
        -------
        Fact
            The fact as an instance of the class it was called from.

        """
        assert issubclass(
            cls, type(fact)
        ), "The fact must be a subclass of the target class."
        assert fact.label == cls.label, "The label must be the same."
        assert fact.version <= cls.version, (
            "The version number of the fact must be "
            "less or equal than the target version."
        )
        return cls._cast(fact)

    @classmethod
    def _cast(cls, fact: "Fact", **kwargs):
        if (cls.label, cls.version) == (fact.label, fact.version):
            return fact
        kwargs.update(
            {
                "_subject": fact.subject,
                "_datetime": fact.datetime,
            }
        )
        new_fact = object.__new__(cls)
        new_fact.__dict__.update(kwargs)
        logger.debug(
            f"Cast fact with label {fact.label}, {fact.version} "
            f"to {new_fact.label}, {new_fact.version}."
        )
        return new_fact

    @fpr.frozen_property
    def label(self) -> str:
        return self.__label

    @fpr.frozen_property
    def version(self) -> int:
        return self.__version

    @fpr.frozen_property
    def subject(self) -> idd.Identifier:
        return self._subject

    @fpr.frozen_property
    def datetime(self) -> dt.datetime:
        return self._datetime

    def __eq__(self, other):
        if other is self:
            return True
        if not isinstance(other, Fact):
            return False
        if other.label != self.label:
            return False
        if other.version != self.version:
            return False
        if other.subject != self.subject:
            return False
        if other.datetime != self.datetime:
            return False
        return True

    def __repr__(self, **kwargs):
        kwargs = {
            "datetime": self.datetime.isoformat(),
            "subject": self.subject,
            **kwargs,
        }
        return (
            f"{self.label}_v{self.version}("
            f"{', '.join(map(lambda x: f'{x[0]}={x[1]}', kwargs.items()))}"
            f")"
        )

    def __reduce__(self):
        return _reconstructor, (self.__label, self.__version), self.__dict__


_FACT_DEFINITIONS: Dict[Tuple[str, int], Type[Fact]] = {}
_FACT_VERSIONS: Dict[str, int] = {}


def _reconstructor(label, version):
    fact_type = get_fact_definition(label, version)
    return object.__new__(fact_type)


def get_fact_definition(label: str, version: int) -> Type[Fact]:
    definition = _FACT_DEFINITIONS.get((label, version))
    if definition is None:
        raise FactTypeNotRegisteredError(
            f"No fact was registered for label={label} and version={version}."
        )
    return definition


def get_latest_version(label: str) -> int:
    return _FACT_VERSIONS[label]


def _get_fact_type_key(fact_type: Type[Fact]) -> Tuple[str, int]:
    return fact_type.label, fact_type.version


def register_fact_definition(fact_type: Type[Fact]) -> None:
    key = _get_fact_type_key(fact_type)
    registered_fact = _FACT_DEFINITIONS.get(key)
    if registered_fact is not None and registered_fact is not fact_type:
        raise KeyError(
            f"The class '{fact_type.__name__}' has the same "
            f"label/version {key} as '{_FACT_DEFINITIONS[key].__name__}'. "
            f"Please set a different 'label' class variable."
        )
    _FACT_DEFINITIONS[key] = fact_type
    try:
        if fact_type.version > get_latest_version(fact_type.label):
            _FACT_VERSIONS[fact_type.label] = fact_type.version
    except KeyError:
        _FACT_VERSIONS[fact_type.label] = fact_type.version


def is_fact_definition_registered(fact_type: Type[Fact]) -> bool:
    key = _get_fact_type_key(fact_type)
    return key in _FACT_DEFINITIONS


def deregister_fact_definition(fact_type: Type[Fact]) -> None:
    key = _get_fact_type_key(fact_type)
    if key not in _FACT_DEFINITIONS:
        raise FactTypeNotRegisteredError(
            "Cannot deregister a fact type that is not registered."
        )
    if get_latest_version(fact_type.label) == fact_type.version:
        _FACT_VERSIONS[fact_type.label] -= 1
    # TODO: Define behaviour if not all versions up to the latest are registered.
    del _FACT_DEFINITIONS[key]


@contextlib.contextmanager
def temporary_fact_definition(*fact_types: Type[Fact]):
    for fact_type in fact_types:
        register_fact_definition(fact_type)
    try:
        yield
    finally:
        for fact_type in fact_types:
            deregister_fact_definition(fact_type)


class FactTypeNotRegisteredError(Exception):
    """Exception when a fact is not registered."""


register_fact_definition(Fact)
