"""

This module contains classes that implement an object that contains facts.
These facts are in a specific order, and can be retrieved given a key/index.

"""

import abc
import logging
import os
import pickle
from typing import Generator, Generic, Optional, Tuple, TypeVar

from streeplijst.eventsourcing import fact as fct

Index = TypeVar("Index")
logger = logging.getLogger(__name__)


class FactRepository(Generic[Index], abc.ABC):
    @abc.abstractmethod
    def get(self, *indices) -> Tuple[fct.Fact, ...]:
        """
        Retrieve an element from this object given an index range.

        Parameters
        ----------
        indices: Index, optional
            A single or iterable of indices related to a fact.

        Returns
        -------
        fct.Fact :
            The fact that was stored.

        """

    @abc.abstractmethod
    def get_all(self) -> Tuple[fct.Fact, ...]:
        """
        Retrieve all elements from this object.

        Returns
        -------
        Tuple[fct.Fact] :
            The facts that are stored in this repository.

        """

    @abc.abstractmethod
    def lock_for_replay(self) -> None:
        """Prevent access to non-replayed objects."""

    @abc.abstractmethod
    def replay(
        self, stop: Optional[int] = None
    ) -> Generator[Tuple[int, fct.Fact], None, None]:
        """
        Replay until a specific target index.
        Facts later than the returned index are not visible.

        Returns
        -------
        Generator :
            The index and facts until this moment.

        """

    @abc.abstractmethod
    def add(self, fact: fct.Fact) -> Index:
        """
        Add a fact to this component.

        Parameters
        ----------
        fact : fct.Fact
            The fact to add.

        Returns
        -------
        Index :
            The index that references the added fact.

        """


class ListStorage(FactRepository[int]):
    def __init__(self):
        self._facts = []
        self._replay_index = None

    def lock_for_replay(self) -> None:
        self._replay_index = -1

    def get(self, *indices: int) -> Tuple[fct.Fact, ...]:
        logger.debug(f"{self}: Retrieving facts {indices}.")
        if self._replay_index is not None and any(
            i > self._replay_index for i in indices
        ):
            raise KeyError("Index out of bounds.")
        return tuple(self._facts[i] for i in indices)

    def get_all(self) -> Tuple[fct.Fact, ...]:
        if self._replay_index is None:
            return tuple(self._facts)
        return tuple(self._facts[: self._replay_index + 1])

    def add(self, fact) -> int:
        index = len(self._facts)
        logger.debug(f"Adding fact on index {index}.")
        self._facts.append(fact)
        return index

    def replay(
        self, stop: Optional[int] = None
    ) -> Generator[Tuple[int, fct.Fact], None, None]:
        if stop is None:
            stop = len(self._facts)
        for index in range(stop):
            self._replay_index = index
            yield index, self._facts[index]
        self._replay_index = None


class FileStorage(ListStorage):
    def __init__(self, path):
        super(FileStorage, self).__init__()
        self._path = os.path.abspath(path)
        logger.debug(f"Reading from {self._path}.")
        folder = os.path.dirname(self._path)
        if not os.path.exists(folder):
            os.makedirs(folder)
        if os.path.isfile(self._path):
            self._load()
        else:
            self._create()

    def _create(self) -> None:
        with open(self._path, "x"):
            pass

    def _load(self) -> None:
        facts = []
        with open(self._path, "rb") as f:
            while True:
                try:
                    facts.append(pickle.load(f))
                except EOFError:
                    break
                except Exception:
                    logger.error(
                        f"Loaded {len(facts)} facts, but failed with: {f.readline()!r}"
                    )
                    raise
        self._facts.clear()
        self._facts.extend(facts)

    def _store(self, fact: fct.Fact) -> None:
        with open(self._path, "ab") as f:
            pickle.dump(fact, f)

    def add(self, fact: fct.Fact) -> int:
        index = super(FileStorage, self).add(fact)
        self._store(fact)
        return index
