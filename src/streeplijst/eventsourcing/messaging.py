from __future__ import annotations

import abc
import logging
from typing import Protocol, Set

logger = logging.getLogger(__name__)


class Subscriber(Protocol):
    """
    A class that can be notified by a subscription with a notification.
    """

    def notify(self, subscription: Subscription, notification: tuple) -> None:
        """
        Handle a notification from a subscription.

        Parameters
        ----------
        subscription : Subscription
            The subscription that has a notification.
        notification : Notification
            The notification that the subscription wants to notify us of.

        """


class Subscription(abc.ABC):
    """
    A subscription that notifies a subscriber when it needs to.

    A subscription is hashable.
    """

    def __init__(self, subscriber: Subscriber):
        self._subscriber = subscriber

    def update(self, notification: tuple):
        """

        Parameters
        ----------
        notification

        Returns
        -------

        """
        logger.debug(f"Notifying {self._subscriber} with {notification}.")
        self._subscriber.notify(self, notification)

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Subscription):
            return False
        if self._subscriber != other._subscriber:
            return False
        return True

    def __hash__(self):
        return hash((self._subscriber,))


class Publisher:
    """
    Publishes notifications via its subscriptions.
    """

    def __init__(self) -> None:
        self._subscriptions: Set[Subscription] = set()

    def add_subscription(self, subscription: Subscription) -> None:
        """
        Add a subscription to publish notifications to.

        Adding a subscription multiple times doesn't publish more notifications.

        Parameters
        ----------
        subscription : Subscription
            The subscription to publish notification to.

        """
        self._subscriptions.add(subscription)

    def del_subscription(self, subscription: Subscription) -> None:
        """
        Remove a subscription from the list.

        Parameters
        ----------
        subscription : Subscription

        Returns
        -------

        """
        self._subscriptions.remove(subscription)

    @property
    def num_subscribers(self):
        return len(self._subscriptions)

    def _update_subscriptions(self, notification):
        for subscription in list(self._subscriptions):
            logger.debug(f"Updating {subscription} with {notification}.")
            subscription.update(notification)
