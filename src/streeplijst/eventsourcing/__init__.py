"""

This module implements the architecture of event sourcing.
Instead of events, we talk about facts.
A fact is a statement about something that happened in the domain.
These facts are stored in streams with a fixed order.
A central component, the fact store, is used to provide an efficient system to route
facts to connected streams.
All facts should have the fact store as single entry point.

From these streams of facts a state can be derived, which is called an aggregate.
The state of the aggregate should always be re-constructable from the fact stream.

"""
