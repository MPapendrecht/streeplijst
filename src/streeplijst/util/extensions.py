def deepmerge(base: dict, target: dict) -> dict:
    """Merge two dicts together where base contains defaults
    that can be overwritten by the target dict.

    Notes
    -----
    May modify the base dict

    """
    for key, target_value in target.items():
        if key not in base:
            base[key] = target_value
            continue
        base_value = base[key]
        if isinstance(base_value, dict) and isinstance(target_value, dict):
            base[key] = deepmerge(base_value, target_value)
        elif isinstance(base_value, list) and isinstance(target_value, list):
            base_value.extend(target_value)
        else:
            base[key] = target_value
    return base


def main():
    base = {"a": 1, "b": {"a": [1, 2, 3], "b": 4}}
    target = {"b": {"a": [5], "c": 5}}
    print(deepmerge(base, target))


if __name__ == "__main__":
    main()
