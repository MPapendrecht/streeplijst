import locale
from enum import Enum, auto


class LANGUAGES(Enum):
    ENGLISH = auto()
    DUTCH = auto()


def as_currency(language: LANGUAGES, value: float | int) -> str:
    locales = {LANGUAGES.ENGLISH: "en_US", LANGUAGES.DUTCH: "nl_NL"}
    locale.setlocale(locale.LC_ALL, locales[language])
    return locale.currency(value)
