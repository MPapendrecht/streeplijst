import cProfile
import logging
import math
import os
import statistics
import time
from contextlib import contextmanager
from logging.handlers import RotatingFileHandler
from typing import Callable, Tuple

import streeplijst
from streeplijst.configs import config


def setup_logger(logger):
    formatter = logging.Formatter(
        "[%(asctime)s] [%(levelname)s] %(name)s -> %(message)s", "%Y-%m-%d %H:%M:%S"
    )
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    log_settings = config.config().logs
    if (
        log_settings.logs_file_path.parent.exists()
    ):  # pragma: no cover Fix for gitlab runners
        error_handler = RotatingFileHandler(
            filename=log_settings.logs_file_path,
            maxBytes=log_settings.max_file_size_in_bytes,
            backupCount=log_settings.backup_count,
        )
        error_handler.setLevel(
            logging.ERROR
            if log_settings.min_level is None
            else log_settings.min_level.value
        )
        logger.addHandler(error_handler)
    return logger


LOGGER = setup_logger(logging.getLogger(streeplijst.__name__))


def get_child_logger(name: str) -> logging.Logger:
    return LOGGER.getChild(name)


def enable_logging(logger=LOGGER) -> int:
    prev_level = logger.level
    logger.setLevel(logging.DEBUG)
    LOGGER.debug(f"Enabling debug logging for {logger}")
    return prev_level


def disable_logging(logger=LOGGER, loglevel: int = logging.NOTSET):
    if logger is LOGGER:
        loglevel = logging.INFO
    LOGGER.debug(f"Disabling debug logging for {logger}")
    logger.setLevel(loglevel)


@contextmanager
def debug_logging(logger=LOGGER):
    prev_level = enable_logging(logger)
    yield
    disable_logging(logger, prev_level)


def debug_output(logger=LOGGER) -> Callable:
    """Decorator: Enables debug logging."""

    def decorator(func: Callable) -> Callable:
        def wrapper(*args, **kwargs):
            with debug_logging(logger):
                return func(*args, **kwargs)

        return wrapper

    return decorator


def profile(func) -> Callable:
    """Decorator: Generates a profile report."""

    def wrapper(*args, **kwargs):
        prof = cProfile.Profile()
        prof.enable()
        output = func(*args, **kwargs)
        prof.disable()
        prof.dump_stats(os.path.join(os.path.dirname(__file__), "../streeplijst.prof"))
        return output

    return wrapper


def elapsed_time(func: Callable, *args, **kwargs) -> float:
    """Calculates the elapsed time executing this function."""
    start_time = time.perf_counter()
    func(*args, **kwargs)
    end_time = time.perf_counter()
    return end_time - start_time


def runtime_distribution(
    func: Callable, runs: int, *args, **kwargs
) -> Tuple[float, float]:
    """Calculates the distribution of runtimes of a function.

    Parameters
    ----------
    func : callable
        The function to calculate the runtime of.
    runs : int
        The number of iterations we do over the function.
    *args : tuple
        The arguments passed to the function.
    **kwargs : dict
        The keyword arguments passed to the function.

    Returns
    -------
    float :
        The mean
    float :
        The standard error of the mean (SEM) = STD / sqrt(runs)

    """
    runtimes = [elapsed_time(func, *args, **kwargs) for _ in range(runs)]
    return statistics.mean(runtimes), statistics.stdev(runtimes) / math.sqrt(runs)
