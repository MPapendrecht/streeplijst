import dataclasses as dtc
import uuid


@dtc.dataclass(frozen=True, eq=True)
class Identifier:
    value: str = dtc.field(default_factory=lambda: str(uuid.uuid4()))
