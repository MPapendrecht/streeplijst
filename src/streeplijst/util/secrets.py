import logging
import sqlite3
from pathlib import Path
from typing import Optional, Protocol

import cryptography.fernet
from cryptography.fernet import Fernet

logger = logging.getLogger(__name__)


class Vault(Protocol):
    """Manage keys per owner"""

    def generate_key(self, owner: str) -> bytes:
        ...

    def retrieve_key(self, owner: str) -> bytes:
        ...

    def delete_owner(self, owner: str) -> None:
        ...


class CannotUnlockException(Exception):
    """For some reason the lock couldn't be unlocked."""


class Lock:
    """Locks values for a particular user"""

    STR_ENCODING = "UTF-8"

    def __init__(self, vault: Vault, owner: str):
        self._vault = vault
        self._owner = owner

    def lock(self, value: str) -> bytes:
        key = self._vault.generate_key(self._owner)
        return Fernet(key).encrypt(value.encode(self.STR_ENCODING))

    def unlock(self, value: bytes) -> str:
        try:
            key = self._vault.retrieve_key(self._owner)
        except KeyError:
            raise CannotUnlockException("Couldn't retrieve the key from the vault.")
        if value is None:
            raise CannotUnlockException("Value to unlock is None")
        try:
            return Fernet(key).decrypt(value).decode(self.STR_ENCODING)
        except cryptography.fernet.InvalidToken:
            raise CannotUnlockException("The value to unlock is invalid")


class MemoryVault:
    """In memory implementation of a Vault."""

    def __init__(self):
        self._keys = {}

    def generate_key(self, owner: str) -> bytes:
        if owner in self._keys:
            return self._keys[owner]
        key = Fernet.generate_key()
        self._keys[owner] = key
        return key

    def retrieve_key(self, owner: str) -> bytes:
        return self._keys[owner]

    def delete_owner(self, owner: str) -> None:
        del self._keys[owner]


class SqliteVault:
    """Sqlite implementation of a Vault."""

    def __init__(self, db_location: Path):
        self._db_path = db_location
        self.init_table()

    def _db(self):
        return sqlite3.connect(self._db_path)

    def init_table(self):
        cur = self._db().cursor()
        cur.execute(
            "CREATE TABLE IF NOT EXISTS keys("
            "owner VARCHAR(255) PRIMARY KEY ON CONFLICT FAIL, "
            "key VARCHAR(255)"
            ")"
        )

    def generate_key(self, owner: str) -> bytes:
        logger.debug(f"SQLITE: Generating new key for owner {owner}")
        transaction = self._db()
        cur = transaction.cursor()
        key = cur.execute("SELECT key FROM keys WHERE owner = ?", (owner,)).fetchone()
        if key is not None:
            return key[0]
        key = Fernet.generate_key()
        cur.execute("INSERT INTO keys VALUES (?, ?)", (owner, key))
        transaction.commit()
        transaction.close()
        return key

    def retrieve_key(self, owner: str) -> bytes:
        cur = self._db().cursor()
        keys = cur.execute("SELECT key FROM keys WHERE owner = ?", (owner,)).fetchall()
        if len(keys) > 1:  # pragma: no cover
            raise KeyError(f"Multiple keys were retrieved for owner {owner}")
        if len(keys) == 0:
            raise KeyError(f"Owner {owner} doesn't have an associated key")
        key = keys[0]
        return key[0]

    def delete_owner(self, owner: str) -> None:
        transaction = self._db()
        cur = transaction.cursor()
        cur.execute("DELETE FROM keys WHERE owner = ?", (owner,))
        transaction.commit()
        transaction.close()


if __name__ == "__main__":
    VAULT = SqliteVault(Path("test.sqlite"))

    class User:
        def __init__(self, identifier):
            self._id = identifier
            self._email = None

        def acquire_lock(self):
            return Lock(VAULT, self._id)

        @property
        def email(self) -> Optional[str]:
            try:
                return self.acquire_lock().unlock(self._email)
            except CannotUnlockException:
                return None

        @email.setter
        def email(self, value: str) -> None:
            self._email = self.acquire_lock().lock(value)

    user = User("anon")
    user.email = "mijnemail"
    user2 = User("anone")
    user2.email = "secondmail"
    VAULT.delete_owner(user._id)
    print(user.email)
    print(user2.email)
