from functools import wraps


class FrozenPropertyException(Exception):
    pass


class FrozenProperty:
    """
    Descriptor that cannot be set or deleted.
    Equivalent to decorating with property and
    raising the exception as setter and deleter

    """

    def __init__(self, fget):
        @wraps(fget)
        def wrapper(*args, **kwargs):
            return fget(*args, **kwargs)

        self._getter = wrapper

    def __get__(self, instance, owner):
        if instance is None:
            try:
                return self._getter(owner)
            except AttributeError:
                return self
        return self._getter(instance)

    def __set__(self, instance, value):
        raise FrozenPropertyException

    def __delete__(self, instance):
        raise FrozenPropertyException


frozen_property = FrozenProperty
