"""

This module contains a custom exception that will call a handler if defined.

"""
from typing import Callable, Dict, List, Tuple

from streeplijst.util.translate import LANGUAGES


class ExceptionWithSideEffect(Exception):
    _side_effects: List[Callable[[], None]] = []

    def __init__(self, *args) -> None:
        super(ExceptionWithSideEffect, self).__init__(*args)
        for side_effect in self._side_effects:
            side_effect()

    @classmethod
    def register_side_effect(cls, side_effect: Callable[[], None]) -> None:
        """
        Register a side effect that is executed on initialization of this exception.

        Parameters
        ----------
        side_effect : Callable[[], None]
            A zero argument None returning function.

        """
        cls._side_effects.append(side_effect)

    @classmethod
    def clear_side_effects(cls) -> None:
        """
        Clears all side effects on this exception.

        """
        cls._side_effects = []

    @classmethod
    def get_side_effects(cls) -> Tuple[Callable[[], None], ...]:
        """
        Get a view of all the side effects currently in effect for this exception.

        Returns
        -------
        Tuple[Callable[[], None], ...]
            A view on the side effects.

        """
        return tuple(cls._side_effects)


class MultiLanguageException(Exception):
    def __init__(self) -> None:
        self._errors: Dict[LANGUAGES, str] = {}

    def with_error_msg(
        self, language: LANGUAGES, message: str
    ) -> "MultiLanguageException":
        self._errors[language] = message
        return self

    def get_error_msg(self, language: LANGUAGES):
        return self._errors[language]

    def __str__(self):
        return next(iter(self._errors.values()))
