import logging
from functools import wraps
from typing import Tuple, Type, Union

from fastapi.exceptions import HTTPException

from streeplijst.util.exception import MultiLanguageException
from streeplijst.util.translate import LANGUAGES

logger = logging.getLogger(__name__)


def catch(
    status_code: int,
    exceptions: Union[Type[MultiLanguageException], Tuple[Type[Exception], ...]],
):
    """Catch a set of exceptions and converts them to HTTPExceptions."""

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exceptions as e:
                if isinstance(e, MultiLanguageException):
                    msg = e.get_error_msg(LANGUAGES.DUTCH)
                else:
                    msg = str(e)
                logger.exception(msg)
                raise HTTPException(status_code, msg)

        return wrapper

    return decorator
