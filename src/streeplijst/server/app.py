import logging
from pathlib import Path

from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from streeplijst import server
from streeplijst.api.payments import service as payment_api
from streeplijst.api.products import service as product_api
from streeplijst.api.users import service as user_api
from streeplijst.configs.config import config
from streeplijst.domain.bank import Bank
from streeplijst.eventsourcing import repository as rpy
from streeplijst.eventsourcing.factstore import FactStore
from streeplijst.server.endpoints import metrics, payments, products, root, users
from streeplijst.util import debug as dbg
from streeplijst.util.secrets import SqliteVault, Vault

logger = logging.getLogger(__name__)


def _with_cors(app):
    origins = [
        "http://localhost",
        "http://localhost:8000",
        "http://localhost:8080",
        "http://127.0.0.1",
        "http://127.0.0.1:8080",
        "http://127.0.0.1:8000",
    ]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app


def _setup_eventsourcing(repository: rpy.FactRepository) -> FactStore:
    return FactStore(repository)


def _setup_user_service(fact_store, bank, vault) -> user_api.UserService:
    return user_api.UserService(fact_store, bank, vault)


def _setup_product_service(fact_store) -> product_api.ProductService:
    return product_api.ProductService(fact_store)


def _setup_payment_service(fact_store, bank) -> payment_api.PaymentService:
    return payment_api.PaymentService(fact_store, bank)


def server_dir():
    return Path(server.__file__).parent.absolute()


def setup_app(repository: rpy.FactRepository, vault: Vault):
    """Create the server instance."""
    appl = _with_cors(FastAPI())
    # All API endpoints
    repository.lock_for_replay()
    apirouter = APIRouter(prefix="/api")
    fact_store = _setup_eventsourcing(repository)
    bank = Bank()
    user_service = _setup_user_service(fact_store, bank, vault)
    product_service = _setup_product_service(fact_store)
    payment_service = _setup_payment_service(fact_store, bank)
    fact_store.replay()
    apirouter.include_router(users.create_users_router(user_service, product_service))
    apirouter.include_router(products.create_products_router(product_service))
    apirouter.include_router(payments.create_payments_router(payment_service))
    apirouter.include_router(
        metrics.create_metrics_router(user_service, product_service, payment_service)
    )
    appl.include_router(apirouter)
    # All file endpoints
    static_dir = server_dir() / "static"
    return root.include_root(appl, static_dir)


def app():
    repository = rpy.FileStorage(config().server.data_file_path)
    vault = SqliteVault(config().server.vault_file_path)
    return setup_app(repository, vault)


def dev_app():
    dbg.enable_logging()
    logger.info("Logging enabled")
    return app()


if __name__ == "__main__":
    dev_app()
