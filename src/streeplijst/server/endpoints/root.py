from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles


def include_root(appl, static_dir):
    @appl.get("/")
    def root():
        return RedirectResponse("/index.html", status_code=301)

    appl.mount("/", StaticFiles(directory=static_dir))
    return appl
