from typing import Any, Dict

from fastapi import APIRouter

from streeplijst.api.metrics.service import MetricService
from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService


def create_metrics_router(
    user_service: UserService,
    product_service: ProductService,
    payment_service: PaymentService,
):
    router = APIRouter(prefix="/metrics")
    service = MetricService(user_service, product_service, payment_service)

    @router.get("/")
    def get_all_metrics() -> Dict[str, Any]:
        return {
            "user_balance": service.get_total_balance(),
            "unverified_payments": service.get_total_unverified_payments(),
            "user_restitution": service.get_total_restitution(),
            "product_profit": service.get_total_product_profit(),
            "projected_product_value": service.get_total_product_value(),
            "sellable_product_value": service.get_sellable_product_value(),
            "total_value": service.get_total_value(),
            "projected_value": service.get_projected_value(),
            "total_invested": service.get_total_invested(),
            "negative_balance": service.get_negative_balance(),
        }

    return router
