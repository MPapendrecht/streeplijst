import datetime as dt
from typing import List

from fastapi import APIRouter

from streeplijst.api.payments import service
from streeplijst.domain import payment as pay
from streeplijst.domain import user as usr
from streeplijst.server import exception_handlers as exc
from streeplijst.util import identifier as idd


class PaymentView:
    def __init__(self, payment: pay.Payment, user: usr.User):
        self.identifier = payment.identifier.value
        self.user_id = user.identifier.value
        self.datetime = payment.datetime.isoformat()
        self.validation_datetime = (
            payment.validated_on.isoformat() if payment.is_verified else None
        )
        self.amount = payment.amount
        self.verified = payment.is_verified


def create_payments_router(payment_service: service.PaymentService):
    router = APIRouter(prefix="/payments")

    @router.get("/")
    def get_all_payments(skip_verified: bool = False) -> List[PaymentView]:
        if skip_verified:
            return [
                PaymentView(payment, user)
                for payment, user in payment_service.unverified_payments()
            ]
        return [PaymentView(payment, user) for payment, user in payment_service.all()]

    @router.post("/{payment_id}/verify", status_code=204)
    @exc.catch(400, pay.InvalidPaymentVerification)
    def verify_payment(payment_id: str, validation_datetime: dt.datetime) -> None:
        payment_service.verify(idd.Identifier(payment_id), validation_datetime)

    return router
