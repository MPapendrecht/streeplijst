import datetime as dt
import logging
from dataclasses import dataclass
from typing import List

from fastapi import APIRouter, HTTPException

from streeplijst.api.products import service as pservice
from streeplijst.api.users import aggregate, service
from streeplijst.cli import check_billing_imports
from streeplijst.domain import payment as pay
from streeplijst.domain import user as usr
from streeplijst.server import exception_handlers as exc
from streeplijst.server.endpoints.payments import PaymentView
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES

logger = logging.getLogger(__name__)


@dataclass
class Order:
    user_id: str
    other_users: dict
    product_id: str
    amount: int
    price: int


class UserView:
    def __init__(self, user_aggregate: aggregate.UserAggregate):
        user = user_aggregate.user()
        self.identifier = user.identifier.value
        self.name = user.name
        self.balance = user.balance
        self.balance_limit = user.balance_limit
        self.age = user.age
        self.is_adult = user.is_adult
        self.allow_negative = user.allow_negative
        self.has_mail = user.mail is not None


class TransactionView:
    def __init__(
        self, transaction: usr.Transaction, product_service: pservice.ProductService
    ):
        product = product_service.find_by_id(transaction.product_id)
        self.label = product.product().name
        self.date = transaction.date.isoformat()
        self.amount = transaction.amount


class BillingNotSupported(exc.MultiLanguageException):
    """Billing is not supported."""


def create_users_router(
    user_service: service.UserService, product_service: pservice.ProductService
):
    router = APIRouter(prefix="/users")

    @router.get("/")
    def get_all_users() -> List[UserView]:
        return [UserView(user) for user in user_service.all()]

    @router.post("/")
    @exc.catch(
        400,
        (
            usr.InvalidUserName,
            usr.InvalidAge,
            usr.InvalidBalance,
            usr.InvalidBalanceLimit,
        ),
    )
    def create_new_user(
        name: str = "Nieuwe gebruiker",
        balance: int = 0,
        balance_limit: int = 0,
        age: int = 1,
        allow_negative: bool = False,
        mail: str = "",
    ) -> UserView:
        logger.debug(
            f"Creating a new user with {name=}, {balance=}, "
            f"{balance_limit=}, {age=}, {allow_negative=}"
        )
        user = user_service.create_new_user(
            name, balance, balance_limit, age, allow_negative, mail
        )
        return UserView(user)

    @router.delete("/{user_id}", status_code=204)
    @exc.catch(400, (service.UnknownUser, usr.CannotDeleteUser))
    def delete_user(user_id: str):
        user_service.delete_user(idd.Identifier(user_id))

    @exc.catch(404, service.UnknownUser)
    def _get_user_safely(user_id: str) -> aggregate.UserAggregate:
        identifier = idd.Identifier(user_id)
        return user_service.find_by_id(identifier)

    @router.get("/{user_id}")
    def get_single_user_by_id(user_id: str):
        user = _get_user_safely(user_id)
        return UserView(user)

    @router.get("/{user_id}/name")
    def get_user_name(user_id: str) -> str:
        user = _get_user_safely(user_id)
        return UserView(user).name

    @router.patch("/{user_id}/name", status_code=204)
    @exc.catch(400, (usr.InvalidUserName, service.UnknownUser))
    def set_user_name(user_id: str, name: str) -> None:
        identifier = idd.Identifier(user_id)
        user_service.set_name(identifier, name)

    @router.get("/{user_id}/balance")
    def get_user_balance(user_id: str) -> int:
        user = _get_user_safely(user_id)
        return UserView(user).balance

    @router.get("/{user_id}/balance_limit")
    def get_user_balance_limit(user_id: str) -> int:
        user = _get_user_safely(user_id)
        return UserView(user).balance_limit

    @router.patch("/{user_id}/balance_limit", status_code=204)
    @exc.catch(400, (usr.InvalidBalanceLimit, service.UnknownUser))
    def set_user_balance_limit(user_id: str, balance_limit: int) -> None:
        identifier = idd.Identifier(user_id)
        user_service.set_balance_limit(identifier, balance_limit)

    @router.get("/{user_id}/age")
    @exc.catch(400, (service.UnknownUser,))
    def get_user_age(user_id: str) -> int:
        user = _get_user_safely(user_id)
        return UserView(user).age

    @router.patch("/{user_id}/age", status_code=204)
    @exc.catch(400, (usr.InvalidAge, service.UnknownUser))
    def set_user_age(user_id: str, age: int) -> None:
        identifier = idd.Identifier(user_id)
        user_service.set_age(identifier, age)

    @router.patch("/{user_id}/mail", status_code=204)
    @exc.catch(400, (service.UnknownUser,))
    def set_user_mail(user_id: str, mail: str) -> None:
        identifier = idd.Identifier(user_id)
        user_service.set_mail(identifier, mail)

    @router.get("/{user_id}/allow_negative")
    @exc.catch(400, (service.UnknownUser,))
    def get_user_allow_negative(user_id: str) -> bool:
        user = _get_user_safely(user_id)
        return UserView(user).allow_negative

    @router.patch("/{user_id}/allow_negative", status_code=204)
    @exc.catch(400, (usr.InvalidAllowNegative, service.UnknownUser))
    def set_user_allow_negative(user_id: str, allow_negative: bool) -> None:
        identifier = idd.Identifier(user_id)
        user_service.set_allow_negative(identifier, allow_negative)

    @router.get("/{user_id}/payments", status_code=200)
    @exc.catch(400, service.UnknownUser)
    def get_user_payments(
        user_id: str, skip_verified: bool = False
    ) -> List[PaymentView]:
        identifier = idd.Identifier(user_id)
        user = _get_user_safely(user_id).user()
        if skip_verified:
            return [
                PaymentView(p, user)
                for p in user_service.unverified_payments(identifier)
            ]
        return [PaymentView(p, user) for p in user_service.payments(identifier)]

    @router.get("/{user_id}/transactions", status_code=200)
    @exc.catch(400, (service.UnknownUser, pservice.UnknownProduct))
    def get_user_transactions(user_id: str) -> List[TransactionView]:
        user = _get_user_safely(user_id).user()
        return [TransactionView(t, product_service) for t in user.transactions]

    @router.post("/{user_id}/payments", status_code=200)
    @exc.catch(400, (service.UnknownUser, usr.InvalidBalance, pay.InvalidPayment))
    def make_payment(user_id: str, amount: int):
        if amount == 0:
            return
        identifier = idd.Identifier(user_id)
        payment_id = user_service.make_payment(identifier, amount)
        return {"payment_id": payment_id.value}

    @router.post("/{user_id}/invoice", status_code=204)
    @exc.catch(400, (service.UnknownUser,))
    @exc.catch(403, (BillingNotSupported))
    def make_invoice(user_id: str, start_date: dt.date, end_date: dt.date) -> None:
        user_idd = idd.Identifier(user_id)
        user = user_service.find_by_id(user_idd).user()
        payments = user_service.payments(user_idd)
        try:
            fm_billing = check_billing_imports()
            billing_request = fm_billing.BillingRequest(user, start_date, end_date)
            for payment in payments:
                billing_request.add_payment(payment)
            fm_billing.send_invoice(billing_request)
        except SystemExit:
            raise BillingNotSupported().with_error_msg(
                LANGUAGES.ENGLISH,
                "Billing is not available on this server. Contact an administrator.",
            ).with_error_msg(
                LANGUAGES.DUTCH,
                "Facturen worden niet ondersteund op deze streeplijst. "
                "Neem contact op met de beheerder.",
            )

    @router.post("/{user_id}/buy/{product_id}", status_code=204)
    @exc.catch(
        400,
        (
            service.UnknownUser,
            pservice.UnknownProduct,
            usr.InvalidTransaction,
            usr.InvalidBalance,
        ),
    )
    def buy_product(user_id: str, product_id: str, amount: int, price: int) -> None:
        product = product_service.find_by_id(idd.Identifier(product_id)).product()
        user = user_service.find_by_id(idd.Identifier(user_id)).user()
        user_service.buy(user, product, amount, price)

    @router.post("/bulk_buy", status_code=204)
    # exc.catch implemented in code
    def bulk_buy(orders: List[Order]):
        products = {
            order.product_id: product_service.find_by_id(
                idd.Identifier(order.product_id)
            ).product()
            for order in orders
        }
        user_ids = set()
        for order in orders:
            user_ids.add(order.user_id)
            for other_user in order.other_users.keys():
                user_ids.add(other_user)
        users = {
            user_id: user_service.find_by_id(idd.Identifier(user_id)).user()
            for user_id in user_ids
        }
        errors = []
        transactions = []
        try_buy = exc.catch(
            400, (service.UnknownUser, pservice.UnknownProduct, usr.InvalidTransaction)
        )(user_service.buy)
        for order in orders:
            try:
                transaction = try_buy(
                    user=users[order.user_id],
                    other_users={
                        users[user_id]: price
                        for user_id, price in order.other_users.items()
                    },
                    product=products[order.product_id],
                    amount=order.amount,
                    price=order.price,
                    dry_run=True,
                )
                if transaction is not None:
                    transactions.append(transaction)
            except HTTPException as e:
                errors.append(e.detail)
        if len(errors) > 0:
            raise HTTPException(400, "\r\n".join(errors))
        user_service.commit_buy(transactions)

    return router
