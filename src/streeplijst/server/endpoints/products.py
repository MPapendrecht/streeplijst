from typing import List

from fastapi import APIRouter

from streeplijst.api.products import aggregate, service
from streeplijst.domain import product as prd
from streeplijst.server import exception_handlers as exc
from streeplijst.util import identifier as idd


class ProductView:
    def __init__(self, product_aggregate: aggregate.ProductAggregate):
        product = product_aggregate.product()
        self.identifier = product.identifier.value
        self.name = product.name
        self.stock = product.stock
        self.stock_limit = product.stock_limit
        self.req_adult = product.req_adult
        self.price = product.price
        self.profit = product.profit


def create_products_router(product_service: service.ProductService):
    router = APIRouter(prefix="/products")

    @router.get("/")
    def get_all_products() -> List[ProductView]:
        return [ProductView(product) for product in product_service.all()]

    @router.get("/totalprofit")
    def get_total_profit() -> int:
        return product_service.total_profit()

    @router.get("/totalvalue")
    def get_total_value() -> int:
        return product_service.total_value()

    @router.post("/")
    @exc.catch(
        400,
        (
            prd.InvalidProductName,
            prd.InvalidProductStock,
            prd.InvalidProductAdultRequirement,
            prd.InvalidProductStockLimit,
        ),
    )
    def create_new_product(
        name: str = "Nieuw product",
        stock: int = 0,
        stock_limit: int = 0,
        req_adult: bool = False,
        price: int = 0,
        profit: int = 0,
    ) -> ProductView:
        product = product_service.create_new_product(
            name, stock, stock_limit, req_adult, price, profit
        )
        return ProductView(product)

    @router.delete("/{prod_id}", status_code=204)
    @exc.catch(400, (service.UnknownProduct, prd.CannotDeleteProduct))
    def delete_product(prod_id: str):
        product_service.delete_product(idd.Identifier(prod_id))

    @exc.catch(404, service.UnknownProduct)
    def _get_product_safely(prod_id: str):
        identifier = idd.Identifier(prod_id)
        return product_service.find_by_id(identifier)

    @router.get("/{prod_id}")
    def get_single_product_by_id(prod_id: str):
        product = _get_product_safely(prod_id)
        return ProductView(product)

    @router.get("/{prod_id}/name")
    def get_product_name(prod_id: str) -> str:
        product = _get_product_safely(prod_id)
        return ProductView(product).name

    @router.patch("/{prod_id}/name", status_code=204)
    @exc.catch(400, (prd.InvalidProductName, service.UnknownProduct))
    def set_product_name(prod_id: str, name: str) -> None:
        identifier = idd.Identifier(prod_id)
        product_service.set_name(identifier, name)

    @router.get("/{prod_id}/stock")
    def get_product_stock(prod_id: str) -> int:
        product = _get_product_safely(prod_id)
        return ProductView(product).stock

    @router.post("/{prod_id}/exchange", status_code=204)
    @exc.catch(
        400, (prd.InvalidProductStock, service.UnknownProduct, prd.InvalidProductProfit)
    )
    def exchange_stock(prod_id: str, amount: int, price: int):
        identifier = idd.Identifier(prod_id)
        product_service.exchange(identifier, amount, price)

    @router.get("/{prod_id}/stock_limit")
    def get_product_stock_limit(prod_id: str) -> int:
        product = _get_product_safely(prod_id)
        return ProductView(product).stock_limit

    @router.patch("/{prod_id}/stock_limit", status_code=204)
    @exc.catch(400, (prd.InvalidProductStockLimit, service.UnknownProduct))
    def set_product_stock_limit(prod_id: str, stock_limit: int) -> None:
        identifier = idd.Identifier(prod_id)
        product_service.set_stock_limit(identifier, stock_limit)

    @router.get("/{prod_id}/req_adult")
    def get_product_req_adult(prod_id: str) -> bool:
        product = _get_product_safely(prod_id)
        return ProductView(product).req_adult

    @router.patch("/{prod_id}/req_adult", status_code=204)
    @exc.catch(400, (prd.InvalidProductAdultRequirement, service.UnknownProduct))
    def set_adult_requirement(prod_id: str, req_adult: bool) -> None:
        identifier = idd.Identifier(prod_id)
        product_service.set_adult_requirement(identifier, req_adult)

    @router.get("/{prod_id}/price")
    def get_product_price(prod_id: str) -> int:
        product = _get_product_safely(prod_id)
        return ProductView(product).price

    @router.patch("/{prod_id}/price", status_code=204)
    @exc.catch(400, (prd.InvalidProductPrice, service.UnknownProduct))
    def set_price(prod_id: str, price: int) -> None:
        identifier = idd.Identifier(prod_id)
        product_service.set_price(identifier, price)

    @router.get("/{prod_id}/profit")
    def get_product_profit(prod_id: str) -> int:
        product = _get_product_safely(prod_id)
        return ProductView(product).profit

    return router
