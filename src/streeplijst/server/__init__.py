"""

This package contains the server application and the endpoints.
Every subpackage should have a separate prefix.

"""

__all__ = ["app"]
