import { RootUrl, myfetch } from './utils/url.js'
import { CurrencyToInt, IntToCurrency, format, splitEqual } from './utils/currency.js'
import { NumToMonth, IsoToDate, DateToHumanSimple, DateToHumanReadable } from './utils/dates.js'
import { objectMap } from './utils/objects.js'
import { IsValidMail } from './utils/mail.js'
import './components/PaymentOverviewSelector.js'
import './components/PaymentOverviewSelectorItem.js'
import './components/PaymentOverviewEditor.js'
import './components/EditableText.js'
import './components/ColorfulButton.js'
// Global constants
/* global Vue */
const selectors = ['Strepen', 'Profiel', 'Voorraad', 'Betalingen', 'Statistieken']
const DatabaseData = {
  persons: {},
  drinks: {},
  payments: {},
  drinks_data: {}
}
const RETRY_TIME = 15000

function DatabaseUpdate () {
  PersonsUpdate()
  DrinksUpdate()
  PaymentsUpdate()
}

function PersonsUpdate (callback) {
  myfetch('api/users/')
    .then(response => {
      if (!response.ok) {
        throw new Error()
      }
      return response.json()
    })
    .then(data => {
      const persons = {}
      data.forEach(user => {
        persons[user.identifier] = user
      })
      DatabaseData.persons = persons
      if (callback != null) {
        callback()
      }
    })
    .catch((error) => {
      console.error(`Response from server was not valid. Retrying in ${RETRY_TIME / 1000} seconds.`)
      console.error(error)
      window.setTimeout(PersonsUpdate, RETRY_TIME)
    })
}

function DrinksUpdate () {
  myfetch('api/products/')
    .then(response => {
      if (!response.ok) {
        throw new Error()
      }
      return response.json()
    })
    .then(data => {
      const products = {}
      data.forEach(product => {
        products[product.identifier] = product
      })
      DatabaseData.drinks = products
    })
    .catch((error) => {
      console.error(`Response from server was not valid. Retrying in ${RETRY_TIME / 1000} seconds.`)
      console.error(error)
      window.setTimeout(DrinksUpdate, RETRY_TIME)
    })
  myfetch('api/products/totalvalue')
    .then(response => {
      if (!response.ok) {
        throw new Error()
      }
      return response.json()
    })
    .then(data => {
      DatabaseData.drinks_data.totalvalue = data
    })
    .catch((error) => {
      console.error(error)
    })
  myfetch('api/products/totalprofit')
    .then(response => {
      if (!response.ok) {
        throw new Error()
      }
      return response.json()
    })
    .then(data => {
      DatabaseData.drinks_data.totalprofit = data
    })
    .catch((error) => {
      console.error(error)
    })
}

function PaymentsUpdate () {
  myfetch('api/payments/?skip_verified=true')
    .then(response => {
      if (!response.ok) {
        throw new Error()
      }
      return response.json()
    })
    .then(data => {
      const payments = {}
      data.forEach(payment => {
        payments[payment.identifier] = payment
      })
      DatabaseData.payments = payments
    })
    .catch((error) => {
      console.error(`Response from server was not valid. Retrying in ${RETRY_TIME / 1000} seconds.`)
      console.error(error)
      window.setTimeout(PaymentsUpdate, RETRY_TIME)
    })
}

Vue.component('modal', {
  data: function () {
    return {
      show: false
    }
  },
  methods: {
    open: function () {
      this.show = true
    },
    close: function () {
      this.show = false
    }
  },
  template: `
    <div v-if="show">
      <div class="modal-mask">
        <div class="modal-wrapper">
          <div class="modal-container">
            <div class="modal-header">
              <slot name="header">
                <div class="modal-default-header">
                  This is a modal header
                </div>
              </slot>
            </div>
            <div class="modal-body">
              <slot name="body">
                This is a modal body.
              </slot>
            </div>
            <div class="modal-footer">
              <slot name="footer">
                <div class="modal-default-footer">
                  <button class="modal-default-button" @click="close">
                    Close
                  </button>
                </div>
              </slot>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})

Vue.component('checkbox', {
  props: ['enabled'],
  data: function () {
    return {
      enabledIcon: RootUrl('vink.svg'),
      disabledIcon: RootUrl('kruis.svg')
    }
  },
  computed: {
    icon: function () {
      if (this.enabled) {
        return this.enabledIcon
      } else {
        return this.disabledIcon
      }
    }
  },
  methods: {
    emitClick: function () {
      this.$emit('click')
    }
  },
  template: `
      <img v-bind:src='icon' @click='emitClick' v-bind:class="[{'vink': enabled, 'kruis': !enabled}, 'icon', 'sep-left']">
  `
})

// Basic person class
Vue.component('person', {
  data: function () {
    return {
      touchdata: {}
    }
  },
  props: ['id', 'name', 'balance', 'balance_limit', 'checked_names', 'allow_negative', 'context'],
  computed: {
    checked: function () {
      return this.checked_names[this.id]
    },
    low_balance: function () {
      if (this.allow_negative) {
        return false
      }
      return this.balance > 0 && this.balance < this.balance_limit
    },
    negative_balance: function () {
      return this.balance < 0
    },
    show_negative_balance: function () {
      return this.context === 'profile' && this.negative_balance
    }
  },
  template: `
  <div
    v-bind:class="[{'selected': checked, 'low-balance': low_balance, 'negative-balance': show_negative_balance}, 'person']"
    @mousedown=startTouch
    @mouseup=endTouch
  >{{name}}</div>
  `,
  methods: {
    changeSetting () {
      this.$emit('change-checked', this.id)
    },
    startTouch (value) {
      this.touchdata.x = value.clientX
      this.touchdata.y = value.clientY
    },
    endTouch (value) {
      const dy = this.touchdata.y - value.clientY
      if (dy * dy < 10) {
        this.changeSetting()
      }
    }
  }
})

// Container for persons
Vue.component('persons', {
  props: {
    persons: Array,
    checked_names: Object,
    condition: {
      type: Function,
      default: () => true
    },
    context: String
  },
  template: `
  <div id='persons' v-dragscroll>
    <person
      v-for="person in persons"
      v-if="condition(person.el)"
      v-bind:id="person.ix"
      v-bind:name="person.el.name"
      v-bind:balance="person.el.balance"
      v-bind:balance_limit="person.el.balance_limit"
      v-bind:allow_negative="person.el.allow_negative"
      v-bind:key="person.ix"
      v-bind:context="context"
      v-bind:checked_names="checked_names"
      v-on:change-checked="$emit('change-checked', $event)"
      >
    </person>
  </div>
  `
})

// Basic drink class
Vue.component('drink', {
  props: ['id', 'name', 'checked_name', 'stock', 'stock_limit', 'context'],
  computed: {
    selected: function () {
      return this.id === this.checked_name
    },
    low_storage: function () {
      return this.stock < this.stock_limit
    },
    display_name: function () {
      if (this.low_storage) {
        return this.name + ' [' + this.stock + ']'
      }
      return this.name
    }
  },
  methods: {
    emitDrink: function () {
      this.$emit('add-drink', this.id)
    }
  },
  template: `
  <div
    v-bind:class="[{'selected': selected, 'low-balance': low_storage}, 'drink']"
    @click.release='emitDrink'
  >{{display_name}}</div>
  `
})

// Container for drinks
Vue.component('drinks', {
  props: {
    drinks: Array,
    condition: {
      type: Function,
      default: () => true
    },
    checked_name: String
  },
  template: `
  <div id="drinks" v-dragscroll>
    <drink
      v-for="drink in drinks"
      v-if="condition(drink.el)"
      v-bind:id="drink.ix"
      v-bind:name="drink.el.name"
      v-bind:stock="drink.el.stock"
      v-bind:stock_limit="drink.el.stock_limit"
      v-bind:checked_name="checked_name"
      v-bind:key="drink.ix"
      @add-drink="$emit('add-drink', $event)"
      >
    </drink>
  </div>
  `
})

// Sub Order
Vue.component('suborder', {
  data: function () {
    return {
      enabledIcon: RootUrl('share-active.svg'),
      disabledIcon: RootUrl('share.svg')
    }
  },
  props: ['id', 'names', 'value', 'price', 'shared_mode'],
  computed: {
    subtotal: function () {
      return IntToCurrency(this.value * this.price)
    },
    subtotal_str: function () {
      return format(this.subtotal)
    },
    isShareEnabled: function () {
      return this.shared_mode != null && this.id === this.shared_mode.id
    }
  },
  methods: {
    del: function () {
      this.$emit('del-elem', this.id)
    },
    dec: function () {
      this.$emit('dec-elem', this.id)
    },
    inc: function () {
      this.$emit('inc-elem', this.id)
    },
    toggleShareMode: function () {
      this.$emit('toggle-share-mode', this.id)
    }
  },
  template: `
  <div class='suborder'>
  <ul class='name'>
    <li v-for="name in names">{{name}}</li>
  </ul>
  <div
    class='share'
    @click='toggleShareMode'
  ><img v-if=isShareEnabled v-bind:src="enabledIcon" class="icon"><img v-else v-bind:src="disabledIcon" class="icon"></div>
  <div
    class='minus'
    @click='dec'
  >-</div>
  <div class='value'>{{value}}</div>
  <div
    class='plus'
    @click='inc'
  >+</div>
  <div 
    class='del'
    @click='del'
  >☓</div>
  <div
    class='subtotal'
  >{{subtotal_str}}</div>
  </div>
  `
})

// Order
Vue.component('order', {
  props: ['drink', 'name', 'persons', 'data', 'price', 'shared_mode'],
  methods: {
    del: function (id) {
      this.$emit('del-elem', { drink: this.drink, id: id })
    },
    dec: function (id) {
      this.$emit('dec-elem', { drink: this.drink, id: id })
    },
    inc: function (id) {
      this.$emit('inc-elem', { drink: this.drink, id: id })
    },
    toggleShareMode: function (id) {
      this.$emit('toggle-share-mode', { drink: this.drink, id: id })
    }
  },
  template: `
  <div class='order'>
  <div class='ordername'>{{name}}</div>
  <suborder v-for='(value, id) in data'
    v-bind:id = id
    v-bind:value = value.amount
    v-bind:shared = value.shared
    v-bind:names = "[id, ...value.shared].map((id) => persons[id].name)"
    v-bind:key = id
    v-bind:price = price
    v-bind:shared_mode = shared_mode
    v-on:del-elem = del
    v-on:dec-elem = dec
    v-on:inc-elem = inc
    v-on:toggle-share-mode = toggleShareMode
  ></suborder>
  </div>
  `
})

// Container for the confirm button
Vue.component('confirm', {
  props: ['price'],
  computed: {
    subtotal: function () {
      return IntToCurrency(this.price)
    },
    subtotal_str: function () {
      return format(this.subtotal)
    }
  },
  methods: {
    sendConfirm: function () {
      this.$emit('confirm')
    }
  },
  template: `
  <div id='overview'>
    <div style="display: flex;">
    <div>Totaal:</div>
    <div style="margin-left: auto">{{subtotal_str}}</div>
    </div>
    <colorful-button
      v-bind:flashClass="'flash'"
      v-on:c-button-click="sendConfirm"
      v-bind:id="'confirm'"
    >
    Bevestig
    </colorful-button>
  </div>
  `
})

// Container for the order
Vue.component('orders', {
  data: function () {
    return {
      orders: {},
      errormsg: ''
    }
  },
  props: ['persons', 'drinks', 'shared_mode'],
  methods: {
    showError: function (value) {
      this.errormsg = value
      this.$refs.errormodal.open()
    },
    addOrder: function (drink, persons) {
      if (!(drink in this.orders)) {
        this.$set(this.orders, drink, {})
      }
      const suborder = this.orders[drink]
      for (const id in persons) {
        if (!persons[id]) continue
        if (!(id in suborder)) {
          this.$set(suborder, id, { amount: 1, shared: [] })
        } else {
          suborder[id].amount += 1
        }
        persons[id] = false
      }
      if (Object.keys(this.orders[drink]).length === 0) {
        this.$delete(this.orders, drink)
      }
    },
    toggleShareholder: function (drink, userid, share) {
      if (share === userid) {
        this.toggleShareMode({ drink: drink, id: userid })
        return
      }
      const shared = this.orders[drink][userid].shared
      const index = shared.indexOf(share)
      if (index === -1) {
        shared.push(share)
      } else {
        shared.splice(index, 1)
      }
    },
    del: function (data) {
      this.$delete(this.orders[data.drink], data.id)
      if (Object.keys(this.orders[data.drink]).length === 0) {
        this.$delete(this.orders, data.drink)
      }
    },
    dec: function (data) {
      this.orders[data.drink][data.id].amount -= 1
    },
    inc: function (data) {
      this.orders[data.drink][data.id].amount += 1
    },
    toggleShareMode: function (data) {
      this.$emit('toggle-share-mode', data)
    },
    confirmOrders: function () {
      const orders = []
      for (const DrinkId in this.orders) {
        const persons = this.orders[DrinkId]
        const price = this.drinks[DrinkId].price
        for (const PersonId in persons) {
          const UserConfig = persons[PersonId]
          const amount = UserConfig.amount
          const valueSplit = splitEqual(amount * price, UserConfig.shared.length + 1)
          const OtherUsers = UserConfig.shared.reduce((obj, UserId, i) => { obj[UserId] = valueSplit[i]; return obj }, {})
          orders.push({
            user_id: PersonId,
            product_id: DrinkId,
            amount: amount,
            price: amount * price,
            other_users: OtherUsers
          })
        }
      }
      window.fetch(RootUrl('api/users/bulk_buy'), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(orders)
      }).then(async response => {
        if (!response.ok) {
          const error = new Error('There was some error when communicating with the server.')
          error.data = await response.json()
          throw error
        } else {
          this.orders = {}
        }
      }).catch(error => {
        console.error(error)
        this.showError(error.data.detail)
      }).finally(() => {
        DatabaseUpdate()
      })
    }
  },
  computed: {
    price: function () {
      let total = 0
      for (const drink in this.orders) {
        const price = this.drinks[drink].price
        let amount = 0
        const suborders = this.orders[drink]
        for (const person in suborders) {
          amount += suborders[person].amount
        }
        total += price * amount
      }
      return total
    }
  },
  template: `
  <div id="orders">
    <div id="order-container" v-dragscroll>
    <order v-for='(data, drink) in orders'
      v-bind:drink = drink
      v-bind:name = drinks[drink].name
      v-bind:persons = persons
      v-bind:data = data
      v-bind:price = drinks[drink].price
      v-bind:key = drink
      v-bind:shared_mode = shared_mode
      v-on:del-elem = del
      v-on:dec-elem = dec
      v-on:inc-elem = inc
      v-on:toggle-share-mode = toggleShareMode
    ></order>
    </div>
    <confirm
      v-bind:price = price
      v-on:confirm = confirmOrders
    ></confirm>
    <modal ref="errormodal">
      <div slot="header" class="modal-default-header">
        Error
      </div>
      <p slot="body" class="modal-default-body preformatted">{{ errormsg }}</p>
    </modal>
  </div>
  `
})

// Container for the streeplijst
Vue.component('streeplijst', {
  data: function () {
    return {
      db_data: DatabaseData,
      checked_names: {},
      shared_mode: null,
      show_underage: true
    }
  },
  computed: {
    persons: function () {
      this.checked_names = {}
      for (const id in this.db_data.persons) {
        this.$set(this.checked_names, id, false)
      }
      return this.db_data.persons
    },
    orderedPersons: function () {
      const persons = Object.keys(this.persons).map(key => { return { el: this.persons[key], ix: key } })
      return persons.sort((a, b) => a.el.name.localeCompare(b.el.name))
    },
    any_underage: function () {
      let underage = false
      for (const id in this.checked_names) {
        if (this.checked_names[id] && !this.persons[id].is_adult) {
          underage = true
          break
        }
      }
      return underage
    },
    drinks: function () {
      return this.db_data.drinks
    },
    orderedDrinks: function () {
      const drinks = Object.keys(this.drinks).map(key => { return { el: this.drinks[key], ix: key } })
      return drinks.sort((a, b) => a.el.name.localeCompare(b.el.name))
    }
  },
  methods: {
    addDrink: function (drink) {
      if (this.shared_mode === null) {
        this.$refs.orders.addOrder(drink, this.checked_names)
      }
    },
    changeChecked: function (id) {
      this.checked_names[id] = !this.checked_names[id]
      if (this.shared_mode != null) {
        this.$refs.orders.toggleShareholder(this.shared_mode.drink, this.shared_mode.id, id)
      }
    },
    shouldShowPerson: function (person) {
      if (!person.allow_negative && person.balance <= 0) {
        return false
      }
      if (!this.show_underage && !person.is_adult) {
        return false
      }
      return true
    },
    shouldShowDrink: function (drink) {
      if (drink.stock <= 0) {
        return false
      }
      if (this.any_underage && drink.req_adult) {
        return false
      }
      return true
    },
    toggleShareMode: function (data) {
      if (this.shared_mode != null && this.shared_mode.drink === data.drink && this.shared_mode.id === data.id) {
        this.shared_mode = null
        this.show_underage = true
        for (const CheckedName in this.checked_names) {
          this.$set(this.checked_names, CheckedName, false)
        }
      } else {
        this.shared_mode = data
        this.show_underage = !this.db_data.drinks[data.drink].req_adult
        const shared = this.$refs.orders.orders[data.drink][data.id].shared
        for (const CheckedName in this.checked_names) {
          this.checked_names[CheckedName] = shared.includes(CheckedName)
        }
        this.checked_names[data.id] = true
      }
    }
  },
  template: `
  <div id='streeplijst'>
    <persons
      v-bind:condition=shouldShowPerson
      v-bind:persons=orderedPersons
      v-bind:checked_names=checked_names
      v-bind:context='"streeplijst"'
      v-on:change-checked=changeChecked
    ></persons>
    <drinks
      v-bind:drinks=orderedDrinks
      v-bind:condition=shouldShowDrink
      v-on:add-drink='addDrink'
    ></drinks>
    <orders
      v-bind:persons=persons
      v-bind:drinks=drinks
      v-bind:shared_mode=shared_mode
      v-on:toggle-share-mode=toggleShareMode
      ref='orders'
    ></orders>
  </div>
 `
})

Vue.component('payment', {
  data: function () {
    return {
      enabledIcon: RootUrl('vink.svg'),
      disabledIcon: RootUrl('kruis.svg')
    }
  },
  props: {
    id: String,
    editable: Boolean,
    date: String,
    validation: String,
    amount: Number
  },
  computed: {
    isVerified: function () {
      return this.validation != null
    },
    formattedAmount: function () {
      return format(IntToCurrency(this.amount))
    },
    formattedDate: function () {
      return DateToHumanReadable(IsoToDate(this.date))
    },
    formattedValidationDate: function () {
      return DateToHumanSimple(IsoToDate(this.validation))
    }
  },
  template: `
  <div class='payment'>
    <span class='payment-date'>{{this.formattedDate}}</span>
    <span>:</span>
    <span class='payment-amount'>{{this.formattedAmount}}. </span>
    <span v-if=isVerified>
      <img v-bind:src='enabledIcon' v-bind:class="['vink', 'icon', 'sep-left']">
      ({{this.formattedValidationDate}}).</span>
    <img v-else v-bind:src='disabledIcon' v-bind:class="['kruis', 'icon', 'sep-left']">
  </div>
  `
})

Vue.component('transaction', {
  props: ['label', 'datetime', 'amount'],
  computed: {
    formattedDate: function () {
      const date = this.datetime.split('T')[0].split('-')
      const month = date[1]
      const day = date[2]
      const time = this.datetime.split('T')[1].split('.')[0]
      const hours = time.split(':').slice(0, 2).join(':')
      return day + ' ' + NumToMonth(Number(month)) + ' ' + hours
    },
    formattedAmount: function () {
      return String(this.amount)
    },
    formattedLabel: function () {
      return this.label
    }
  },
  template: `
    <div class='transaction'>
        <span class='transaction-date'>{{formattedDate}}</span>
        <span> - </span>
        <span class='transaction-amount'>{{formattedAmount}}</span>
        <span class='transaction-label'>{{formattedLabel}}</span>
    </div>
    `
})

Vue.component('profile', {
  props: ['id', 'persons'],
  data: function () {
    const defaultInvoiceStartDate = 'start datum'
    const defaultInvoiceEndDate = 'end datum'
    return {
      payments: null,
      transactions: null,
      transactionMessage: '',
      errormsg: '',
      qr: RootUrl('qr.svg'),
      pen_icon: RootUrl('pen.svg'),
      vink_icon: RootUrl('vink.svg'),
      edit_mail: '',
      invoice_start_date: defaultInvoiceStartDate,
      invoice_end_date: defaultInvoiceEndDate,
      default_invoice_start_date: defaultInvoiceStartDate,
      default_invoice_end_date: defaultInvoiceEndDate
    }
  },
  created: function () {
    this.setUnverPayments()
    this.setTransactions()
  },
  computed: {
    person: function () {
      if (this.persons[this.id] === undefined) {
        return {
          name: 'default_user',
          balance: 0,
          balance_limit: 0
        }
      }
      return this.persons[this.id]
    },
    icon: function () {
      if (this.person.has_mail) {
        return this.vink_icon
      } else {
        return this.pen_icon
      }
    },
    name: function () {
      return this.person.name
    },
    saldo: function () {
      return IntToCurrency(this.person.balance)
    },
    threshold: function () {
      return IntToCurrency(this.person.balance_limit)
    },
    adult: function () {
      return this.person.is_adult
    },
    allow_negative: function () {
      return this.person.allow_negative
    }
  },
  watch: {
    id: function (newVal, oldVal) {
      this.setUnverPayments()
      this.setTransactions()
    }
  },
  methods: {
    showError: function (value) {
      this.errormsg = value
      this.$refs.errormodal.open()
    },
    setTransactions: function () {
      if (this.id === undefined) {
        this.transactions = {}
      } else {
        myfetch(`api/users/${this.id}/transactions`)
          .then(response => { return response.json() })
          .then(data => { this.transactions = data.sort((a, b) => -1 * a.date.localeCompare(b.date)) })
      }
    },
    setUnverPayments: function () {
      if (this.id === undefined) {
        this.payments = {}
      } else {
        myfetch(`api/users/${this.id}/payments`)
          .then(response => { return response.json() })
          .then(data => { this.payments = data.sort((a, b) => -a.datetime.localeCompare(b.datetime)) })
      }
    },
    saveName: function (value) {
      this.person.name = value
      myfetch(`api/users/${this.id}/name`, 'PATCH', { name: value })
        .then(res => {
          if (!res.ok) { throw res }
        })
        .catch(err => {
          this.showError(err.data.detail)
        })
        .finally(() => {
          PersonsUpdate()
        })
    },
    errorName: function () {
      this.showError('Een naam kan niet leeg zijn.')
    },
    saveSaldo: function (value) {
      const NewSaldo = CurrencyToInt(value)
      const difference = NewSaldo - this.person.balance
      const diffStr = IntToCurrency(difference)
      this.transactionMessage = `Maak ${diffStr} euro over naar de beheerder.`
      this.persons[this.id].balance = NewSaldo
      myfetch(`api/users/${this.id}/payments`, 'POST', { amount: difference })
        .then(res => {
          if (!res.ok) {
            throw res
          }
          this.$refs.transaction.open()
          this.setUnverPayments()
        })
        .catch(error => {
          this.showError(error.data.detail)
        })
        .finally(() => {
          PaymentsUpdate()
          PersonsUpdate()
        })
    },
    showEditMail: function () {
      this.$refs.editmailmodal.open()
    },
    saveEditMail: function () {
      if (!IsValidMail(this.edit_mail)) {
        this.showError('De email is niet correct.')
        return
      }
      myfetch(`api/users/${this.id}/mail`, 'PATCH', { mail: this.edit_mail })
        .then(res => PersonsUpdate())
      this.closeEditMail()
    },
    closeEditMail: function () {
      this.edit_mail = ''
      this.$refs.editmailmodal.close()
    },
    errorSaldo: function () {
      this.showError('Geef een bedrag in euro (x.xx)')
    },
    saveThreshold: function (value) {
      const NewTreshold = CurrencyToInt(value)
      this.persons[this.id].threshold = NewTreshold
      myfetch(`api/users/${this.id}/balance_limit`, 'PATCH', { balance_limit: NewTreshold })
        .then(res => PersonsUpdate())
    },
    errorThreshold: function () {
      this.showError('Geef een bedrag in euro (x.xx)')
    },
    changeAdult: function () {
      if (this.adult) {
        myfetch(`api/users/${this.id}/age`, 'PATCH', { age: 1 })
          .then(res => PersonsUpdate())
      }
      if (!this.adult) {
        myfetch(`api/users/${this.id}/age`, 'PATCH', { age: 18 })
          .then(res => PersonsUpdate())
      }
    },
    changeAllowNegative: function () {
      if (this.allow_negative) {
        myfetch(`api/users/${this.id}/allow_negative`, 'PATCH', { allow_negative: false })
          .then(res => PersonsUpdate())
          .catch(error => {
            this.showError(error.data.detail)
          })
      }
      if (!this.allow_negative) {
        myfetch(`api/users/${this.id}/allow_negative`, 'PATCH', { allow_negative: true })
          .then(res => PersonsUpdate())
          .catch(error => {
            this.showError(error.data.detail)
          })
      }
    },
    errorDate: function (value) {
      this.showError('Incorrecte datum')
    },
    saveInvoiceStartDate: function (value) {
      this.invoice_start_date = value
    },
    saveInvoiceEndDate: function (value) {
      this.invoice_end_date = value
    },
    openInvoice: function () {
      this.$refs.invoiceModal.open()
    },
    closeInvoice: function () {
      this.invoice_start_date = this.default_invoice_start_date
      this.invoice_end_date = this.default_invoice_end_date
      this.$refs.invoiceModal.close()
    },
    sendInvoice: function () {
      if (this.invoice_start_date === this.default_invoice_start_date || this.invoice_end_date === this.default_invoice_end_date) {
        this.showError('Vul een start en eind datum in.')
        return
      }
      if (this.invoice_start_date > this.invoice_end_date) {
        this.showError('De eind datum moet later zijn dan de start datum.')
        return
      }
      myfetch(`api/users/${this.id}/invoice`, 'POST', { start_date: this.invoice_start_date, end_date: this.invoice_end_date })
        .catch(error => {
          this.showError(error.data.detail)
        })
      this.invoice_start_date = this.default_invoice_start_date
      this.invoice_end_date = this.default_invoice_end_date
      this.closeInvoice()
    }
  },
  template:
  `
  <div id='profile'>
    <div id='profile-name'>
      <editable-text
        v-bind:value=name
        v-bind:format=/.+/
        v-bind:onError=errorName
        v-bind:onSave=saveName
      ></editable-text>
    </div>
    <div id='profile-body' v-dragscroll>

      <span id='pb-saldo' class='single-line-editable sub-profile'>
        Huidig saldo: €
        <editable-text
          v-bind:value=saldo
          v-bind:format=/^-?\\d+\\.\\d\\d$/
          v-bind:onError=errorSaldo
          v-bind:onSave=saveSaldo
        ></editable-text>
      </span>
      <span id='pb-threshold' class='single-line-editable sub-profile'>
        Waarschuwen bij: €
        <editable-text
          v-bind:value=threshold
          v-bind:format=/^\\d+\\.\\d\\d$/
          v-bind:onError=errorThreshold
          v-bind:onSave=saveThreshold
        ></editable-text>
      </span>
      <div class="sub-profile icon-container">
        <span>Volwassen:</span>
        <checkbox
          v-bind:enabled='adult'
          @click=changeAdult
        ></checkbox>
      </div>
      <div class="sub-profile icon-container">
        <span>Mag rood staan:</span>
        <checkbox
          v-bind:enabled='allow_negative'
          @click=changeAllowNegative
        ></checkbox>
      </div>
      <span class="sub-profile icon-container">
        <span>Email instellen:</span>
        <img @click="showEditMail" v-bind:src="icon" class="icon sep-left editable-text-icon">
      </span>
      <span class="sub-profile" v-if="this.person.has_mail">
        <colorful-button
          v-on:c-button-click="openInvoice"
          v-bind:flashClass="'flash'"
        >
        Verstuur factuur
        </colorful-button>
      </span>
      <div id='transactions' class='sub-profile'>
        <span>Meest recente transacties:</span>
        <div id='transaction-list'>
            <transaction
                v-for='(props, id) in transactions'
                v-bind:key='id'
                v-bind:datetime='props.date'
                v-bind:label='props.label'
                v-bind:amount='props.amount'
            ></transaction>
        </div>
      </div>
      <div class='sub-profile'>
        <span>Betalingen:</span>
        <div id='payment-list'>
          <payment
            v-for='(props, id) in payments'
            v-bind:key='id'
            v-bind:id='props.identifier'
            v-bind:date='props.datetime'
            v-bind:validation='props.validation_datetime'
            v-bind:amount='props.amount'
            v-on:verified=setUnverPayments
          ></payment>
        </div>
      </div>
    </div>
    <modal ref="invoiceModal">
      <div slot="header" class="modal-default-header">
        Factuur verzenden
      </div>
      <div slot="body" class="invoice-container">
        <span class='single-line-editable modal-default-body'>
          <p>Maak factuur van</p>
          <pre> </pre>
          <editable-text
            :value=this.invoice_start_date
            :format=/.*/
            :onError=errorDate
            :onSave=saveInvoiceStartDate
            :type='"date"'
          ></editable-text>
          <pre> </pre>
          <p>tot</p>
          <pre> </pre>
          <editable-text
            :value=this.invoice_end_date
            :format=/.*/
            :onError=errorDate
            :onSave=saveInvoiceEndDate
            :type='"date"'
          ></editable-text>
        </span>
      </div>
      <div slot="footer" class="modal-default-footer">
        <button class="modal-default-button" @click="sendInvoice">
          Verzend
        </button>
        <button class="modal-default-button" @click="closeInvoice">
          Annuleren
        </button>
      </div>
    </modal>
    <modal ref="transaction">
      <div slot="header" class="modal-default-header">
        Geld overmaken
      </div>
      <div slot="body" class="transaction-container">
        <p class="modal-default-body">{{ transactionMessage }}</p>
        <a class="transaction-icon" href="http://martijn.graafbernadotte.nl/streeplijst/tikkie" target="_black" rel="noreferrer noopener">
          <img v-bind:src=qr style="width: 100%; max-height: 60vh;">
        </a>
      </div>
    </modal>
    <modal ref="editmailmodal">
      <p slot="header" class="modal-default-header">Email aanpassen</p>
      <div slot="body" class="modal-default-body">
        <p>Email:</p>
        <input class="input" v-model="edit_mail" type="text"/>
      </div>
      <div slot="footer" class="modal-default-footer">
        <button class="modal-default-button" @click="saveEditMail">
          Verder
        </button>
        <button class="modal-default-button" @click="closeEditMail">
          Annuleren
        </button>
      </div>
    </modal>
    <modal ref="errormodal">
      <div slot="header" class="modal-default-header">
        Error
      </div>
      <p slot="body" class="modal-default-body">{{ errormsg }}</p>
    </modal>
  </div>
  `
})

// Container for the profiles
Vue.component('profiles', {
  data: function () {
    return {
      db_data: DatabaseData,
      checked_names: {},
      checked_name: '',
      errormsg: '',
      new_user_mail: ''
    }
  },
  created: function () {
    this.selectFirstUser()
  },
  computed: {
    persons: function () {
      this.checked_names = {}
      for (const id in this.db_data.persons) {
        this.$set(this.checked_names, id, (id === this.checked_name))
      }
      return this.db_data.persons
    },
    orderedPersons: function () {
      const persons = Object.keys(this.persons).map(key => { return { el: this.persons[key], ix: key } })
      return persons.sort((a, b) => a.el.name.localeCompare(b.el.name))
    }
  },
  methods: {
    showError: function (value) {
      this.errormsg = value
      this.$refs.errormodal.open()
    },
    showNewUser: function () {
      this.$refs.newusermodal.open()
    },
    selectFirstUser: function () {
      this.checked_name = this.orderedPersons[0].ix
    },
    changeChecked: function (id) {
      if (this.checked_name !== id) {
        this.checked_names[this.checked_name] = false
        this.checked_names[id] = true
        this.checked_name = id
      }
    },
    saveNewUser: function () {
      if (!IsValidMail(this.new_user_mail)) {
        this.showError('De email is niet correct.')
        return
      }
      myfetch('api/users/', 'POST', { mail: this.new_user_mail })
        .then(res => res.json())
        .then(res => {
          PersonsUpdate()
          this.changeChecked(res.identifier)
        })
      this.closeNewUser()
    },
    closeNewUser: function () {
      this.new_user_mail = ''
      this.$refs.newusermodal.close()
    },
    delUser: function () {
      const person = this.persons[this.checked_name]
      if (person.balance !== 0) {
        this.errormsg = 'Alleen een account met een saldo van 0 euro kan worden verwijderd.'
        this.$refs.errormodal.open()
        return
      }
      const name = person.name
      const del = window.confirm(`Je gaat het account van ${name} verwijderen. Weet je het zeker?`)
      if (del) {
        myfetch(`api/users/${this.checked_name}`, 'DELETE')
          .then((res) => {
            PersonsUpdate(this.selectFirstUser)
          })
          .catch(error => {
            this.showError(error.data.detail)
          })
      }
    }
  },
  template: `
  <div id='profiles'>
    <div id='profile-selector'>
      <persons
        v-bind:persons=orderedPersons
        v-bind:checked_names=checked_names
        v-bind:context='"profile"'
        v-on:change-checked=changeChecked
      ></persons>
      <profile
        v-bind:persons=persons
        v-bind:id=checked_name
      ></profile>
    </div>
    <div id='profile-edits'>
      <colorful-button
        v-bind:id="'new-user'"
        v-bind:flashClass="'flash'"
        v-on:c-button-click="showNewUser"
      >
      Nieuw profiel
      </colorful-button>
      <colorful-button
        v-bind:id="'del-user'"
        v-on:c-button-click="delUser"
        v-bind:flashClass="'flash'"
      >
      Verwijder profiel
      </colorful-button>
    </div>
    <modal ref="newusermodal">
      <p slot="header" class="modal-default-header">Nieuwe gebruiker aanmaken</p>
      <div slot="body" class="modal-default-body">
        <p>Email:</p>
        <input class="input" v-model="new_user_mail" type="text"/>
      </div>
      <div slot="footer" class="modal-default-footer">
        <button class="modal-default-button" @click="saveNewUser">
          Verder
        </button>
        <button class="modal-default-button" @click="closeNewUser">
          Annuleren
        </button>
      </div>
    </modal>
    <modal ref="errormodal">
      <div slot="header" class="modal-default-header">
        Error
      </div>
      <p slot="body" class="modal-default-body">{{ errormsg }}</p>
    </modal>
  </div>
 `
})

Vue.component('drinkprops', {
  props: {
    drinks: Object,
    id: String
  },
  data: function () {
    return {
      stock: 0,
      profit: 0,
      errormsg: ''
    }
  },
  methods: {
    showError: function (value) {
      this.errormsg = value
      this.$refs.errormodal.open()
    },
    saveName: function (value) {
      this.drink.name = value
      myfetch(`api/products/${this.id}/name`, 'PATCH', { name: value })
        .then(res => DrinksUpdate())
    },
    errorName: function () {
      this.showError('Een naam kan niet leeg zijn.')
    },
    savePrice: function (value) {
      const NewPrice = CurrencyToInt(value)
      this.drink.price = NewPrice
      myfetch(`api/products/${this.id}/price`, 'PATCH', { price: NewPrice })
        .then(res => DrinksUpdate())
    },
    errorPrice: function () {
      this.showError('Deze prijs is niet correct.')
    },
    saveStock: function (value) {
      this.stock = parseInt(value)
    },
    saveStockModification: function (value) {
      this.stock = this.drink.stock + parseInt(value)
    },
    errorStock: function () {
      this.showError('Deze hoeveelheid is niet correct.')
    },
    saveStockLimit: function (value) {
      this.drink.stock_limit = parseInt(value)
      myfetch(`api/products/${this.id}/stock_limit`, 'PATCH', { stock_limit: value })
        .then(res => DrinksUpdate())
    },
    saveProfit: function (value) {
      this.profit = CurrencyToInt(value)
    },
    saveProfitModification: function (value) {
      this.profit = this.drink.profit - CurrencyToInt(value)
    },
    errorProfit: function (value) {
      this.showError('Deze investering is niet correct.')
    },
    commitChanges: function () {
      if (this.stock === this.drink.stock & this.profit === this.drink.profit) {
        return
      }
      myfetch(`api/products/${this.id}/exchange`, 'POST', { amount: -this.stock_diff, price: -this.profit_diff })
        .then(res => {
          DrinksUpdate()
        })
    },
    changeReqAdult: function () {
      if (this.req_adult) {
        myfetch(`api/products/${this.id}/req_adult`, 'PATCH', { req_adult: false })
          .then(res => {
            DrinksUpdate()
          })
      } else {
        myfetch(`api/products/${this.id}/req_adult`, 'PATCH', { req_adult: true })
          .then(res => {
            DrinksUpdate()
          })
      }
    }
  },
  computed: {
    drink: function () {
      let drink = this.drinks[this.id]
      if (drink === undefined) {
        drink = {
          name: 'default_drink',
          price: 0,
          stock: 0,
          profit: 0,
          stock_limit: 0,
          req_adult: false
        }
      }
      this.stock = drink.stock
      this.profit = drink.profit
      return drink
    },
    name: function () {
      return this.drink.name
    },
    price: function () {
      return this.drink.price
    },
    req_adult: function () {
      return this.drink.req_adult
    },
    stock_limit: function () {
      return this.drink.stock_limit
    },
    stock_diff: function () {
      return this.stock - this.drink.stock
    },
    profit_diff: function () {
      return this.drink.profit - this.profit
    },
    break_even_price: function () {
      if (this.stock === 0) return 0
      return -this.profit / this.stock
    },
    total_value: function () {
      return this.profit + this.stock * this.price
    },
    low_storage: function () {
      return this.drink.stock < this.drink.stock_limit
    },
    price_str: function () {
      return IntToCurrency(this.price)
    },
    profit_str: function () {
      return IntToCurrency(this.profit)
    },
    profit_diff_str: function () {
      return IntToCurrency(this.profit_diff)
    },
    break_even_price_str: function () {
      return IntToCurrency(this.break_even_price)
    },
    total_value_str: function () {
      return IntToCurrency(this.total_value)
    }
  },
  template: `
  <div id='drinkprops'>
    <div id='profile-name'>
      <editable-text
        v-bind:value=name
        v-bind:format=/.+/
        v-bind:onSave=saveName
        v-bind:onError=errorName
       ></editable-text>
    </div>
    <div id='drinks-body' v-dragscroll>
      <div class="single-line-editable sub-profile">
        Prijs: €
        <editable-text
          v-bind:value=price_str
          v-bind:format=/^\\d+\\.\\d\\d$/
          v-bind:onSave=savePrice
          v-bind:onError=errorPrice
        ></editable-text>
      </div>
      <div class="single-line-editable sub-profile">
        <p>Voorraad:</p><pre> </pre>
        <editable-text
          v-bind:value=stock
          v-bind:format=/^-?\\d+/
          v-bind:onSave=saveStock
          v-bind:onError=errorStock
        ></editable-text>
        <pre> + (</pre>
        <editable-text
          v-bind:value=stock_diff
          v-bind:format=/^-?\\d+/
          v-bind:onSave=saveStockModification
          v-bind:onError=errorStock
        ></editable-text>
        <pre>)</pre>
      </div>
      <div class="single-line-editable sub-profile">
        <p>Winst:</p><pre> </pre>
        <editable-text
          v-bind:value=profit_str
          v-bind:format=/^-?\\d+\\.\\d\\d$/
          v-bind:onSave=saveProfit
          v-bind:onError=errorProfit
        ></editable-text>
        <pre> - (</pre>
        <editable-text
          v-bind:value=profit_diff_str
          v-bind:format=/^-?\\d+\\.\\d\\d$/
          v-bind:onSave=saveProfitModification
          v-bind:onError=errorProfit
        ></editable-text>
        <pre>)</pre>
      </div>
      <div class="single-line-editable sub-profile">
        <p>Minimale voorraad:</p><pre> </pre>
        <editable-text
          v-bind:value=stock_limit
          v-bind:format=/^\\d+$/
          v-bind:onSave=saveStockLimit
          v-bind:onError=errorStock
        ></editable-text>
      </div>
      <div class="sub-profile icon-container">
        <span>
          Vereist volwassenen:
        </span>
        <checkbox @click='changeReqAdult' v-bind:enabled='req_adult'></checkbox>
      </div>
      <div class="sub-profile">
      <span>Break even prijs: €{{break_even_price_str}}</span>
      </div>
      <div class="sub-profile">
      <span>Verwachte totale waarde: €{{total_value_str}}</span>
      </div>
      <div class="sub-profile">
      <colorful-button
        v-bind:id="'commit-drink-changes'"
        v-bind:flashClass="'flash'"
        v-on:c-button-click="commitChanges"
      >
      Sla voorraad verandering op
      </colorful-button>
      </div>
    </div>
    <modal ref="errormodal">
      <div slot="header" class="modal-default-header">
      Error
      </div>
      <p slot="body" class="modal-default-body">{{ errormsg }}</p>
    </modal>
  </div>
  `
})

Vue.component('stock', {
  data: function () {
    return {
      db_data: DatabaseData,
      checked_name: '',
      errormsg: ''
    }
  },
  created: function () {
    this.selectFirstDrink()
  },
  computed: {
    drinks: function () {
      return this.db_data.drinks
    },
    orderedDrinks: function () {
      const drinks = Object.keys(this.drinks).map(key => { return { el: this.drinks[key], ix: key } })
      return drinks.sort((a, b) => a.el.name.localeCompare(b.el.name))
    },
    totalValue: function () {
      return format(IntToCurrency(this.db_data.drinks_data.totalvalue ? this.db_data.drinks_data.totalvalue : 0))
    },
    totalProfit: function () {
      return format(IntToCurrency(this.db_data.drinks_data.totalprofit ? this.db_data.drinks_data.totalprofit : 0))
    }
  },
  methods: {
    showError: function (value) {
      this.errormsg = value
      this.$refs.errormodal.open()
    },
    selectFirstDrink: function () {
      this.checked_name = this.orderedDrinks[0].ix
    },
    changeChecked: function (id) {
      this.checked_name = id
    },
    newDrink: function () {
      myfetch('api/products/', 'POST')
        .then(res => res.json())
        .then(res => {
          DrinksUpdate()
          this.changeChecked(res.identifier)
        })
    },
    delDrink: function () {
      const drink = this.drinks[this.checked_name]
      if (drink.stock !== 0) {
        this.showError('Alleen een product met een voorraad van 0 kan worden verwijderd.')
        return
      }
      if (drink.profit !== 0) {
        this.showError('Alleen een product zonder waarde kan worden verwijderd.')
        return
      }
      const name = drink.name
      const del = window.confirm(`Je gaat het ${name} verwijderen. Weet je het zeker?`)
      if (del) {
        myfetch(`api/products/${this.checked_name}`, 'DELETE')
          .then((res) => {
            DrinksUpdate()
            this.selectFirstDrink()
          })
          .catch(error => {
            this.showError(error.data.detail)
          })
      }
    }
  },
  template: `
  <div id='stock'>
    <div id='drink-selector'>
      <drinks
        v-bind:drinks=orderedDrinks
        v-bind:checked_name=checked_name
        v-on:add-drink=changeChecked
      ></drinks>
      <drinkprops
        v-bind:drinks=drinks
        v-bind:id=checked_name
      ></drinkprops>
    </div>
    <div id='drink-edits'>
      <colorful-button
        v-bind:id="'new-drink'"
        v-bind:flashClass="'flash'"
        v-on:c-button-click="newDrink"
      >
      Nieuw product
      </colorful-button>
      <colorful-button
        v-bind:id="'del-drink'"
        v-on:c-button-click="delDrink"
        v-bind:flashClass="'flash'"
      >
      Verwijder product
      </colorful-button>
      <div class="flex_vcenter c-button">
        Totale waarde: {{totalValue}}
      </div>
      <div class="flex_vcenter c-button">
        Totale winst: {{totalProfit}}
      </div>
    </div>
    <modal ref="errormodal">
      <div slot="header" class="modal-default-header">
        Error
      </div>
      <p slot="body" class="modal-default-body">{{ errormsg }}</p>
    </modal>
  </div>
 `
})

Vue.component('paymentoverview', {
  data: function () {
    return {
      db_data: DatabaseData,
      selected_payment: null
    }
  },
  methods: {
    itemSelected (value) {
      this.selected_payment = value
    },
    reload () {
      this.selected_payment = null
      this.$root.$emit('paymentdeselected')
      PaymentsUpdate()
    }
  },
  computed: {
    selectedItems: function () {
      if (this.selected_payment == null) {
        return null
      }
      return this.itemsByDate[this.selected_payment]
    },
    itemsByDate: function () {
      const dates = {}
      Object.entries(this.items).forEach(([key, payment]) => {
        const date = IsoToDate(payment.datetime)
        if (date in dates) {
          dates[date].push(payment)
        } else {
          dates[date] = [payment]
        }
      })
      return dates
    },
    items: function () {
      return objectMap(this.db_data.payments, (payment) => {
        return Object.assign(payment, { username: this.db_data.persons[payment.user_id].name })
      })
    }
  },
  template: `
  <div id="paymentoverview" class="windowcontained">
    <paymentoverviewselector
      :items = itemsByDate
      v-on:PaymentOverviewSelectorUpdate='itemSelected'
    >
    </paymentoverviewselector>
    <paymentovervieweditor
      :items = selectedItems
      v-on:reload=reload
    >
    </paymentovervieweditor>
  </div>
  `
})

Vue.component('metrics', {
  data: function () {
    return {
      metrics: {}
    }
  },
  created: function () {
    myfetch('api/metrics/')
      .then(response => {
        if (!response.ok) {
          throw new Error()
        }
        return response.json()
      })
      .then(data => {
        this.metrics = data
      })
      .catch((error) => {
        console.error(`Response from server was not valid. Retrying in ${RETRY_TIME / 1000} seconds.`)
        console.error(error)
      })
  },
  computed: {
    balance: function () { return format(IntToCurrency(this.metrics.user_balance)) },
    unverified_payments: function () { return format(IntToCurrency(-this.metrics.unverified_payments)) },
    restitution: function () { return format(IntToCurrency(this.metrics.user_restitution)) },
    product_profit: function () { return format(IntToCurrency(this.metrics.product_profit)) },
    projected_product_value: function () { return format(IntToCurrency(this.metrics.projected_product_value)) },
    sellable_product_value: function () { return format(IntToCurrency(this.metrics.sellable_product_value)) },
    total_value: function () { return format(IntToCurrency(this.metrics.total_value)) },
    projected_value: function () { return format(IntToCurrency(this.metrics.projected_value)) },
    negative_balance: function () { return format(IntToCurrency(this.metrics.negative_balance)) },
    total_invested: function () { return format(IntToCurrency(this.metrics.total_invested)) }
  },
  template: `
  <div id="metrics" class="windowcontained">
    <div class="element">
      <h3>Gebruikers</h3>
      <p>Saldo van alle gebruikers: {{balance}}</p>
      <p>Waarde openstaande betalingen: {{unverified_payments}}</p>
      <p>Waarde roodstaande gebruikers: {{negative_balance}}</p>
      <p>Potentiele restitutie: {{restitution}}</p>
      <h3>Producten</h3>
      <p>Totale verkoopwaarde van de voorraad: {{sellable_product_value}}</p>
      <p>Huidige winst van alle producten: {{product_profit}}</p>
      <p>Verwachte* totale winst van alle producten: {{projected_product_value}}</p>
      <h3>Systeem</h3>
      <p>Totaal op de rekening: {{total_invested}}</p>
      <p>Huidige winst: {{total_value}}</p>
      <p>Mogelijke* winst: {{projected_value}}</p>
      <br></br>
      <p>*Verwachte waarde: Na verkoop van alle producten tegen de huidige prijs</p>
      <p>*Mogelijke waarde: Na verkoop van alle producten binnen het huidige gebruikers saldo</p>
    </div>
  </div>
  `
})

// Basic selector element class
Vue.component('selector', {
  props: ['id', 'name', 'selected'],
  methods: {
    updateSelector (event) {
      this.$emit('selector-update', this.id)
    }
  },
  computed: {
    isActive: function () {
      return this.id === this.selected
    }
  },
  template: `
  <span 
  :class="{selected: isActive}"
  @click="updateSelector"
  >{{name}}</span>
  `
})

// Load the values for persons and drinks
function LoadVue () {
  // eslint-disable-next-line no-unused-vars
  const vm = new Vue({
    el: '#main',
    data: {
      selectors: selectors,
      selected: 0
    },
    methods: {
      selectorUpdate (value) {
        this.selected = value
      }
    }
  })
}

window.onload = function () {
  DatabaseUpdate()
  LoadVue()
}
