export function NumToMonth (number) {
  return ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'][number - 1]
}

export function IsoToDate (datetime) {
  return datetime.substring(0, datetime.indexOf('T'))
}
export function DateToHumanReadable (datetime) {
  const date = datetime.split('-')
  date[1] = NumToMonth(date[1])
  return date.reverse().join(' ')
}

export function DateToHumanSimple (datetime) {
  return datetime.split('-').reverse().join('-')
}
