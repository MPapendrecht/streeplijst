const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'EUR'
})

function format (value) {
  return formatter.format(value)
}

function IntToCurrency (value) {
  return Number(value / 100).toFixed(2)
}

function CurrencyToInt (value) {
  return parseInt(parseFloat(value).toFixed(2).replace('.', ''))
}

function splitEqual (value, NumParts) {
  const result = Array(NumParts).fill(Math.floor(value / NumParts))
  for (let i = 0; i < (value % NumParts); i++) {
    result[i] += 1
  }
  return result
}

export { format, IntToCurrency, CurrencyToInt, splitEqual }
