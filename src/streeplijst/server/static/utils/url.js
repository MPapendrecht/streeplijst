function RootUrl (url) {
  const str = window.location.pathname
  const host = str.substring(0, str.lastIndexOf('/'))
  return host + '/' + url
//  return window.location.protocol + '//' + window.location.host + '/' + url
}

async function myfetch (url, method, params) {
  if (method === undefined) {
    method = 'GET'
  }
  let query
  if (params === undefined) {
    query = window.fetch(RootUrl(url), { method: method })
  } else {
    query = window.fetch(RootUrl(url) + '?' + new URLSearchParams(params), { method: method })
  }
  const res = await query
  if (!res.ok) {
    const data = await res.json()
    const error = new Error('There was some error when communicating with the server.')
    error.data = data
    throw error
  }
  return res
}

export { RootUrl, myfetch }
