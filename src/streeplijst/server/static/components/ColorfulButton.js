Vue.component('colorful-button', {
  props: {
    flashClass: String,
    classes: {
      type: Array,
      required: false
    }
  },
  data: function () {
    return {
      enabled: false
    }
  },
  computed: {
    displayClass: function () {
      let standard = [{ flash: this.enabled }, 'c-button']
      if (this.classes != null) {
        standard = [...standard, ...this.classes]
      }
      return standard
    }
  },
  methods: {
    onClick: function () {
      this.enabled = true
      this.$emit('c-button-click')
    },
    offClick: function () {
      setTimeout(() => {
        this.enabled = false
      }, 200)
    }
  },
  template: `
    <button
      v-bind:class = "displayClass"
      @click="onClick"
      @click.release="offClick"
    >
      <slot></slot>
    </button>
  `
})
