import { format } from '../utils/currency.js'
import { DateToHumanSimple, IsoToDate } from '../utils/dates.js'
Vue.component('paymentovervieweditoritem', {
  data: function () {
    return {
      verified: false,
      validation_date: null
    }
  },
  props: {
    id: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    amount: {
      type: Number,
      required: true
    },
    datetime: {
      type: String,
      required: false
    }
  },
  computed: {
    displayAmount: function () {
      return format(this.amount / 100)
    },
    displayText: function () {
      if (this.verified) {
        return `${this.name} heeft ${this.displayAmount} betaald.`
      } else {
        return `${this.name} moet nog ${this.displayAmount} betalen.`
      }
    },
    displayDate: function () {
      if (this.validation_date != null) {
        return DateToHumanSimple(this.validation_date)
      }
      if (this.datetime == null) {
        return 'Onbekend'
      }
      return DateToHumanSimple(IsoToDate(this.datetime))
    }
  },
  methods: {
    errorDate: function (value) {
      console.log('invalid date')
    },
    saveDate: function (value) {
      this.validation_date = value
      this.$emit('paymentovervieweditoritemsaved', { id: this.id, date: this.validation_date })
    }
  },
  template: `
    <div class='element list'>
    <span>
      {{this.displayText}}
    </span>
    <span class='single-line-editable'>
      <p>Betaald op:</p>
      <pre> </pre>
      <editable-text
        :value=this.displayDate
        :format=/.*/
        :onError=errorDate
        :onSave=saveDate
        :type='"date"'
      ></editable-text>
    </span>
    </div>
  `

})
