import { DateToHumanReadable } from '../utils/dates.js'

Vue.component('paymentoverviewselector', {
  data: function () {
    return {
      selected: null,
      clearing: null
    }
  },
  props: {
    items: {
      type: Object,
      required: true
    }
  },
  mounted () {
    this.$root.$on('paymentdeselected', () => {
      this.selected = null
    })
  },
  methods: {
    itemSelected ({ id }) {
      if (this.clearing != null) {
        window.clearTimeout(this.clearing)
      }
      if (this.clearing != null && this.selected === id) {
        this.selected = null
        this.clearing = null
      } else {
        this.selected = id
        this.clearing = window.setTimeout(() => { this.clearing = null }, 300)
      }
      this.$emit('PaymentOverviewSelectorUpdate', this.selected)
    },
    isSelected (identifier) {
      return identifier === this.selected
    }
  },
  computed: {
    displayItems: function () {
      return Object.keys(this.items).map(
        (identifier) => {
          return {
            key: identifier,
            label: DateToHumanReadable(identifier)
          }
        }).sort((a, b) => a.key.localeCompare(b.key))
    },
    style: function () {
      return ['list', 'element', 'flex_size_1_of_5']
    }
  },
  template: `
  <ul :class=style v-dragscroll>
    <paymentoverviewselectoritem
      v-for='item in displayItems'
      :key=item.key
      :id=item.key
      :label=item.label
      :selected=isSelected(item.key)
      v-on:PaymentOverviewSelectorItemUpdate='itemSelected'
    >
    </paymentoverviewselectoritem>
  </ul>
  `

})
