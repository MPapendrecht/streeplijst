import { RootUrl } from '../utils/url.js'
Vue.component('editable-text', {
  props: {
    value: null,
    format: RegExp,
    onError: Function,
    onSave: Function,
    type: {
      type: String,
      required: false
    }
  },
  data: function () {
    return {
      tempValue: null,
      editing: false,
      icon: RootUrl('pen.svg')
    }
  },
  methods: {
    enableEditing: function () {
      this.tempValue = this.value
      this.editing = true
      this.$nextTick(() => {
        this.$refs.input.focus()
      })
    },
    disableEditing: function () {
      this.tempValue = null
      this.editing = false
    },
    saveEdit: function () {
      // However we want to save it to the database
      if (!this.format.test(this.tempValue)) {
        console.log(this.format)
        console.log(this.tempValue)
        this.onError()
        return
      }
      this.$emit('value-change', this.tempValue)
      this.onSave(this.tempValue)
      this.disableEditing()
    }
  },
  template: `
    <div v-if="!editing" class='editable-text'>
      <div @click="enableEditing" class="icon-container">
        <p class="editable-text-body">{{value}}</p>
        <img v-bind:src="icon" class="icon editable-text-icon">
      </div>
    </div>
    <div v-else class='editable-text'>
      <input :type="type" ref="input" v-model="tempValue" class="input"/>
      <button @click="disableEditing"> Cancel </button>
      <button @click="saveEdit"> Save </button>
    </div>
  `
})
