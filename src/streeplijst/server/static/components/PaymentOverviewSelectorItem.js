Vue.component('paymentoverviewselectoritem', {
  data: function () {
    return {
      touchdata: {}
    }
  },
  props: {
    id: {
      type: String,
      required: true
    },
    label: {
      type: String,
      required: true
    },
    selected: {
      type: Boolean,
      required: true
    }
  },
  computed: {
    style: function () {
      return [{ selected: this.selected }]
    }
  },
  methods: {
    startTouch (event) {
      this.touchdata.x = event.clientX
      this.touchdata.y = event.clientY
    },
    endTouch (event) {
      const dy = this.touchdata.y - event.clientY
      if (dy * dy < 10) {
        this.updateSelector()
      }
    },
    updateSelector (event) {
      this.$emit('PaymentOverviewSelectorItemUpdate', { id: this.id })
    }
  },
  template: `
    <button
    :class=style
    @mousedown="startTouch"
    @mouseup="endTouch"
    >
      {{label}}
    </button>
  `
})
