import './PaymentOverviewEditorItem.js'
import { myfetch } from '../utils/url.js'
Vue.component('paymentovervieweditor', {
  data: function () {
    return {
      changedItems: {}
    }
  },
  props: {
    items: {
      type: Array,
      required: false
    }
  },
  methods: {
    saveItems: function () {
      Object.entries(this.changedItems).forEach(([id, date]) => {
        myfetch(`api/payments/${id}/verify?validation_datetime=${date}T00:00:00`, 'POST')
      })
      this.changedItems = {}
      this.$emit('reload')
    },
    itemChanged: function ({ id, date }) {
      this.changedItems[id] = date
    }
  },
  computed: {
    sortedItems: function () {
      return [...this.items].sort((a, b) => a.username.localeCompare(b.username))
    }
  },
  template: `
    <div class='element flex_size_4_of_5' v-if='this.items == null'>Select a date first</div>
    <div class='element list flex_size_4_of_5' v-else>
      <ul class='list' v-dragscroll>
        <paymentovervieweditoritem v-for='item in this.sortedItems'
          :key=item.identifier
          :id=item.identifier
          :name=item.username
          :amount=item.amount
          :datetime=item.validation_datetime
          v-on:paymentovervieweditoritemsaved=itemChanged
        ></paymentovervieweditoritem>
      </ul>
      <colorful-button
        v-bind:flashClass="'flash'"
        v-on:c-button-click="saveItems"
        v-bind:classes="['small', 'end']"
      >
        <p>Betalingen verwerken</p>
      </colorful-button>
    </div>
  `
})
