"""
The api package contains all code necessary to link the domain model to
external services.
If the api has a 1 to 1 relationship to the domain, it should have a matching name.
Not everything in the domain has to be exposed via the api, and not everything
in the api has to match with the domain.

Every api must have a service module.

"""

from typing import Tuple, Type

from streeplijst.eventsourcing.fact import Fact


def init_fact_types() -> Tuple[Type[Fact], ...]:
    from streeplijst.api.payments import facts as pay_facts
    from streeplijst.api.products import facts as prd_facts
    from streeplijst.api.users import facts as u_facts

    return *pay_facts.all_facts, *prd_facts.all_facts, *u_facts.all_facts
