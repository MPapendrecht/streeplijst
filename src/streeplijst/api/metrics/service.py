from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService


class MetricService:
    def __init__(
        self,
        user_service: UserService,
        product_service: ProductService,
        payment_service: PaymentService,
    ):
        self._user_service = user_service
        self._product_service = product_service
        self._payment_service = payment_service

    def get_total_balance(self):
        return sum(ua.user().balance for ua in self._user_service.all())

    def get_total_unverified_payments(self):
        return sum(pm.amount for (pm, _) in self._payment_service.unverified_payments())

    def get_total_product_profit(self):
        return sum(p.product().profit for p in self._product_service.all())

    def get_total_product_value(self):
        return sum(p.product().value for p in self._product_service.all())

    def get_negative_balance(self):
        balances = (ua.user().balance for ua in self._user_service.all())
        return sum(filter(lambda x: x < 0, balances))

    def get_total_value(self):
        return self.get_total_product_profit() - self.get_total_restitution()

    def get_sellable_product_value(self):
        return self.get_total_product_value() - self.get_total_product_profit()

    def get_projected_value(self):
        profit = self.get_total_product_profit()
        value = self.get_total_product_value()
        balance = self.get_total_balance()
        negative_balance = self.get_negative_balance()
        return profit + min(value - profit, balance) - negative_balance

    def get_total_restitution(self):
        return self.get_total_balance() - self.get_total_unverified_payments()

    def get_total_invested(self):
        return self.get_total_restitution() + self.get_total_product_profit()
