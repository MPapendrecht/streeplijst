"""

This module defines the state related to the user domain.
It processes the facts in order to produce a state.
These states must be read-only.

"""
from copy import deepcopy
from typing import Any, Callable, Dict, Type

from streeplijst.api.payments import facts as payfacts
from streeplijst.api.users import facts as ufacts
from streeplijst.configs.config import config
from streeplijst.domain import bank as bnk
from streeplijst.domain import product as prd
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util import secrets
from streeplijst.util.translate import LANGUAGES


class InvalidUserIdentifier(exc.MultiLanguageException):
    """Exception when the identifier is invalid."""


class UserAggregate(agg.AutoAggregate):
    def __init__(
        self,
        factstore: fcs.FactStore,
        bank: bnk.Bank,
        vault: secrets.Vault,
        identifier: idd.Identifier,
    ):
        if not isinstance(identifier, idd.Identifier):
            raise InvalidUserIdentifier().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The identifier {identifier} is not a valid identifier.",
            ).with_error_msg(LANGUAGES.DUTCH, f"Het id {identifier} is geen geldig id.")
        self._identifier = identifier
        self._bank = bank
        self._vault = vault
        self._fact_handlers: Dict[Type[fct.Fact], Callable[[Any], Any]] = {
            ufacts.UserCreated: self._handle_create_user,
            ufacts.UserNameChanged: self._handle_name,
            ufacts.UserBalanceLimitChanged: self._handle_balance_limit,
            ufacts.UserAgeChanged: self._handle_age,
            ufacts.UserMailChanged: self._handle_mail,
            payfacts.UserMakesPayment: self._handle_payment,
            ufacts.UsersBuysProduct: self._handle_buy_product,
            ufacts.UserDeleted: self._handle_deletion,
            ufacts.UserAllowedNegativeBalance: self._handle_allow_negative,
        }
        self._user: usr.User
        super(UserAggregate, self).__init__(
            factstore, self.fact_descriptors(identifier)
        )

    def fact_descriptors(self, identifier: idd.Identifier):
        return (
            *(
                fcs.FactDescriptor(ftype, subject=identifier)
                for ftype in ufacts.all_facts
            ),
            fcs.FactDescriptor(payfacts.UserMakesPayment, user_id=self._identifier),
            fcs.FactDescriptor(
                ufacts.UsersBuysProduct,
                other_users=lambda others: any(
                    user_id == identifier for user_id in others.keys()
                ),
            ),
        )

    def _handle_create_user(self, fact: ufacts.UserCreated):
        self._user = usr.User(
            identifier=fact.subject,
            name=fact.name,
            balance=fact.balance,
            balance_limit=fact.balance_limit,
            age=fact.age,
            allow_negative=fact.allow_negative,
            mail="",
            vault=self._vault,
        )
        self._user.locked_mail = fact.mail

    def _handle_name(self, fact: ufacts.UserNameChanged):
        self._user.name = fact.name

    def _handle_balance_limit(self, fact: ufacts.UserBalanceLimitChanged):
        self._user.balance_limit = fact.balance_limit

    def _handle_age(self, fact: ufacts.UserAgeChanged):
        self._user.age = fact.age

    def _handle_mail(self, fact: ufacts.UserMailChanged):
        self._user.locked_mail = fact.mail

    def _handle_payment(self, fact: payfacts.UserMakesPayment):
        payment = self._bank.prepare_payment(
            self._user, fact.amount, identifier=fact.payment_id, datetime=fact.datetime
        )
        self._bank.process_payment(self._user, payment)

    def _handle_allow_negative(self, fact: ufacts.UserAllowedNegativeBalance):
        self._user.allow_negative = fact.allow_negative

    def _handle_buy_product(self, fact: ufacts.UsersBuysProduct):
        # TODO: Can we avoid creating a product here?
        #   We assume now the buy method won't fail with these settings for any
        #   scenario.
        product = prd.Product(
            identifier=fact.product_id,
            name="temp product",
            stock=config().transactions.maximum_amount,
            stock_limit=0,
            req_adult=False,
            price=0,
            profit=0,
        )
        if self._user.identifier == fact.subject:
            price = fact.price - sum(fact.other_users.values())
            self._user.buy(product, fact.amount, price, fact.datetime)
        else:
            self._user.balance -= fact.other_users[self._user.identifier]

    def _handle_deletion(self, fact: ufacts.UserDeleted):
        self.unsubscribe()

    def handle_fact(self, fact: fct.Fact):
        if type(fact) not in self._fact_handlers:
            raise agg.FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )
        self._fact_handlers[type(fact)](fact)

    def user(self) -> usr.User:
        return deepcopy(self._user)
