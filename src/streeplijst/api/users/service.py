"""

This module contains the api for user instances.

"""
import logging
from typing import Dict, List, Optional, Tuple

from streeplijst.api.payments import facts as pft
from streeplijst.api.users import facts as uft
from streeplijst.api.users.aggregate import UserAggregate
from streeplijst.domain import bank as bnk
from streeplijst.domain import payment as pay
from streeplijst.domain import product as prd
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util import secrets
from streeplijst.util.translate import LANGUAGES

logger = logging.getLogger(__name__)


class UnknownUser(exc.MultiLanguageException):
    """Exception when we don't know who the user is."""


class CreatedUsersAggregate(agg.AutoAggregate):
    def __init__(self, factstore: fcs.FactStore, bank: bnk.Bank, vault: secrets.Vault):
        self._bank = bank
        self._vault = vault
        self._users: Dict[idd.Identifier, UserAggregate] = {}
        super(CreatedUsersAggregate, self).__init__(
            factstore,
            (
                fcs.FactDescriptor(uft.UserCreated),
                fcs.FactDescriptor(uft.UserDeleted),
            ),
        )

    def handle_fact(self, fact: fct.Fact):
        if isinstance(fact, uft.UserCreated):
            self._users[fact.subject] = UserAggregate(
                self._factstore, self._bank, self._vault, fact.subject
            )
        elif isinstance(fact, uft.UserDeleted):
            self._bank.delete_user(self._users[fact.subject].user())
            del self._users[fact.subject]
        else:
            raise agg.FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )

    @property
    def all(self) -> List[UserAggregate]:
        return list(self._users.values())

    def find_by_id(self, identifier: idd.Identifier):
        if identifier not in self._users:
            raise UnknownUser().with_error_msg(
                LANGUAGES.ENGLISH, f"User with identifier {identifier} not found."
            ).with_error_msg(
                LANGUAGES.DUTCH, f"Geen gebruiker gevonden met id {identifier}"
            )
        return self._users[identifier]


class UserService:
    """Keeps track of all the users in the system."""

    def __init__(self, factstore: fcs.FactStore, bank: bnk.Bank, vault: secrets.Vault):
        self._factstore = factstore
        self._bank = bank
        self._vault = vault
        self._users = CreatedUsersAggregate(factstore, bank, self._vault)

    def create_new_user(self, name, balance, balance_limit, age, allow_negative, mail):
        """
        Validates the users data, creates a stream and returns the aggregate.
        Parameters
        ----------
        name
        balance
        balance_limit
        age
        allow_negative
        mail

        Returns
        -------
        UserAggregate

        """
        identifier = idd.Identifier()
        logger.debug(f"Creating new user with identifier {identifier}")
        user = usr.User(
            identifier,
            name,
            balance,
            balance_limit,
            age,
            allow_negative,
            mail,
            self._vault,
        )
        self._factstore.add_fact(uft.UserCreated(user))
        return self._users.find_by_id(identifier)

    def _find_user(self, identifier: idd.Identifier):
        return self._users.find_by_id(identifier).user()

    def delete_user(self, identifier: idd.Identifier):
        user = self._find_user(identifier)
        self._bank.delete_user(user)
        user.delete()
        self._factstore.add_fact(uft.UserDeleted(user))

    def set_name(self, identifier: idd.Identifier, name):
        user = self._find_user(identifier)
        user.name = name
        self._factstore.add_fact(uft.UserNameChanged(user))

    def set_age(self, identifier: idd.Identifier, age):
        user = self._find_user(identifier)
        user.age = age
        self._factstore.add_fact(uft.UserAgeChanged(user))

    def set_mail(self, identifier: idd.Identifier, mail: str):
        user = self._find_user(identifier)
        user.mail = mail
        self._factstore.add_fact(uft.UserMailChanged(user))

    def set_balance_limit(self, identifier: idd.Identifier, balance_limit):
        user = self._find_user(identifier)
        user.balance_limit = balance_limit
        self._factstore.add_fact(uft.UserBalanceLimitChanged(user))

    def set_allow_negative(self, identifier: idd.Identifier, allow_negative):
        user = self._find_user(identifier)
        user.allow_negative = allow_negative
        self._factstore.add_fact(uft.UserAllowedNegativeBalance(user, allow_negative))

    def payments(self, user_id: idd.Identifier) -> Tuple[pay.Payment, ...]:
        user = self._find_user(user_id)
        return self._bank.payments(user)

    def unverified_payments(self, user_id: idd.Identifier) -> Tuple[pay.Payment, ...]:
        user = self._find_user(user_id)
        return self._bank.unverified_payments(user)

    def make_payment(self, identifier: idd.Identifier, amount: int) -> idd.Identifier:
        user = self._find_user(identifier)
        payment = self._bank.prepare_payment(user, amount)
        self._factstore.add_fact(pft.UserMakesPayment(payment, user))
        return payment.identifier

    def buy(
        self,
        user: usr.User,
        product: prd.Product,
        amount: int,
        price: int,
        dry_run: bool = False,
        other_users: Optional[Dict[usr.User, int]] = None,
    ) -> Optional[uft.UsersBuysProduct]:
        if amount == 0 and price == 0:
            return None
        if other_users is None:
            other_users = {}
        for other_user, other_price in other_users.items():
            other_user.transfer(other_price, user)
        user.buy(product, amount, price)
        fact = uft.UsersBuysProduct(user, other_users, product, amount, price)
        if not dry_run:
            self._factstore.add_fact(fact)
            return None
        return fact

    def commit_buy(self, transactions: List[uft.UsersBuysProduct]) -> None:
        for transaction in transactions:
            self._factstore.add_fact(transaction)

    def find_by_id(self, identifier: idd.Identifier):
        """
        Find a users by its id.

        Parameters
        ----------
        identifier

        Returns
        -------
        UserAggregate

        """
        return self._users.find_by_id(identifier)

    def all(self):
        """
        Retrieve all users.

        Returns
        -------
        List[UserAggregate]

        """
        return self._users.all
