"""

This module defines the facts related to the users domain.

"""
from typing import Dict

from streeplijst.domain import product as prd
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import fact as fct
from streeplijst.util.frozen_property import frozen_property
from streeplijst.util.translate import LANGUAGES


class UserCreatedV0(fct.Fact, label="UserCreated"):
    def __init__(self, user: usr.User):
        super().__init__(subject=user.identifier)
        self._name = user.name
        self._balance = user.balance
        self._balance_limit = user.balance_limit
        self._age = user.age

    @frozen_property
    def name(self):
        return self._name

    @frozen_property
    def balance(self):
        return self._balance

    @frozen_property
    def balance_limit(self):
        return self._balance_limit

    @frozen_property
    def age(self):
        return self._age

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UserCreatedV0):
            kwargs.update(
                {
                    "_name": fact.name,
                    "_balance": fact.balance,
                    "_balance_limit": fact.balance_limit,
                    "_age": fact.age,
                }
            )
        if "_name" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_name is required for upcasting."
            )
        if "_balance" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_balance is required for upcasting."
            )
        if "_balance_limit" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_balance_limit is required for upcasting."
            )
        if "_age" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_age is required for upcasting."
            )
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {
            "name": self.name,
            "balance": self.balance,
            "balance_limit": self.balance_limit,
            "age": self.age,
            **kwargs,
        }
        return super().__repr__(**kwargs)


class UserCreatedV1(UserCreatedV0):
    def __init__(self, user: usr.User):
        super().__init__(user)
        self._allow_negative = user.allow_negative

    @frozen_property
    def allow_negative(self):
        return self._allow_negative

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UserCreatedV1):
            kwargs.update({"_allow_negative": fact.allow_negative})
        if "_allow_negative" not in kwargs:
            kwargs["_allow_negative"] = False
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {"allow_negative": self.allow_negative, **kwargs}
        return super().__repr__(**kwargs)


class UserCreatedV2(UserCreatedV1):
    def __init__(self, user: usr.User):
        super().__init__(user)
        self._mail = user.locked_mail

    @frozen_property
    def mail(self):
        return self._mail

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UserCreatedV2):
            kwargs.update({"_mail": fact.mail})
        if "_mail" not in kwargs:
            kwargs["_mail"] = ""
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {"mail": self.mail, **kwargs}
        return super().__repr__(**kwargs)


UserCreated = UserCreatedV2


class UserDeleted(fct.Fact, label="UserDeleted"):
    def __init__(self, user: usr.User):
        super(UserDeleted, self).__init__(subject=user.identifier)


class UserNameChanged(fct.Fact, label="UserNameChanged"):
    def __init__(self, user: usr.User):
        super(UserNameChanged, self).__init__(subject=user.identifier)
        self._name = user.name

    @frozen_property
    def name(self):
        return self._name

    def __repr__(self, **kwargs):
        kwargs = {"name": self.name, **kwargs}
        return super(UserNameChanged, self).__repr__(**kwargs)


class UserBalanceLimitChanged(fct.Fact, label="UserBalanceLimitChanged"):
    def __init__(self, user: usr.User):
        super(UserBalanceLimitChanged, self).__init__(subject=user.identifier)
        self._balance_limit = user.balance_limit

    @frozen_property
    def balance_limit(self):
        return self._balance_limit

    def __repr__(self, **kwargs):
        kwargs = {"balance_limit": self.balance_limit, **kwargs}
        return super(UserBalanceLimitChanged, self).__repr__(**kwargs)


class UserAgeChanged(fct.Fact, label="UserAgeChanged"):
    def __init__(self, user: usr.User):
        super(UserAgeChanged, self).__init__(subject=user.identifier)
        self._age = user.age

    @frozen_property
    def age(self):
        return self._age

    def __repr__(self, **kwargs):
        kwargs = {"age": self.age, **kwargs}
        return super(UserAgeChanged, self).__repr__(**kwargs)


class UserMailChanged(fct.Fact, label="UserMailChanged"):
    def __init__(self, user: usr.User):
        super(UserMailChanged, self).__init__(subject=user.identifier)
        self._mail = user.locked_mail

    @frozen_property
    def mail(self):
        return self._mail

    def __repr__(self, **kwargs):
        kwargs = {"mail": self.mail, **kwargs}
        return super(UserMailChanged, self).__repr__(**kwargs)


class UserBuysProduct(fct.Fact, label="UserBuysProduct"):
    def __init__(self, user: usr.User, product: prd.Product, amount: int, price: int):
        super(UserBuysProduct, self).__init__(subject=user.identifier)
        self._product_id = product.identifier
        self._amount = amount
        self._price = price

    @frozen_property
    def product_id(self):
        return self._product_id

    @frozen_property
    def amount(self):
        return self._amount

    @frozen_property
    def price(self):
        return self._price

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UserBuysProduct):
            kwargs.update(
                {
                    "_product_id": fact._product_id,
                    "_amount": fact._amount,
                    "_price": fact._price,
                }
            )
        if "_product_id" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_product_id is required for upcasting."
            )
        if "_amount" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_amount is required for upcasting."
            )
        if "_price" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_price is required for upcasting."
            )
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {
            "product_id": self.product_id,
            "amount": self.amount,
            "price": self.price,
            **kwargs,
        }
        return super().__repr__(**kwargs)


class UsersBuysProduct(UserBuysProduct):
    def __init__(
        self,
        user: usr.User,
        other_users: Dict[usr.User, int],
        product: prd.Product,
        amount: int,
        price: int,
    ):
        super(UsersBuysProduct, self).__init__(user, product, amount, price)
        self._other_users = {
            other_user.identifier: price for other_user, price in other_users.items()
        }

    @frozen_property
    def other_users(self):
        return self._other_users

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UsersBuysProduct):
            kwargs.update({"_other_users": fact.other_users})
        if "_other_users" not in kwargs:
            kwargs["_other_users"] = {}
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {
            "other_users": self.other_users,
            **kwargs,
        }
        return super().__repr__(**kwargs)


class UserAllowedNegativeBalance(fct.Fact, label="UserAllowedNegativeBalance"):
    def __init__(self, user: usr.User, allow_negative: bool):
        super().__init__(subject=user.identifier)
        self._allow_negative = allow_negative

    @frozen_property
    def allow_negative(self):
        return self._allow_negative

    def __repr__(self, **kwargs):
        kwargs = {"allow_negative": self.allow_negative, **kwargs}
        return super().__repr__(**kwargs)


all_facts = (
    UserCreatedV0,
    UserCreatedV1,
    UserCreatedV2,
    UserNameChanged,
    UserBalanceLimitChanged,
    UserAgeChanged,
    UserMailChanged,
    UserBuysProduct,
    UsersBuysProduct,
    UserDeleted,
    UserAllowedNegativeBalance,
)
for fact in all_facts:
    fct.register_fact_definition(fact)
