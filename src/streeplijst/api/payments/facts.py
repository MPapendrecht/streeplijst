"""

This module defines the facts related to the domain of payments.

"""
import datetime as dt

from streeplijst.domain import payment as pay
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import fact as fct
from streeplijst.util import identifier as idd
from streeplijst.util.frozen_property import frozen_property
from streeplijst.util.translate import LANGUAGES


class UserMakesPaymentV0(fct.Fact, label="UserMakesPayment"):
    def __init__(self, payment: pay.Payment, user: usr.User):
        super().__init__(subject=payment.identifier, creation_datetime=payment.datetime)
        self._user_id = user.identifier
        self._amount = payment.amount

    @frozen_property
    def amount(self):
        return self._amount

    @frozen_property
    def user_id(self):
        return self._user_id

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs) -> "UserMakesPaymentV0":
        if isinstance(fact, UserMakesPaymentV0):
            kwargs.update(
                {
                    "_amount": fact.amount,
                    "_user_id": fact.user_id,
                }
            )
        if "_amount" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH,
                f"Missing an amount in order to cast a {type(fact)} to {cls}",
            )
        if "_user_id" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH,
                f"Missing a user_id in order to cast a {type(fact)} to {cls}",
            )
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {"user_id": self.user_id, "amount": self.amount, **kwargs}
        return super().__repr__(**kwargs)


class UserMakesPaymentV1(UserMakesPaymentV0):
    def __init__(self, payment: pay.Payment, user: usr.User):
        super().__init__(payment, user)
        self._payment_id = payment.identifier

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, UserMakesPaymentV1):
            kwargs.update({"_payment_id": fact._payment_id})
        if "_payment_id" not in kwargs:
            kwargs.update({"_payment_id": fact.subject})
        return super()._cast(fact, **kwargs)

    @frozen_property
    def payment_id(self):
        return self._payment_id

    def __repr__(self, **kwargs):
        kwargs = {"payment_id": self.payment_id, **kwargs}
        return super().__repr__(**kwargs)


UserMakesPayment = UserMakesPaymentV1


class PaymentVerifiedV0(fct.Fact, label="PaymentVerified"):
    def __init__(self, identifier):
        super().__init__(subject=identifier)


class PaymentVerifiedV1(PaymentVerifiedV0):
    def __init__(self, identifier: idd.Identifier, validation_datetime: dt.datetime):
        super().__init__(identifier=identifier)
        self._validation_datetime = validation_datetime

    @frozen_property
    def validation_datetime(self):
        return self._validation_datetime

    @classmethod
    def _cast(cls, fact: fct.Fact, **kwargs):
        if isinstance(fact, PaymentVerifiedV1):
            kwargs.update({"_validation_datetime": fact.validation_datetime})
        if "_validation_datetime" not in kwargs:
            raise fct.IncompleteDataForCasting().with_error_msg(
                LANGUAGES.ENGLISH, "_validation datetime required to upcast this fact."
            )
        return super()._cast(fact, **kwargs)

    def __repr__(self, **kwargs):
        kwargs = {"validation_datetime": self.validation_datetime, **kwargs}
        return super().__repr__(**kwargs)


PaymentVerified = PaymentVerifiedV1

all_facts = (
    UserMakesPaymentV0,
    UserMakesPaymentV1,
    PaymentVerifiedV0,
    PaymentVerifiedV1,
)
for fact in all_facts:
    fct.register_fact_definition(fact)
