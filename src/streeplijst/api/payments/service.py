import datetime as dt
from typing import Tuple

from streeplijst.api.payments import facts as pfct
from streeplijst.domain import bank as bnk
from streeplijst.domain import payment as pay
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES


class CreatedPaymentsAggregate(agg.AutoAggregate):
    def __init__(self, factstore, bank: bnk.Bank):
        self._bank = bank
        super(CreatedPaymentsAggregate, self).__init__(
            factstore,
            (fcs.FactDescriptor(pfct.PaymentVerified),),
        )

    def handle_fact(self, fact: fct.Fact):
        if isinstance(fact, pfct.PaymentVerified):
            self._bank.verify_payment(fact.subject, fact.validation_datetime)
        else:
            raise agg.FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )


class PaymentService:
    """Keeps track of all the payments in the system."""

    def __init__(self, factstore: fcs.FactStore, bank: bnk.Bank):
        self._factstore = factstore
        self._bank = bank
        self._payment_aggregate = CreatedPaymentsAggregate(factstore, bank)

    def verify(self, payment_id: idd.Identifier, validation_date: dt.datetime):
        payment = self._bank.get_payment(payment_id)
        payment.verify(validation_date)
        self._factstore.add_fact(pfct.PaymentVerified(payment_id, validation_date))

    def unverified_payments(self) -> Tuple[Tuple[pay.Payment, usr.User], ...]:
        return tuple(
            (payment, self._bank.user(payment))
            for payment in self._bank.unverified_payments()
        )

    def all(self) -> Tuple[Tuple[pay.Payment, usr.User], ...]:
        return tuple(
            (payment, self._bank.user(payment)) for payment in self._bank.payments()
        )
