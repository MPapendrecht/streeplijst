from streeplijst.domain import product as prd
from streeplijst.eventsourcing import fact as fct
from streeplijst.util.frozen_property import frozen_property


class ProductCreated(fct.Fact, label="ProductCreated"):
    def __init__(self, product: prd.Product):
        super(ProductCreated, self).__init__(subject=product.identifier)
        self._name = product.name
        self._stock = product.stock
        self._stock_limit = product.stock_limit
        self._req_adult = product.req_adult
        self._profit = product.profit
        self._price = product.price

    @frozen_property
    def name(self):
        return self._name

    @frozen_property
    def stock(self):
        return self._stock

    @frozen_property
    def stock_limit(self):
        return self._stock_limit

    @frozen_property
    def req_adult(self):
        return self._req_adult

    @frozen_property
    def profit(self):
        return self._profit

    @frozen_property
    def price(self):
        return self._price

    def __repr__(self, **kwargs):
        kwargs = {
            "name": self.name,
            "stock": self.stock,
            "stock_limit": self.stock_limit,
            "req_adult": self.req_adult,
            "profit": self.profit,
            "price": self.price,
            **kwargs,
        }
        return super(ProductCreated, self).__repr__(**kwargs)


class ProductDeleted(fct.Fact, label="ProductDeleted"):
    def __init__(self, product: prd.Product):
        super(ProductDeleted, self).__init__(subject=product.identifier)


class ProductNameChanged(fct.Fact, label="ProductNameChanged"):
    def __init__(self, product: prd.Product):
        super(ProductNameChanged, self).__init__(subject=product.identifier)
        self._name = product.name

    @frozen_property
    def name(self):
        return self._name

    def __repr__(self, **kwargs):
        kwargs = {"name": self.name, **kwargs}
        return super(ProductNameChanged, self).__repr__(**kwargs)


class ProductStockLimitChanged(fct.Fact, label="ProductStockLimitChanged"):
    def __init__(self, product: prd.Product):
        super(ProductStockLimitChanged, self).__init__(product.identifier)
        self._stock_limit = product.stock_limit

    @frozen_property
    def stock_limit(self):
        return self._stock_limit

    def __repr__(self, **kwargs):
        kwargs = {"stock_limit": self.stock_limit, **kwargs}
        return super(ProductStockLimitChanged, self).__repr__(**kwargs)


class ProductAdultRequirementChanged(fct.Fact, label="ProductAdultRequirementChanged"):
    def __init__(self, product: prd.Product):
        super(ProductAdultRequirementChanged, self).__init__(product.identifier)
        self._req_adult = product.req_adult

    @frozen_property
    def req_adult(self):
        return self._req_adult

    def __repr__(self, **kwargs):
        kwargs = {"req_adult": self.req_adult, **kwargs}
        return super(ProductAdultRequirementChanged, self).__repr__(**kwargs)


class ProductPriceChanged(fct.Fact, label="ProductPriceChanged"):
    def __init__(self, product: prd.Product):
        super(ProductPriceChanged, self).__init__(product.identifier)
        self._price = product.price

    @frozen_property
    def price(self):
        return self._price

    def __repr__(self, **kwargs):
        kwargs = {"price": self.price, **kwargs}
        return super(ProductPriceChanged, self).__repr__(**kwargs)


class ProductStockBought(fct.Fact, label="ProductStockBought"):
    def __init__(self, product: prd.Product, amount: int, price: int):
        super(ProductStockBought, self).__init__(product.identifier)
        self._amount = amount
        self._price = price

    @frozen_property
    def amount(self):
        return self._amount

    @frozen_property
    def price(self):
        return self._price

    def __repr__(self, **kwargs):
        kwargs = {"amount": self.amount, "price": self.price, **kwargs}
        return super(ProductStockBought, self).__repr__(**kwargs)


all_facts = (
    ProductCreated,
    ProductNameChanged,
    ProductStockLimitChanged,
    ProductAdultRequirementChanged,
    ProductPriceChanged,
    ProductStockBought,
    ProductDeleted,
)
for fact in all_facts:
    fct.register_fact_definition(fact)
