"""

This module contains the api for product instances.

"""
from typing import Dict, List

from streeplijst.api.products import aggregate as pagg
from streeplijst.api.products import facts as pfct
from streeplijst.domain import product as prd
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES


class UnknownProduct(exc.MultiLanguageException):
    """Exception when we don't know what the product is."""


class CreatedProductAggregate(agg.AutoAggregate):
    def __init__(self, factstore: fcs.FactStore):
        self._products: Dict[idd.Identifier, pagg.ProductAggregate] = {}
        super(CreatedProductAggregate, self).__init__(
            factstore,
            (
                fcs.FactDescriptor(pfct.ProductCreated),
                fcs.FactDescriptor(pfct.ProductDeleted),
            ),
        )

    def handle_fact(self, fact: fct.Fact):
        if isinstance(fact, pfct.ProductCreated):
            self._products[fact.subject] = pagg.ProductAggregate(
                self._factstore, fact.subject
            )
        elif isinstance(fact, pfct.ProductDeleted):
            del self._products[fact.subject]
        else:
            raise agg.FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )

    @property
    def all(self) -> List[pagg.ProductAggregate]:
        return list(self._products.values())

    def find_by_id(self, identifier: idd.Identifier):
        if identifier not in self._products:
            raise UnknownProduct().with_error_msg(
                LANGUAGES.ENGLISH, f"Product with identifier {identifier} not found."
            ).with_error_msg(
                LANGUAGES.DUTCH, f"Geen product gevonden met id {identifier}."
            )
        return self._products[identifier]


class ProductService:
    """Keeps track of all the products in the system."""

    def __init__(self, factstore: fcs.FactStore):
        self._factstore = factstore
        self._products = CreatedProductAggregate(factstore)

    def create_new_product(
        self, name, stock, stock_limit, req_adult, price, profit
    ) -> pagg.ProductAggregate:
        """
        Validates the product data, creates a stream and returns the aggregate.

        Parameters
        ----------
        name
        stock
        stock_limit
        req_adult
        price
        profit

        Returns
        -------
        ProductAggregate

        """
        identifier = idd.Identifier()
        product = prd.Product(
            identifier, name, stock, stock_limit, req_adult, price, profit
        )
        self._factstore.add_fact(pfct.ProductCreated(product))
        return self._products.find_by_id(identifier)

    def find_by_id(self, identifier: idd.Identifier) -> pagg.ProductAggregate:
        """
        Find a product aggregate by its identifier.

        Parameters
        ----------
        identifier : Identifier
            The identifier of the product.

        Returns
        -------
        ProductAggregate :
            The product aggregate of this identifier.

        Raises
        ------
        UnknownProduct :
            When the identifier is not known by the system.

        """
        return self._products.find_by_id(identifier)

    def _find_product(self, identifier: idd.Identifier) -> prd.Product:
        return self.find_by_id(identifier).product()

    def all(self) -> List[pagg.ProductAggregate]:
        """
        Retrieve all products.

        Returns
        -------
        List[ProductAggregate]

        """
        return self._products.all

    def set_name(self, identifier: idd.Identifier, name):
        product = self._find_product(identifier)
        product.name = name
        self._factstore.add_fact(pfct.ProductNameChanged(product))

    def set_adult_requirement(self, identifier: idd.Identifier, req_adult):
        product = self._find_product(identifier)
        product.req_adult = req_adult
        self._factstore.add_fact(pfct.ProductAdultRequirementChanged(product))

    def set_stock_limit(self, identifier: idd.Identifier, stock_limit):
        product = self._find_product(identifier)
        product.stock_limit = stock_limit
        self._factstore.add_fact(pfct.ProductStockLimitChanged(product))

    def set_price(self, identifier: idd.Identifier, price):
        product = self._find_product(identifier)
        product.price = price
        self._factstore.add_fact(pfct.ProductPriceChanged(product))

    def exchange(self, identifier: idd.Identifier, amount: int, price: int):
        product = self._find_product(identifier)
        product.exchange(amount, price)
        self._factstore.add_fact(pfct.ProductStockBought(product, amount, price))

    def delete_product(self, identifier: idd.Identifier):
        product = self._find_product(identifier)
        product.delete()
        self._factstore.add_fact(pfct.ProductDeleted(product))

    def total_profit(self):
        return sum(p.product().profit for p in self.all())

    def total_value(self):
        def calculate_value(aggregate: pagg.ProductAggregate) -> int:
            product = aggregate.product()
            return product.profit + product.stock * product.price

        return sum(map(calculate_value, self.all()))
