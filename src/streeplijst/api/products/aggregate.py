"""

This module defines the state related to the product domain.
It processes the facts in order to produce a state.
These states must be read-only.

"""
from copy import deepcopy
from typing import Any, Callable, Dict, Type

from streeplijst.api.products import facts as pfacts
from streeplijst.api.users import facts as ufacts
from streeplijst.domain import product as prd
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.util import exception as exc
from streeplijst.util import identifier as idd
from streeplijst.util.translate import LANGUAGES


class InvalidProductIdentifier(exc.MultiLanguageException):
    """Exception when the product identifier is invalid."""


class ProductAggregate(agg.AutoAggregate):
    def __init__(self, factstore: fcs.FactStore, identifier: idd.Identifier):
        if not isinstance(identifier, idd.Identifier):
            raise InvalidProductIdentifier().with_error_msg(
                LANGUAGES.ENGLISH,
                f"The identifier {identifier} is not a valid identifier.",
            ).with_error_msg(LANGUAGES.DUTCH, f"Het id {identifier} is geen geldig id.")
        self._product: prd.Product
        self._fact_handlers: Dict[Type[fct.Fact], Callable[[Any], Any]] = {
            pfacts.ProductCreated: self._handle_create,
            pfacts.ProductNameChanged: self._handle_set_name,
            pfacts.ProductStockLimitChanged: self._handle_set_stock_limit,
            pfacts.ProductAdultRequirementChanged: self._handle_adult_req,
            pfacts.ProductPriceChanged: self._handle_price,
            pfacts.ProductStockBought: self._handle_stock_bought,
            pfacts.ProductDeleted: self._handle_deletion,
            ufacts.UsersBuysProduct: self._handle_buy,
        }
        super(ProductAggregate, self).__init__(
            factstore, self.fact_descriptors(identifier)
        )

    @staticmethod
    def fact_descriptors(identifier):
        return (
            *(
                fcs.FactDescriptor(ftype, subject=identifier)
                for ftype in pfacts.all_facts
            ),
            fcs.FactDescriptor(ufacts.UsersBuysProduct, product_id=identifier),
        )

    def _handle_create(self, fact: pfacts.ProductCreated):
        self._product = prd.Product(
            identifier=fact.subject,
            name=fact.name,
            stock=fact.stock,
            stock_limit=fact.stock_limit,
            req_adult=fact.req_adult,
            price=fact.price,
            profit=fact.profit,
        )

    def _handle_set_name(self, fact: pfacts.ProductNameChanged):
        self._product.name = fact.name

    def _handle_set_stock_limit(self, fact: pfacts.ProductStockLimitChanged):
        self._product.stock_limit = fact.stock_limit

    def _handle_adult_req(self, fact: pfacts.ProductAdultRequirementChanged):
        self._product.req_adult = fact.req_adult

    def _handle_price(self, fact: pfacts.ProductPriceChanged):
        self._product.price = fact.price

    def _handle_stock_bought(self, fact: pfacts.ProductStockBought):
        self._product.exchange(fact.amount, fact.price)

    def _handle_buy(self, fact: ufacts.UsersBuysProduct):
        self._product.exchange(fact.amount, fact.price)

    def _handle_deletion(self, fact: pfacts.ProductDeleted):
        self.unsubscribe()

    def handle_fact(self, fact: fct.Fact):
        if type(fact) not in self._fact_handlers:
            raise agg.FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )

        self._fact_handlers[type(fact)](fact)

    def product(self) -> prd.Product:
        return deepcopy(self._product)
