"""

This package contains a user and consumable management system.
Users can buy consumables, manage their balance, and the administrator can
manage the consumables.
The domain package implements the domain logic.
The eventsourcing package implements a eventsourcing backend.
The server package implements the frontend view.
These three packages should be completely decoupled.
The api package links the frontend, domain and backend package together.
The util package implements common (developer) tools and functions.
The configs package implements user configurable files required to setup the
project.

"""
