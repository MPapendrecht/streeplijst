from typing import Dict, Iterable, Union

from pandas import DataFrame

from streeplijst.api import init_fact_types
from streeplijst.api.payments import facts as pfct
from streeplijst.api.users import facts as ufct
from streeplijst.configs.config import config
from streeplijst.domain import payment as pay
from streeplijst.eventsourcing.aggregate import AutoAggregate, FactHandlerException
from streeplijst.eventsourcing.fact import Fact
from streeplijst.eventsourcing.factstore import FactDescriptor, FactStore
from streeplijst.eventsourcing.repository import FileStorage
from streeplijst.util.identifier import Identifier
from streeplijst.util.translate import LANGUAGES


class ExcelAggregate(AutoAggregate):
    def __init__(self, factstore: FactStore):
        self._user_names: Dict[Identifier, str] = {}
        self._user_by_payments: Dict[Identifier, Identifier] = {}
        self._payments: Dict[Identifier, pay.Payment] = {}
        super(ExcelAggregate, self).__init__(
            factstore,
            (
                FactDescriptor(ufct.UserCreated),
                FactDescriptor(ufct.UserNameChanged),
                FactDescriptor(pfct.UserMakesPayment),
                FactDescriptor(pfct.PaymentVerified),
            ),
        )

    def handle_fact(self, fact: Fact):
        if isinstance(fact, ufct.UserCreated):
            self._user_names[fact.subject] = fact.name
        elif isinstance(fact, ufct.UserNameChanged):
            self._user_names[fact.subject] = fact.name
        elif isinstance(fact, pfct.UserMakesPayment):
            self._payments[fact.payment_id] = pay.Payment(
                amount=fact.amount,
                identifier=fact.payment_id,
                creation_datetime=fact.datetime,
            )
            self._user_by_payments[fact.payment_id] = fact.user_id
        elif isinstance(fact, pfct.PaymentVerified):
            self._payments[fact.subject].verify(fact.validation_datetime)
        else:
            raise FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, "Unhandled fact"
            )

    def to_dicts(self) -> Iterable[Dict[str, Union[str, int]]]:
        return (
            {
                "identifier": payment.identifier.value,
                "datetime": payment.datetime,
                "name": self._user_names.get(
                    self._user_by_payments[payment.identifier], "Onbekende gebruiker"
                ),
                "amount": payment.amount / 100,
                "validation_date": payment.validated_on.date()
                if payment.is_verified
                else "Nog niet betaald",
            }
            for payment in self._payments.values()
        )


def main():
    input_file = config().server.data_file_path
    print(f"Reading from {input_file}.")
    output_file = input_file.parent / "payments.xlsx"
    init_fact_types()
    facts = FileStorage(input_file)
    fact_store = FactStore(facts)
    aggregate = ExcelAggregate(fact_store)
    df = DataFrame(aggregate.to_dicts())
    df.to_excel(output_file, "transactions", index=False)
    print(f"Writing to {output_file}.")


if __name__ == "__main__":
    main()
