"""

Module for exporting all debug information to an excel file.

"""
import datetime as dt
from pathlib import Path
from typing import Dict, List, Optional, Tuple

import pandas as pd

from factmanagement.exports.editing import fact_to_dict
from streeplijst.api import init_fact_types
from streeplijst.api.metrics.service import MetricService
from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService
from streeplijst.configs.config import config
from streeplijst.domain.bank import Bank
from streeplijst.eventsourcing.fact import Fact
from streeplijst.eventsourcing.factstore import FactStore
from streeplijst.eventsourcing.repository import FileStorage, ListStorage
from streeplijst.util.secrets import MemoryVault


def read_facts(fact_path) -> Tuple[Fact, ...]:
    init_fact_types()
    return FileStorage(fact_path).get_all()


def setup_metric_service(factstore: FactStore) -> MetricService:
    bank = Bank()
    vault = MemoryVault()
    user_service = UserService(factstore, bank, vault)
    product_service = ProductService(factstore)
    payment_service = PaymentService(factstore, bank)
    return MetricService(user_service, product_service, payment_service)


def debug_dict(facts) -> List[Dict]:
    print(f"Parsing {len(facts)} items.")
    factstore = FactStore(ListStorage())
    metric_service = setup_metric_service(factstore)
    num_parsed = 0
    start_time = dt.datetime.now()
    divisions = 10
    target = 1 + divmod(len(facts), divisions)[0]

    def parse_fact(fact) -> Dict:
        factstore.add_fact(fact)
        nonlocal num_parsed
        num_parsed += 1
        if num_parsed % target == 0:
            nonlocal start_time
            end_time = dt.datetime.now()
            delta = (end_time - start_time).total_seconds()
            print(f"Parsed {num_parsed} items in {delta} sec.")
            start_time = end_time
        return {
            **fact_to_dict(fact),
            "total_balance": metric_service.get_total_balance(),
            "total_unverified_payments": metric_service.get_total_unverified_payments(),
            "total_product_profit": metric_service.get_total_product_profit(),
            "total_product_value": metric_service.get_total_product_value(),
            "total_negative_balance": metric_service.get_negative_balance(),
            "total_value": metric_service.get_total_value(),
            "total_sellable_product_value": metric_service.get_sellable_product_value(),
            "total_projected_value": metric_service.get_projected_value(),
            "total_restitution": metric_service.get_total_restitution(),
            "total_invested": metric_service.get_total_invested(),
        }

    return list(map(parse_fact, facts))


def save_to_excel(data: List[Dict], destination: Path) -> None:
    dataframe = pd.DataFrame(data)
    print(f"Saving data to: {destination}")
    if not destination.parent.exists():
        destination.parent.mkdir()
    dataframe.to_excel(destination, index=False)


def get_fact_path(fact_path: Optional[Path]) -> Path:
    return fact_path if fact_path is not None else config().server.data_file_path


def get_excel_path(output_path: Optional[Path]) -> Path:
    return (
        output_path
        if output_path is not None
        else config().server.data_file_path.parent / "debug.xlsx"
    )


def export(fact_path: Optional[Path] = None, output_path: Optional[Path] = None):
    facts = read_facts(get_fact_path(fact_path))
    data = debug_dict(facts)
    save_to_excel(data, get_excel_path(output_path).absolute())


if __name__ == "__main__":
    export()
