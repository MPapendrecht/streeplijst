"""

Module for exporting and importing facts from an excel sheet.

"""
import json
from pathlib import Path
from typing import Dict, Iterable

import pandas as pd  # type: ignore

from streeplijst.api import init_fact_types
from streeplijst.configs.config import config
from streeplijst.eventsourcing.fact import Fact, get_fact_definition
from streeplijst.eventsourcing.repository import FileStorage
from streeplijst.util import identifier as idd

ID_FIELDS = ["_subject", "_user_id", "_product_id", "_payment_id"]
BOOL_FIELDS = ["_req_adult", "_allow_negative"]
BYTES_FIELDS = ["_mail"]
ID_DICT_FIELDS = ["_other_users"]
FACT_PATH = config().server.data_file_path.parent / "export.repo"
EXCEL_PATH = config().server.data_file_path.parent / "export.xlsx"


def to_excel(fact_path, excel_path):
    """Save to excel."""
    init_fact_types()
    facts = FileStorage(fact_path).get_all()
    _save(facts, excel_path)
    print(f"Exported {fact_path} to {excel_path}.")


def _save(facts: Iterable[Fact], destination: Path) -> None:
    dataframe = pd.DataFrame(map(fact_to_dict, facts))
    print(destination)
    if not destination.parent.exists():
        destination.parent.mkdir()
    dataframe.to_excel(destination, index=False)


def to_facts(fact_path, excel_path):
    init_fact_types()
    facts = _load_facts_from_dataframe(pd.read_excel(excel_path))
    fact_path.unlink(missing_ok=True)
    fs = FileStorage(fact_path)
    for fact in facts:
        fs.add(fact)


def _load_facts_from_dataframe(dataframe: pd.DataFrame):
    return map(lambda x: _load_fact_from_entry(x[1]), dataframe.iterrows())


def _load_fact_from_entry(entry: pd.Series) -> Fact:
    entry = entry.dropna()
    data = entry.to_dict()
    _class = get_fact_definition(data.pop("label"), data.pop("version"))
    for field in ID_FIELDS:
        if field in data:
            data[field] = idd.Identifier(data[field])
    for field in BOOL_FIELDS:
        if field in data:
            data[field] = data[field] == 1
    for field in BYTES_FIELDS:
        if field in data:
            data[field] = bytes.fromhex(data[field])
        else:
            data[field] = b""
    for field in ID_DICT_FIELDS:
        if field in data:
            data[field] = {
                idd.Identifier(k): v for k, v in json.loads(data[field]).items()
            }
        else:
            data[field] = {}
    for key, val in data.items():
        if isinstance(val, float):
            data[key] = int(val)
    obj = object.__new__(_class)
    obj.__dict__.update(data)
    assert isinstance(obj, Fact), "Can only load objects of type Fact."
    return obj


def fact_to_dict(fact: Fact) -> Dict:
    try:
        reduced_fact = fact.__reduce__()
    except TypeError:
        return {"label": "error", "version": "NoneType"}
    label, version = reduced_fact[1]  # type: ignore
    fact__dict__ = reduced_fact[2]  # type: ignore
    data = {"label": label, "version": version, **fact__dict__}  # type: ignore
    for field in ID_FIELDS:
        if field in data and isinstance(data[field], idd.Identifier):
            data[field] = data[field].value  # type: ignore
    # Convert bytes to hex
    for field in BYTES_FIELDS:
        if field in data and isinstance(data[field], bytes):
            data[field] = data[field].hex()
    # Convert mapping into json
    for field in ID_DICT_FIELDS:
        if field in data and isinstance(data[field], dict):
            data[field] = json.dumps({k.value: v for k, v in data[field].items()})
    return data


if __name__ == "__main__":
    to_facts(FACT_PATH, EXCEL_PATH)
