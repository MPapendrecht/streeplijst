import datetime

from pandas import DataFrame

from streeplijst.api import init_fact_types
from streeplijst.api.metrics.service import MetricService
from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService
from streeplijst.configs.config import config
from streeplijst.domain.bank import Bank
from streeplijst.eventsourcing.factstore import FactStore
from streeplijst.eventsourcing.repository import FileStorage, ListStorage
from streeplijst.util.secrets import MemoryVault


def calculate_metrics(metric_service, target_date):
    return {
        "day": target_date,
        "user_balance": metric_service.get_total_balance() / 100,
        "unverified_payments": metric_service.get_total_unverified_payments() / 100,
        "user_restitution": metric_service.get_total_restitution() / 100,
        "product_profit": metric_service.get_total_product_profit() / 100,
        "projected_product_value": metric_service.get_total_product_value() / 100,
        "sellable_product_value": metric_service.get_sellable_product_value() / 100,
        "total_value": metric_service.get_total_value() / 100,
        "projected_value": metric_service.get_projected_value() / 100,
        "total_invested": metric_service.get_total_invested() / 100,
        "negative_balance": metric_service.get_negative_balance() / 100,
    }


def main() -> None:
    input_file = config().server.data_file_path
    print(f"Reading from {input_file}.")
    output_file = input_file.parent / "metrics.xlsx"
    init_fact_types()
    facts = FileStorage(input_file)
    fact_store = FactStore(ListStorage())
    bank = Bank()
    vault = MemoryVault()
    user_service = UserService(fact_store, bank, vault)
    product_service = ProductService(fact_store)
    payment_service = PaymentService(fact_store, bank)
    metric_service = MetricService(user_service, product_service, payment_service)
    values = []
    target_date: datetime.date = next(facts.replay(1))[1].datetime.date()
    for _, fact in facts.replay():
        if fact.datetime.date() > target_date:
            values.append(calculate_metrics(metric_service, target_date))
            target_date = fact.datetime.date()
        fact_store.add_fact(fact)
    values.append(calculate_metrics(metric_service, target_date))
    df = DataFrame(values)
    df.to_excel(output_file, "values", index=False)
    print(f"Writing to {output_file}.")


if __name__ == "__main__":
    main()
