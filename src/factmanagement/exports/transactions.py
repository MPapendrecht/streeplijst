from datetime import datetime
from enum import Enum
from typing import Dict, Iterable, List, Optional, Union

from pandas import DataFrame

from streeplijst.api import init_fact_types
from streeplijst.api.products import facts as pfct
from streeplijst.api.users import facts as ufct
from streeplijst.configs.config import config
from streeplijst.domain import user
from streeplijst.eventsourcing.aggregate import AutoAggregate, FactHandlerException
from streeplijst.eventsourcing.fact import Fact
from streeplijst.eventsourcing.factstore import FactDescriptor, FactStore
from streeplijst.eventsourcing.repository import FileStorage
from streeplijst.util.identifier import Identifier
from streeplijst.util.translate import LANGUAGES

SYS_ADMIN = Identifier(value="SYSADMIN")


class Action(Enum):
    CREATE = "Created"
    BOUGHT = "Bought"
    SHARED = "Shared"
    PRICE_CHANGE = "changed price of"
    STOCK_CHANGE = "changed stock of"


class ExcelTransaction(user.Transaction):
    def __init__(
        self,
        user_id: Identifier,
        action: Action,
        expense: int,
        value_change: int,
        total_value: int,
        product_id: Identifier,
        amount: int,
        sell_price: int,
        total_stock: int,
        date: Optional[datetime] = None,
    ):
        super(ExcelTransaction, self).__init__(product_id, amount, date)
        self._user_id = user_id
        self._action = action
        self._expense = expense
        self._value_change = value_change
        self._total_value = total_value
        self._sell_price = sell_price
        self._total_stock = total_stock

    @property
    def user_id(self):
        return self._user_id

    @property
    def action(self):
        return self._action

    @property
    def expense(self):
        return self._expense

    @property
    def sell_price(self):
        return self._sell_price

    @property
    def total_stock(self):
        return self._total_stock

    @property
    def value_change(self):
        return self._value_change

    @property
    def total_value(self):
        return self._total_value


class ExcelAggregate(AutoAggregate):
    def __init__(self, factstore: FactStore):
        self._user_names: Dict[Identifier, str] = {}
        self._user_names[SYS_ADMIN] = "System Admin"
        self._product_names: Dict[Identifier, str] = {}
        self._product_price: Dict[Identifier, int] = {}
        self._product_stock: Dict[Identifier, int] = {}
        self._product_value: Dict[Identifier, int] = {}
        self._transactions: List[ExcelTransaction] = []
        super(ExcelAggregate, self).__init__(
            factstore,
            (
                FactDescriptor(ufct.UserCreated),
                FactDescriptor(ufct.UserNameChanged),
                FactDescriptor(ufct.UsersBuysProduct),
                FactDescriptor(pfct.ProductCreated),
                FactDescriptor(pfct.ProductNameChanged),
                FactDescriptor(pfct.ProductPriceChanged),
                FactDescriptor(pfct.ProductStockBought),
            ),
        )

    def handle_fact(self, fact: Fact):
        if isinstance(fact, ufct.UserCreated):
            self._user_names[fact.subject] = fact.name
        elif isinstance(fact, ufct.UserNameChanged):
            self._user_names[fact.subject] = fact.name
        elif isinstance(fact, pfct.ProductCreated):
            self._product_names[fact.subject] = fact.name
            self._product_price[fact.subject] = fact.price
            self._product_stock[fact.subject] = fact.stock
            self._product_value[fact.subject] = fact.price * fact.stock
            self._transactions.append(
                ExcelTransaction(
                    user_id=SYS_ADMIN,
                    action=Action.CREATE,
                    product_id=fact.subject,
                    amount=fact.stock,
                    expense=0,
                    sell_price=self._product_price[fact.subject],
                    total_stock=self._product_stock[fact.subject],
                    date=fact.datetime,
                    value_change=self._product_value[fact.subject],
                    total_value=fact.price * fact.stock,
                )
            )
        elif isinstance(fact, pfct.ProductNameChanged):
            self._product_names[fact.subject] = fact.name
        elif isinstance(fact, ufct.UsersBuysProduct):
            self._product_stock[fact.product_id] -= fact.amount
            self._product_value[fact.product_id] += fact.price
            self._transactions.append(
                ExcelTransaction(
                    user_id=fact.subject,
                    action=Action.BOUGHT,
                    product_id=fact.product_id,
                    amount=fact.amount,
                    expense=fact.price,
                    sell_price=self._product_price[fact.product_id],
                    total_stock=self._product_stock[fact.product_id],
                    date=fact.datetime,
                    value_change=fact.price,
                    total_value=self._product_value[fact.product_id],
                )
            )
            for user_id, price in fact.other_users.items():
                self._transactions.append(
                    ExcelTransaction(
                        user_id=user_id,
                        action=Action.SHARED,
                        product_id=fact.product_id,
                        amount=fact.amount,
                        expense=price,
                        sell_price=self._product_price[fact.product_id],
                        total_stock=self._product_stock[fact.product_id],
                        date=fact.datetime,
                        value_change=price,
                        total_value=self._product_value[fact.product_id],
                    )
                )
        elif isinstance(fact, pfct.ProductPriceChanged):
            old_price = self._product_price[fact.subject]
            self._product_price[fact.subject] = fact.price
            value_change = (fact.price - old_price) * self._product_stock[fact.subject]
            self._product_value[fact.subject] += value_change
            self._transactions.append(
                ExcelTransaction(
                    user_id=SYS_ADMIN,
                    action=Action.PRICE_CHANGE,
                    product_id=fact.subject,
                    amount=0,
                    expense=0,
                    sell_price=fact.price,
                    total_stock=self._product_stock[fact.subject],
                    date=fact.datetime,
                    value_change=value_change,
                    total_value=self._product_value[fact.subject],
                )
            )
        elif isinstance(fact, pfct.ProductStockBought):
            self._product_stock[fact.subject] -= fact.amount
            value_change = fact.price - fact.amount * self._product_price[fact.subject]
            self._product_value[fact.subject] += value_change
            self._transactions.append(
                ExcelTransaction(
                    user_id=SYS_ADMIN,
                    action=Action.STOCK_CHANGE,
                    product_id=fact.subject,
                    amount=fact.amount,
                    expense=fact.price,
                    sell_price=self._product_price[fact.subject],
                    total_stock=self._product_stock[fact.subject],
                    date=fact.datetime,
                    value_change=value_change,
                    total_value=self._product_value[fact.subject],
                )
            )
        else:
            raise FactHandlerException().with_error_msg(
                LANGUAGES.ENGLISH, f"Unhandled fact {fact}"
            )

    def to_dicts(self) -> Iterable[Dict[str, Union[str, int]]]:
        return (
            {
                "datetime": transaction.date,
                "name": self._user_names.get(transaction.user_id, "Unknown user"),
                "action": transaction.action.value,
                "product": self._product_names.get(
                    transaction.product_id, "Unknown product"
                ),
                "amount": transaction.amount,
                "total_stock": transaction.total_stock,
                "expense": transaction.expense / 100,
                "sell_price": transaction.sell_price / 100,
                "value_change": transaction.value_change / 100,
                "total_value": transaction.total_value / 100,
            }
            for transaction in self._transactions
        )


def main():
    input_file = config().server.data_file_path
    print(f"Reading from {input_file}.")
    output_file = input_file.parent / "transactions.xlsx"
    init_fact_types()
    facts = FileStorage(input_file)
    fact_store = FactStore(facts)
    aggregate = ExcelAggregate(fact_store)
    df = DataFrame(aggregate.to_dicts())
    df.to_excel(output_file, "transactions", index=False)
    print(f"Writing to {output_file}.")


if __name__ == "__main__":
    main()
