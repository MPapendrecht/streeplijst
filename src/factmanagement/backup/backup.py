import shutil
from datetime import datetime
from pathlib import Path
from typing import Callable, List, Tuple

import yaml

from streeplijst.configs.config import config, config_dir, extend_config

CopyFunction = Callable[[Path, Path], None]


def make_filename(prefix) -> str:
    now = datetime.now().strftime("%Y%m%dT%H%M%S")
    return f"{prefix}-{now}.repo"


def make_backup(source: Path, destination: Path, filename: str) -> Tuple[Path, Path]:
    if not source.exists():
        raise ValueError(f"Source file {source} not found")
    if not destination.exists():
        destination.mkdir()
    if not destination.is_dir():
        raise ValueError("Destination should be a directory")
    new_destination = destination / filename
    if new_destination.exists():
        raise ValueError("File already exists")
    return source, new_destination


def make_backups(
    save_locations: List[Path],
    data_file_path: Path,
    file_prefix: str,
    copyfunction: CopyFunction,
):
    errors = []
    for destination in save_locations:
        try:
            source, dest = make_backup(
                data_file_path,
                destination,
                make_filename(file_prefix),
            )
            copyfunction(source, dest)
        except ValueError as e:
            print(f"Error: {e}")
            errors.append(e)
    if len(errors) > 0:
        raise ValueError(errors)


def copier(src: Path, dest: Path):
    print(f"Copying from {src} to {dest}")
    shutil.copyfile(src, dest)


def init_config_file():
    backup_dir = Path(__file__).parent
    backup_file = config_dir() / "rotate-backup.ini"
    if not backup_file.exists():
        shutil.copyfile(backup_dir / "rotate-backup.example.ini", backup_file)
    with open(Path(__file__).parent / "config.example.yaml", "r") as f:
        extend_config(yaml.safe_load(f))


def main():
    init_config_file()
    build_config = config()
    if build_config.backup is not None:
        make_backups(
            build_config.backup.save_locations,
            build_config.server.data_file_path,
            build_config.backup.file_prefix,
            copier,
        )
