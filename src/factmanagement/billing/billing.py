import datetime as dt
import logging
from typing import Iterable, List, Optional

import requests
from fastapi import HTTPException

from streeplijst.api import init_fact_types
from streeplijst.api.payments.service import PaymentService
from streeplijst.api.users.service import UserService
from streeplijst.configs.config import config
from streeplijst.configs.modules.billing import BillingConfig
from streeplijst.domain.bank import Bank
from streeplijst.domain.payment import Payment
from streeplijst.domain.user import User
from streeplijst.eventsourcing import repository as rpy
from streeplijst.eventsourcing.factstore import FactStore
from streeplijst.util.secrets import SqliteVault

logger = logging.getLogger(__name__)


def setup_services(repository, vault):
    repository.lock_for_replay()
    fact_store = FactStore(repository)
    bank = Bank()
    user_service = UserService(fact_store, bank, vault)
    PaymentService(fact_store, bank)  # Necessary to verify payments
    fact_store.replay()
    return user_service


def within_range(
    date: dt.date,
    start_date: Optional[dt.date],
    end_date: Optional[dt.date],
) -> bool:
    if start_date is not None and date < start_date:
        return False
    if end_date is not None and date > end_date:
        return False
    return True


class BillingRequest:
    def __init__(
        self,
        user: User,
        start_date: Optional[dt.date] = None,
        end_date: Optional[dt.date] = None,
    ):
        self._start_date = start_date
        self._end_date = end_date
        self._name = user.name
        self._mail = user.mail
        self._balance = user.balance
        self._payments: List[Payment] = []

    def add_payment(self, payment: Payment):
        if not payment.is_verified or within_range(
            payment.validated_on.date(), self._start_date, self._end_date
        ):
            self._payments.append(payment)

    def to_dict(self):
        verified_payments = list(filter(lambda p: p.is_verified, self._payments))
        start_date = self._start_date
        if start_date is None:
            start_date = (
                "2000-01-01"
                if len(verified_payments) == 0
                else min(p.validated_on.date() for p in verified_payments)
            )
        end_date = self._end_date
        if end_date is None:
            end_date = (
                "3000-01-01"
                if len(verified_payments) == 0
                else max(p.validated_on.date() for p in verified_payments)
            )
        return {
            "user": {
                "name": self._name,
                "email": self._mail,
                "balance": self._balance,
            },
            "period": {
                "start_date": str(start_date),
                "end_date": str(end_date),
            },
            "payments": [
                {
                    "date": str(p.datetime.date()),
                    "value": p.amount,
                    "payed_on": None
                    if p.validated_on is None
                    else str(p.validated_on.date()),
                }
                for p in sorted(self._payments, key=lambda p: p.datetime)
            ],
        }


def should_be_billed(user: User, payments: Iterable[Payment]) -> bool:
    unverified_payment_total = sum(
        map(lambda p: p.amount, filter(lambda p: not p.is_verified, payments))
    )
    outstanding_balance = 0 if user.balance >= 0 else -1 * user.balance
    total_due = unverified_payment_total + outstanding_balance
    if total_due == 0:
        return False
    verified_payments = list(filter(lambda p: p.is_verified, payments))

    if len(verified_payments) > 0:
        last_payment = max(map(lambda p: p.validated_on, verified_payments))
        if (
            last_payment - dt.datetime.now()
            < dt.timedelta(days=get_billing_config().grace_period)
            and total_due < user.balance_limit
        ):
            return False
    return True


def get_billing_config() -> BillingConfig:
    billing_config = config().billing
    if billing_config is None:
        raise SystemExit("Billing configuration is missing in the configuration file")
    return billing_config


def send_invoice(billing_request: BillingRequest) -> None:
    billing_config = get_billing_config()
    req = requests.post(
        billing_config.entrypoint,
        auth=(billing_config.username, billing_config.password),
        json=billing_request.to_dict(),
    )
    if req.status_code != 200:
        raise HTTPException(req.status_code, req.text)


def invoice(
    start_date: Optional[dt.date] = None,
    end_date: Optional[dt.date] = None,
    dry_run: bool = True,
):  # pragma: no cover
    init_fact_types()
    repository = rpy.FileStorage(config().server.data_file_path)
    vault = SqliteVault(config().server.vault_file_path)
    user_service = setup_services(repository, vault)
    for user_agg in user_service.all():
        user: User = user_agg.user()
        payments = user_service.payments(user.identifier)
        if not should_be_billed(user, payments):
            if dry_run:
                print(f"Skipping invoice for {user.name}. Everything is payed.")
            continue
        if user.mail is None:
            print(f"WARNING: User {user.name} does not have an email yet.")
            continue
        # Generate billing request
        billing_request = BillingRequest(user, start_date, end_date)
        for payment in user_service.payments(user.identifier):
            billing_request.add_payment(payment)
        # Send invoice
        if dry_run:
            print(f"Not sending request: {billing_request.to_dict()}")
        else:
            try:
                send_invoice(billing_request)
            except HTTPException as e:
                logger.error(f"Requesting an invoice failed: {e}")


if __name__ == "__main__":
    invoice()
