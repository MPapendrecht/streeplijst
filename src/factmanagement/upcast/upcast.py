from typing import Tuple

from factmanagement.upcast.transform import FactTransformer, UnsupportedTransformation
from streeplijst import api
from streeplijst.configs.config import config
from streeplijst.eventsourcing.fact import Fact, get_fact_definition, get_latest_version
from streeplijst.eventsourcing.repository import FileStorage


def upcast(*facts: Fact) -> Tuple[Fact, ...]:
    transformer = FactTransformer()

    def _upgrade(fact) -> Fact:
        label = fact.label
        version = get_latest_version(label)
        fact_type = get_fact_definition(label, version)
        try:
            return transformer.transform(fact_type, fact)
        except UnsupportedTransformation:
            transformer.register_transformation(type(fact), fact_type, fact_type.upcast)
            return transformer.transform(fact_type, fact)

    return tuple(_upgrade(fact) for fact in facts)


def main():
    print("Initializing fact types.")
    api.init_fact_types()
    print(f"Loading from {config().server.data_file_path}.")
    fs = FileStorage(config().server.data_file_path)
    facts = fs.get_all()
    new_facts = upcast(*facts)
    config().server.data_file_path.unlink()
    fs = FileStorage(config().server.data_file_path)
    for fact in new_facts:
        fs.add(fact)
    print(f"Upgraded {len(facts)} facts to {len(new_facts)} new facts.")


if __name__ == "__main__":
    main()
