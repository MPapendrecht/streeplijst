"""

This module implements the logic for upcasting facts to their latest version.
It implements a command line interface.

"""
from collections import defaultdict
from typing import Callable, Dict, Tuple, Type, Union

from streeplijst.eventsourcing.fact import Fact


class UnsupportedTransformation(Exception):
    """A transformation between facts was not supported."""


FactTypes = Union[Type[Fact], Tuple[Type[Fact], ...]]


def _tuple_guard(types: FactTypes) -> Tuple[Type[Fact], ...]:
    if not isinstance(types, tuple):
        return (types,)
    return types


class FactTransformer:
    def __init__(self) -> None:
        self._transformations: Dict[
            FactTypes, Dict[FactTypes, Callable[[Fact], Fact]]
        ] = defaultdict(dict)

    def register_transformation(
        self,
        input_types: FactTypes,
        output_types: FactTypes,
        transformer: Callable[[Fact], Fact],
    ):
        self._transformations[_tuple_guard(input_types)][
            _tuple_guard(output_types)
        ] = transformer

    def transform(self, target: FactTypes, *facts: Fact) -> Fact:
        tuple_types = tuple(type(f) for f in facts)
        if tuple_types not in self._transformations:
            raise UnsupportedTransformation(
                f"No transformations known for the input types: {tuple_types}."
            )
        transformations = self._transformations[tuple_types]
        target = _tuple_guard(target)
        if target not in transformations:
            raise UnsupportedTransformation(
                f"No transformations from {tuple_types} to {target} known."
            )
        transformation = transformations[target]
        return transformation(*facts)
