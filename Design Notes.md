This document contains some issues that were ran into and how they were resolved.


# Entities
Entities have properties, and these properties need validation. 
It was considered to make them immutable, but these entities also _take action_.
To avoid returning itself on every action an entity takes, and thus having to validate itself each time, it was chosen to make these entities mutable.
The properties might have a interdependence relation.
This is why entities assign all parameters to their internval variables first, and then reassign them again.
This will look like double assignment, but the pattern allows for complex property relations and has minimal overhead (assigning each variable once).
An added benefit is that normal properties can be used and the _variables defined outside init_ warning won't occur.

# Repr
Facts should have a representation that shows (at least) the public properties.
Instead of programmatically fetching the properties, it is decided to extend the standard `__repr__(self)` method.
The method signature for facts changes to `__repr__(self, *kwargs)`.
The keyword arguments will be parsed into a string `f"{key}={val}"`.
This design enforces naming every property accurately, but gives freedom to define any name and value.
Every child class can extend the members like so:
```python3
def __repr__(self, **kwargs):
    kwargs = {
        'key': 'value',  # Values defined for this class
        **kwargs  # Values defined for children
    }
    return super().__repr__(**kwargs)
```
In this way the children can also override fields of the parents.