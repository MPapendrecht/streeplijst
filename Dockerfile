FROM registry.gitlab.com/mpapendrecht/streeplijst:py310

COPY . .
ARG STRPLIST_OPTIONS "gunicorn"
RUN pip install .[$STRPLIST_OPTIONS]
RUN rm -rf *
RUN streeplijst init

VOLUME /home/config /home/data

ENTRYPOINT ["streeplijst"]
CMD ["--help"]