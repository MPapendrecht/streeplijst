from factmanagement.upcast import upcast
from streeplijst.eventsourcing import fact as fct


class TestUpcast:
    def test_upcasting_a_fact(self):
        class TestFact(fct.Fact, label="testFact", version=1):
            pass

        class TestFact2(fct.Fact, label="testFact", version=2):
            @classmethod
            def upcast(cls, fact: fct.Fact):
                return TestFact2(subject=fact.subject, creation_datetime=fact.datetime)

        fact1 = TestFact()
        fact2 = TestFact()
        with fct.temporary_fact_definition(TestFact, TestFact2):
            facts = upcast.upcast(fact1, fact2)
        assert len(facts) == 2
        assert isinstance(facts[0], TestFact2)
        assert fact1.subject == facts[0].subject
        assert fact2.datetime == facts[1].datetime
