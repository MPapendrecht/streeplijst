import pytest

from factmanagement.backup import backup as bck
from streeplijst.configs.config import config


def test_filename_starts_with_prefix():
    assert bck.make_filename("test").startswith("test")


def test_backup_without_source_fails(tmp_path):
    with pytest.raises(ValueError):
        bck.make_backup(tmp_path / "source", tmp_path, "")


def test_backup_without_destination_fails(tmp_path):
    open(tmp_path / "source", "w").close()
    with pytest.raises(ValueError):
        bck.make_backup(tmp_path / "source", tmp_path / "destination", "")


def test_backup_with_file_destination(tmp_path):
    dest_path = tmp_path / "destination"
    source_path = tmp_path / "source"
    open(dest_path, "w").close()
    open(source_path, "w").close()
    with pytest.raises(ValueError):
        bck.make_backup(source_path, dest_path, "")


def test_backup_with_filenames(tmp_path):
    open(tmp_path / "source", "w").close()
    assert bck.make_backup(tmp_path / "source", tmp_path, "backup") == (
        tmp_path / "source",
        tmp_path / "backup",
    )


def test_backup_with_config(tmp_path):
    outputs = []

    def mock_copier(source, dest):
        outputs.append((source, dest))

    open(tmp_path / "source", "w").close()
    bck.make_backups(
        [tmp_path],
        tmp_path / "source",
        "backup",
        mock_copier,
    )
    assert len(outputs) == 1
    source = outputs[0][0]
    dest = outputs[0][1]
    assert source == tmp_path / "source"
    assert dest.name.startswith("backup")


def test_backup_without_valid_source(tmp_path):
    outputs = []

    def mock_copier(source, dest):
        outputs.append((source, dest))

    assert not (tmp_path / "source").exists()
    with pytest.raises(ValueError):
        bck.make_backups(
            [tmp_path],
            tmp_path / "source",
            "backup",
            mock_copier,
        )


def test_copier(tmp_path):
    open(tmp_path / "source", "w").close()
    bck.copier(tmp_path / "source", tmp_path / "dest")
    assert (tmp_path / "dest").exists()


def test_init_config_file(tmp_path):
    bck.init_config_file()
    assert (tmp_path / "config" / "rotate-backup.ini").exists()
    assert config().backup is not None


def test_init_config_file_overwrite(tmp_path):
    with open(tmp_path / "config" / "rotate-backup.ini", "w") as f:
        f.write("hello, world!")
    bck.init_config_file()
    with open(tmp_path / "config" / "rotate-backup.ini", "r") as f:
        text = f.read()
    assert text == "hello, world!"
