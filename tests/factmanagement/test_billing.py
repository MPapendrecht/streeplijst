import datetime as dt

from factmanagement.billing import billing


class TestBilling:
    def test_date_within_range(self):
        target_date = dt.date(2024, 6, 1)
        start_date = dt.date(2023, 1, 1)
        end_date = dt.date(2025, 1, 1)
        assert billing.within_range(target_date, start_date, end_date)

    def test_date_within_none(self):
        target_date = dt.date(2024, 6, 1)
        assert billing.within_range(target_date, None, None)

    def test_date_too_late(self):
        target_date = dt.date(2024, 6, 1)
        start_date = dt.date(2024, 7, 1)
        assert not billing.within_range(target_date, start_date, None)

    def test_date_to_early(self):
        target_date = dt.date(2024, 6, 1)
        end_date = dt.date(2024, 5, 1)
        assert not billing.within_range(target_date, None, end_date)

    def test_can_create_user_service(self, repository, vault):
        user_service = billing.setup_services(repository, vault)
        assert len(user_service.all()) == 0

    def test_billing_request_data(self, user, payment):
        billing_request = billing.BillingRequest(user)
        billing_request.add_payment(payment)
        data = billing_request.to_dict()
        assert data["user"]["name"] == user.name
        assert len(data["payments"]) == 1

    def test_billing_request_data_without_payments(self, user):
        billing_request = billing.BillingRequest(user)
        data = billing_request.to_dict()
        assert data["user"]["name"] == user.name
        assert len(data["payments"]) == 0
        assert data["period"]["start_date"] == "2000-01-01"
        assert data["period"]["end_date"] == "3000-01-01"

    def test_billing_request_data_outside_end_range(self, user, payment):
        billing_request = billing.BillingRequest(user, end_date=dt.date(2024, 1, 1))
        payment.verify(dt.datetime(2025, 1, 1, 1, 1, 1))
        billing_request.add_payment(payment)
        data = billing_request.to_dict()
        assert len(data["payments"]) == 0

    def test_billing_request_data_outside_start_range(self, user, payment):
        billing_request = billing.BillingRequest(user, dt.date(2024, 1, 1))
        payment.verify(dt.datetime(2023, 1, 1, 1, 1, 1))
        billing_request.add_payment(payment)
        data = billing_request.to_dict()
        assert len(data["payments"]) == 0

    def test_billing_request_period_determined_based_on_payments(
        self, user, payment_factory
    ):
        billing_request = billing.BillingRequest(user)
        payment = payment_factory(amount=1)
        payment.verify(dt.datetime(2023, 1, 1, 1, 1, 1))
        billing_request.add_payment(payment)
        payment2 = payment_factory(amount=2)
        payment2.verify(dt.datetime(2023, 2, 1, 1, 1, 1))
        billing_request.add_payment(payment2)
        data = billing_request.to_dict()
        assert data["period"]["start_date"] == "2023-01-01"
        assert data["period"]["end_date"] == "2023-02-01"

    def test_user_should_be_billed_with_negative_balance(self, user):
        user.allow_negative = True
        user.balance = -1
        assert billing.should_be_billed(user, [])

    def test_user_should_be_billed_with_unverified_payments(self, user, payment):
        user.balance = 1
        assert billing.should_be_billed(user, [payment])

    def test_user_should_not_be_billed_with_verified_payments(self, user, payment):
        user.balance = 1
        payment.verify(dt.datetime(2023, 1, 1, 1, 1, 1))
        assert not billing.should_be_billed(user, [payment])

    def test_user_should_not_be_billed_if_a_recent_payment_exists(
        self, user, payment_factory, billing_config
    ):
        user.balance_limit = 20
        payment1 = payment_factory(10)
        payment2 = payment_factory(20)
        payment3 = payment_factory(30)
        payment2.verify(dt.datetime.now() - dt.timedelta(days=1))
        payment3.verify(dt.datetime.now() - dt.timedelta(days=99))
        assert not billing.should_be_billed(user, [payment1, payment2])

    def test_user_should_be_billed_if_balance_limit_is_exceeded(
        self, user, payment_factory, billing_config
    ):
        user.balance_limit = 5
        payment1 = payment_factory(10)
        payment2 = payment_factory(20)
        payment3 = payment_factory(30)
        payment2.verify(dt.datetime.now() - dt.timedelta(days=1))
        payment3.verify(dt.datetime.now() - dt.timedelta(days=99))
        assert billing.should_be_billed(user, [payment1, payment2])
