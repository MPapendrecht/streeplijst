import pytest

from factmanagement.upcast import transform
from streeplijst.eventsourcing import fact as fct


class TestFactTransformer:
    def test_transform_between_unrelated_facts(self):
        class AppleFact(fct.Fact, label="apple"):
            taste = "sweet"

        class PearFact(fct.Fact, label="pear"):
            structure = "sugary"

            def __init__(self, structure):
                super().__init__()
                self.structure = structure

        transformer = transform.FactTransformer()
        transformer.register_transformation(
            AppleFact, (PearFact,), lambda x: PearFact(x.taste)
        )
        fact = transformer.transform(PearFact, AppleFact())
        assert isinstance(fact, PearFact)
        assert fact.structure == "sweet"

    def test_unknown_input_type(self):
        transformer = transform.FactTransformer()
        with pytest.raises(transform.UnsupportedTransformation):
            transformer.transform(fct.Fact, fct.Fact())

    def test_unknown_output_type(self):
        transformer = transform.FactTransformer()
        transformer.register_transformation(fct.Fact, tuple(), lambda x: x)
        with pytest.raises(transform.UnsupportedTransformation):
            transformer.transform(fct.Fact, fct.Fact())
