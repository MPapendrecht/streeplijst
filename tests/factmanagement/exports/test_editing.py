import pytest

from streeplijst.eventsourcing.fact import Fact, temporary_fact_definition
from streeplijst.eventsourcing.repository import FileStorage

edit = pytest.importorskip("factmanagement.exports.editing")


class EditFact(Fact, label="TestEditing"):
    def __init__(self, req_adult: bool, value: float):
        super(EditFact, self).__init__()
        self._req_adult = req_adult
        self._value = value

    @property
    def value(self):
        return self._value


class TestEditing:
    def test_save_equals_load(self, tmp_path):
        facts_path = tmp_path / "facts.repo"
        excel_path = tmp_path / "facts.xlsx"
        fs = FileStorage(facts_path)
        fs.add(EditFact(False, 1.01))
        fs.add(Fact())
        with temporary_fact_definition(EditFact):
            edit.to_excel(facts_path, excel_path)
            edit.to_facts(facts_path, excel_path)
            new_fs = FileStorage(facts_path)
        facts = new_fs.get_all()
        assert len(facts) == 2
        assert facts[0].value == 1
