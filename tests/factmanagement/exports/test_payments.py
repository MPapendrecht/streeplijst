import pytest
from pytest import fixture

from streeplijst.api.payments.facts import PaymentVerified, UserMakesPayment
from streeplijst.api.users.facts import UserCreated, UserNameChanged
from streeplijst.eventsourcing.aggregate import FactHandlerException
from streeplijst.eventsourcing.fact import Fact

excel = pytest.importorskip("factmanagement.exports.payments")


@fixture
def excel_facts(user, payment, present_date):
    return [
        UserCreated(user),
        UserMakesPayment(payment, user),
        UserNameChanged(user),
        PaymentVerified(payment.identifier, present_date),
    ]


class TestExcelAggregate:
    def test_excel_aggregate_can_handle_fact(
        self, factstore, excel_facts, present_date
    ):
        agg = excel.ExcelAggregate(factstore)
        for fact in excel_facts:
            agg.handle_fact(fact)

        data = list(agg.to_dicts())
        assert len(data) == 1
        payments = data[0]
        assert payments["amount"] == 0.01
        assert payments["validation_date"] == present_date.date()

    def test_excel_aggregate_fails_on_unknown_fact(self, factstore):
        agg = excel.ExcelAggregate(factstore)
        with pytest.raises(FactHandlerException):
            agg.handle_fact(Fact())
