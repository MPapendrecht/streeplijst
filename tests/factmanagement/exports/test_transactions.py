import pytest
from pytest import fixture

from streeplijst.api.products.facts import ProductCreated, ProductNameChanged
from streeplijst.api.users.facts import UserCreated, UserNameChanged, UsersBuysProduct
from streeplijst.eventsourcing.aggregate import FactHandlerException
from streeplijst.eventsourcing.fact import Fact

excel = pytest.importorskip("factmanagement.exports.transactions")


@fixture
def excel_facts(user, product):
    return [
        UserCreated(user),
        ProductCreated(product),
        UserNameChanged(user),
        ProductNameChanged(product),
        UsersBuysProduct(user, {}, product, 1, 2),
    ]


class TestExcelAggregate:
    def test_excel_aggregate_can_handle_fact(self, factstore, excel_facts):
        agg = excel.ExcelAggregate(factstore)
        for fact in excel_facts:
            agg.handle_fact(fact)

        data = list(agg.to_dicts())
        assert len(data) == 2
        transaction = data[1]
        assert transaction["amount"] == 1
        assert transaction["expense"] == 0.02

    def test_excel_aggregate_fails_on_unknown_fact(self, factstore):
        agg = excel.ExcelAggregate(factstore)
        with pytest.raises(FactHandlerException):
            agg.handle_fact(Fact())
