import pandas as pd
import pytest

from streeplijst.eventsourcing.fact import Fact, temporary_fact_definition
from streeplijst.eventsourcing.repository import FileStorage

debug = pytest.importorskip("factmanagement.exports.debug")


class DebugFact(Fact, label="TestDebugging"):
    def __init__(self, req_adult: bool, value: float):
        super().__init__()
        self._req_adult = req_adult
        self._value = value

    @property
    def value(self):
        return self._value


class TestDebug:
    def test_can_export_to_excel(self, tmp_path):
        facts_path = tmp_path / "facts.repo"
        excel_path = tmp_path / "facts.xlsx"
        fs = FileStorage(facts_path)
        fs.add(DebugFact(False, 1.01))
        fs.add(Fact())
        with temporary_fact_definition(DebugFact):
            debug.export(facts_path, excel_path)
        df = pd.read_excel(excel_path)
        assert len(df) == 2
        assert df["total_invested"][0] == 0

    def test_can_export_to_new_directory(self, tmp_path):
        facts_path = tmp_path / "facts.repo"
        excel_path = tmp_path / "new_dir" / "facts.xlsx"
        fs = FileStorage(facts_path)
        fs.add(DebugFact(False, 1.01))
        fs.add(Fact())
        with temporary_fact_definition(DebugFact):
            debug.export(facts_path, excel_path)
        df = pd.read_excel(excel_path)
        assert len(df) == 2
        assert df["total_invested"][0] == 0

    def test_debugging_splitted_in_10(self, tmp_path):
        facts_path = tmp_path / "facts.repo"
        excel_path = tmp_path / "new_dir" / "facts.xlsx"
        fs = FileStorage(facts_path)
        for _ in range(20):
            fs.add(Fact())
        with temporary_fact_definition(DebugFact):
            debug.export(facts_path, excel_path)
        df = pd.read_excel(excel_path)
        assert len(df) == 20

    def test_debugging_has_defaults(self):
        assert debug.get_excel_path(None) is not None
        assert debug.get_fact_path(None) is not None
