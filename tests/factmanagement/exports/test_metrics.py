import pytest

from streeplijst.api.metrics.service import MetricService
from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService

metrics = pytest.importorskip("factmanagement.exports.metrics")


class TestMetricCalculation:
    def test_empty_metric_service_has_no_data(
        self, factstore, bank, vault, present_date
    ):
        user_service = UserService(factstore, bank, vault)
        payment_service = PaymentService(factstore, bank)
        product_service = ProductService(factstore)
        metric_service = MetricService(user_service, product_service, payment_service)
        result = metrics.calculate_metrics(metric_service, present_date)
        assert result["day"] == present_date
        assert result["total_value"] == 0
