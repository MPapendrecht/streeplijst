import pytest

from streeplijst.util import exception as exc
from streeplijst.util.translate import LANGUAGES


class TestException:
    def test_exception_can_be_raised(self):
        with pytest.raises(exc.ExceptionWithSideEffect):
            raise exc.ExceptionWithSideEffect()

    def test_exception_executes_side_effects(self):
        logs = []
        exc.ExceptionWithSideEffect.register_side_effect(lambda: logs.append("1"))
        exc.ExceptionWithSideEffect.register_side_effect(lambda: logs.append("2"))
        assert len(exc.ExceptionWithSideEffect.get_side_effects()) == 2
        with pytest.raises(exc.ExceptionWithSideEffect):
            raise exc.ExceptionWithSideEffect()
        assert len(logs) == 2
        assert "1" in logs
        assert "2" in logs
        exc.ExceptionWithSideEffect.clear_side_effects()

    def test_multilanguage_exception_message(self):
        exception = exc.MultiLanguageException().with_error_msg(
            LANGUAGES.ENGLISH, "Error message"
        )
        assert str(exception) == "Error message"
