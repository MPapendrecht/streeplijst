from streeplijst.util.extensions import deepmerge


class TestExtensions:
    def test_deepmerge_concatenates_lists(self):
        base = {"a": [1, 2]}
        target = {"a": [3, 4]}
        result = deepmerge(base, target)
        assert len(result["a"]) == 4
