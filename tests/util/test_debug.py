import logging
import os
import time
from os import path as path

from streeplijst.util import debug as dbg


class TestLogging:
    logger = logging.getLogger(f"{dbg.__name__}.test")

    def test_contextmanager(self):
        assert logging.INFO == self.logger.getEffectiveLevel()
        with dbg.debug_logging(self.logger):
            assert self.logger.getEffectiveLevel() == logging.DEBUG

    @dbg.debug_output()
    def test_decorator_without_argument(self):
        assert self.logger.getEffectiveLevel() == logging.DEBUG

    @dbg.debug_output(logger)
    def test_decorator_with_argument(self):
        assert self.logger.getEffectiveLevel() == logging.DEBUG

    @dbg.profile
    def _profiled_func(self):
        pass

    def test_profile(self):
        dbg_path = path.abspath(dbg.__file__)
        dbg_dir = path.dirname(dbg_path)
        util_dir = path.dirname(dbg_dir)
        assert path.exists(util_dir)
        prof_file = f"{util_dir}/streeplijst.prof"
        if path.exists(prof_file):
            os.remove(prof_file)
        self._profiled_func()
        assert path.exists(util_dir)
        os.remove(prof_file)

    def test_child_logger(self):
        logger = dbg.get_child_logger("abc")
        assert logger.name.endswith(".abc")


class TestTimer:
    def test_elapsed_time(self):
        def wait_a_bit():
            time.sleep(0.1)

        timer = dbg.elapsed_time(wait_a_bit)
        assert abs(timer - 0.1) < 0.03

    def test_runtime_distribution(self):
        target_duration = 0.01
        repeats = 25
        runtimes = []

        def wait_a_bit():
            start = time.perf_counter()
            time.sleep(target_duration)
            stop = time.perf_counter()
            runtimes.append(stop - start)

        mean, std = dbg.runtime_distribution(wait_a_bit, repeats)
        actual_runtime = sum(runtimes) / repeats
        assert abs(mean - actual_runtime) / actual_runtime < 0.01
        assert std < actual_runtime / 10
