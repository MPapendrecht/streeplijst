import pytest

from streeplijst.util.secrets import (
    CannotUnlockException,
    Lock,
    MemoryVault,
    SqliteVault,
)


class TestSecrets:
    def test_unlocking_invalid_value_is_not_allowed(self):
        vault = MemoryVault()
        owner = "abc"
        vault.generate_key(owner)
        lock = Lock(vault, owner)
        with pytest.raises(CannotUnlockException):
            lock.unlock("anything".encode())

    def test_unlocking_none_is_not_allowed(self):
        vault = MemoryVault()
        owner = "abc"
        vault.generate_key(owner)
        lock = Lock(vault, owner)
        with pytest.raises(CannotUnlockException):
            lock.unlock(None)

    def test_sqlite_vault_keeps_key_if_exists(self, tmp_path):
        filepath = tmp_path / "vault.temp"
        vault = SqliteVault(filepath)
        owner = "abc"
        key1 = vault.generate_key(owner)
        key2 = vault.generate_key(owner)
        assert key1 == key2

    def test_sqlite_vault_retains_values(self, tmp_path):
        filepath = tmp_path / "vault.temp"
        vault = SqliteVault(filepath)
        owner = "abc"
        key = vault.generate_key(owner)
        new_vault = SqliteVault(filepath)
        assert new_vault.retrieve_key(owner) == key

    def test_sqlite_vault_deletes_values(self, tmp_path):
        filepath = tmp_path / "vault.temp"
        vault = SqliteVault(filepath)
        owner = "abc"
        vault.generate_key(owner)
        vault.delete_owner(owner)
        new_vault = SqliteVault(filepath)
        with pytest.raises(KeyError):
            new_vault.retrieve_key(owner)
