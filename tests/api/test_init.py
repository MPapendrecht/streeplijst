from streeplijst.api import init_fact_types
from streeplijst.eventsourcing.fact import is_fact_definition_registered


def test_loaded_facts_are_registered():
    fact_types = init_fact_types()
    for fact_type in fact_types:
        assert is_fact_definition_registered(fact_type)
