import pytest

from streeplijst.api.products import service as srv
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct


@pytest.fixture
def service(factstore):
    return srv.ProductService(factstore)


@pytest.fixture
def new_product_args():
    return {
        "name": "testproduct",
        "stock": 0,
        "stock_limit": 0,
        "req_adult": False,
        "price": 0,
        "profit": 0,
    }


class TestProductService:
    def test_finding_an_existing_product_returns_product(
        self, service: srv.ProductService, new_product_args
    ):
        product = service.create_new_product(**new_product_args)
        assert service.find_by_id(product.product().identifier) == product

    def test_finding_all_product_return_all_products(
        self, service: srv.ProductService, new_product_args
    ):
        service.create_new_product(**new_product_args)
        new_product_args["name"] = "testproduct2"
        service.create_new_product(**new_product_args)
        assert len(service.all()) == 2

    def test_finding_unknown_product_raises_unknown_product_exception(
        self, service: srv.ProductService
    ):
        with pytest.raises(srv.UnknownProduct):
            service.find_by_id("identifier")  # type: ignore

    def test_unknown_fact_raises_fact_handler_exception(self, factstore):
        aggregate = srv.CreatedProductAggregate(factstore)
        with pytest.raises(agg.FactHandlerException):
            aggregate.handle_fact(fct.Fact())

    def test_service_restores_user_from_stream(self, factstore, new_product_args):
        service = srv.ProductService(factstore)
        service.create_new_product(**new_product_args)
        new_service = srv.ProductService(factstore)
        products = new_service.all()
        assert len(products) == 1

    def test_setting_req_adult_on_product_changes_prop(
        self, service: srv.ProductService, new_product_args
    ):
        product = service.create_new_product(**new_product_args)
        service.set_adult_requirement(product.product().identifier, True)
        assert product.product().req_adult is True

    def test_setting_stock_limit_on_product_changes_prop(
        self, service: srv.ProductService, new_product_args
    ):
        product = service.create_new_product(**new_product_args)
        service.set_stock_limit(product.product().identifier, 20)
        assert product.product().stock_limit == 20

    def test_setting_price_on_product_changes_price(
        self, service: srv.ProductService, new_product_args
    ):
        product = service.create_new_product(**new_product_args)
        service.set_price(product.product().identifier, 101)
        assert product.product().price == 101

    def test_setting_name_on_product_changes_name(
        self, service: srv.ProductService, new_product_args
    ):
        product = service.create_new_product(**new_product_args)
        service.set_name(product.product().identifier, "testproduct2")
        assert product.product().name == "testproduct2"

    def test_total_profit_of_no_products(self, service: srv.ProductService):
        assert service.total_profit() == 0

    def test_total_profit_of_multiple_products(
        self, service: srv.ProductService, new_product_args
    ):
        for profit in range(4):
            new_product_args["profit"] = profit
            service.create_new_product(**new_product_args)
        assert service.total_profit() == 6

    def test_total_value_of_no_products(self, service: srv.ProductService):
        assert service.total_value() == 0

    def test_total_value_of_multiple_products(
        self, service: srv.ProductService, new_product_args
    ):
        for profit in range(4):
            new_product_args["profit"] = profit
            new_product_args["price"] = profit
            new_product_args["stock"] = profit
            service.create_new_product(**new_product_args)
        assert service.total_value() == 0 + 2 + 6 + 12
