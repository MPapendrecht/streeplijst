"""

This package contains the tests for the products api.

"""

from tests.api.products import test_aggregate, test_service

__all__ = [
    "test_service",
    "test_aggregate",
]
