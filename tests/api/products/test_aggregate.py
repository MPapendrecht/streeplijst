import pytest

from streeplijst.api.products import aggregate as pagg
from streeplijst.api.products import facts as pfacts
from streeplijst.eventsourcing import aggregate as agg


class TestAggregate:
    def test_unknown_fact_raises_exception(self, factstore, fact):
        aggregate = pagg.ProductAggregate(factstore, fact.subject)
        factstore.add_subscription(aggregate)
        with pytest.raises(agg.FactHandlerException):
            factstore.add_fact(fact)

    def test_when_stream_empty_product_raises(self, factstore):
        with pytest.raises(pagg.InvalidProductIdentifier):
            pagg.ProductAggregate(factstore, None)

    def test_product_created_on_create_fact(self, factstore, product):
        agg = pagg.ProductAggregate(factstore, product.identifier)
        factstore.add_fact(pfacts.ProductCreated(product))
        assert agg.product() == product

    def test_product_name_changed_on_set_name_fact(self, factstore, product):
        agg = pagg.ProductAggregate(factstore, product.identifier)
        factstore.add_fact(pfacts.ProductCreated(product))
        product.name = "name2"
        factstore.add_fact(pfacts.ProductNameChanged(product))
        assert agg.product().name == "name2"

    def test_product_stock_limit_changed_on_set_stock_limit_fact(
        self, factstore, product
    ):
        agg = pagg.ProductAggregate(factstore, product.identifier)
        factstore.add_fact(pfacts.ProductCreated(product))
        product.stock_limit = 1
        factstore.add_fact(pfacts.ProductStockLimitChanged(product))
        assert agg.product().stock_limit == 1

    def test_product_req_changed_on_set_req_fact(self, factstore, product):
        agg = pagg.ProductAggregate(factstore, product.identifier)
        factstore.add_fact(pfacts.ProductCreated(product))
        product.req_adult = True
        factstore.add_fact(pfacts.ProductAdultRequirementChanged(product))
        assert agg.product().req_adult is True
