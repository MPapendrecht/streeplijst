import pytest

from streeplijst.api.users import aggregate as uagg
from streeplijst.eventsourcing import aggregate as agg


class TestAggregate:
    def test_unknown_fact_raises_exception(self, factstore, fact, bank, vault):
        aggregate = uagg.UserAggregate(factstore, bank, vault, fact.subject)
        factstore.add_subscription(aggregate)
        with pytest.raises(agg.FactHandlerException):
            factstore.add_fact(fact)

    def test_when_stream_empty_product_raises(self, factstore, bank, vault):
        with pytest.raises(uagg.InvalidUserIdentifier):
            uagg.UserAggregate(factstore, bank, vault, None)
