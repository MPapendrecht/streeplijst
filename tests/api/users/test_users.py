import pytest

from streeplijst.domain.payment import InvalidPaymentVerification


class TestUsers:
    def testint_creating_a_new_user(self, userapi):
        user_state = {
            "name": "testuser",
            "balance": 0,
            "balance_limit": 2,
            "age": 1,
            "allow_negative": False,
            "mail": "",
        }
        user = userapi.create_new_user(**user_state).user()
        assert user.name == user_state["name"]
        assert user.age == user_state["age"]
        assert user.balance == user_state["balance"]
        assert user.balance_limit == user_state["balance_limit"]

    def testint_find_by_id(self, userapi):
        user_state = {
            "name": "testuser",
            "balance": 0,
            "balance_limit": 0,
            "age": 1,
            "allow_negative": False,
            "mail": "",
        }
        user = userapi.create_new_user(**user_state).user()
        found_user = userapi.find_by_id(user.identifier)
        assert found_user.user().identifier == user.identifier

    def testint_changing_name_of_user_update_aggregate(self, userapi, agg):
        userapi.set_name(agg.user().identifier, "martijn")
        assert agg.user().name == "martijn"

    def testint_changing_age_of_user_updates_aggregate(self, userapi, agg):
        userapi.set_age(agg.user().identifier, 19)
        assert agg.user().age == 19

    def testint_changing_balance_limit_of_user_updates_aggregate(self, userapi, agg):
        userapi.set_balance_limit(agg.user().identifier, 101)
        assert agg.user().balance_limit == 101

    def testint_payments_cannot_be_verified_twice(
        self, userapi, payment_api, present_date
    ):
        user = userapi.create_new_user("martijn", 0, 0, 19, False, "")
        payment_id = userapi.make_payment(user.user().identifier, 101)
        assert len(userapi.unverified_payments(user.user().identifier)) == 1
        payment_api.verify(payment_id, present_date)
        assert len(userapi.unverified_payments(user.user().identifier)) == 0
        with pytest.raises(InvalidPaymentVerification):
            payment_api.verify(payment_id, present_date)
