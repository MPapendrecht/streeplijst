import pytest

from streeplijst.api.users import service as srv
from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct


@pytest.fixture
def service(factstore, bank, vault):
    return srv.UserService(factstore, bank, vault)


class TestUserService:
    def test_finding_an_existing_user_returns_user(self, service):
        user = service.create_new_user("testuser", 0, 0, 1, False, "")
        assert service.find_by_id(user.user().identifier) == user

    def test_finding_all_users_returns_all_users(self, service):
        service.create_new_user("testuser", 0, 0, 1, False, "")
        service.create_new_user("testuser2", 0, 0, 1, False, "")
        assert len(service.all()) == 2

    def test_finding_unknown_user_raises_unknown_user_exception(self, service):
        with pytest.raises(srv.UnknownUser):
            service.find_by_id("identifier")

    def test_unknown_fact_raises_fact_handler_exception(self, factstore, bank, vault):
        aggregate = srv.CreatedUsersAggregate(factstore, bank, vault)
        with pytest.raises(agg.FactHandlerException):
            aggregate.handle_fact(fct.Fact())

    def test_service_restores_users_from_stream(self, factstore, bank, vault):
        service = srv.UserService(factstore, bank, vault)
        service.create_new_user("testuser", 0, 0, 1, False, "")
        new_service = srv.UserService(factstore, bank, vault)
        users = new_service.all()
        assert len(users) == 1

    def test_service_sets_name_of_users(self, factstore, bank, vault):
        service = srv.UserService(factstore, bank, vault)
        useragg = service.create_new_user("testuser", 0, 0, 1, False, "")
        user = useragg.user()
        service.set_name(user.identifier, "testuser2")
        assert useragg.user().name == "testuser2"

    def test_service_ignores_empty_transactions(self, factstore, bank, product, vault):
        service = srv.UserService(factstore, bank, vault)
        useragg = service.create_new_user("testuser", 0, 0, 1, False, "")
        user = useragg.user()
        eventcount = len(factstore._repository.get_all())
        service.buy(user.identifier, product, 0, 0)
        assert eventcount == len(factstore._repository.get_all())

    def test_service_sets_allow_negative_of_users(self, factstore, bank, vault):
        service = srv.UserService(factstore, bank, vault)
        useragg = service.create_new_user("testuser", 0, 0, 1, False, "")
        user = useragg.user()
        service.set_allow_negative(user.identifier, True)
        assert useragg.user().allow_negative is True

    def test_service_sets_mail_of_users(self, factstore, bank, vault):
        service = srv.UserService(factstore, bank, vault)
        useragg = service.create_new_user("testuser", 0, 0, 1, False, "initialmail")
        user = useragg.user()
        service.set_mail(user.identifier, "changedmail")
        assert useragg.user().mail == "changedmail"
