import pytest

from streeplijst.api.users import facts as ufcts
from streeplijst.eventsourcing import fact as fct


class TestUserFacts:
    def test_upcasting_to_itself_equals_itself(self, user):
        fact = ufcts.UserCreated(user)
        assert ufcts.UserCreated.upcast(fact) == fact

    @pytest.mark.parametrize(
        "kwargs",
        [
            {},
            {"_allow_negative": False},
            {"_allow_negative": False, "_name": None},
            {"_allow_negative": False, "_name": None, "_balance": None},
            {
                "_allow_negative": False,
                "_name": None,
                "_balance": None,
                "_balance_limit": None,
            },
        ],
    )
    def test_upcasting_user_created_from_none_raises_missing_data_exception(
        self, kwargs
    ):
        with pytest.raises(fct.IncompleteDataForCasting):
            ufcts.UserCreatedV1._cast(None, **kwargs)
        with pytest.raises(fct.IncompleteDataForCasting):
            ufcts.UserCreatedV2._cast(None, **kwargs)

    @pytest.mark.parametrize(
        "kwargs",
        [
            {},
            {"_product_id": None},
            {"_product_id": None, "_amount": None},
        ],
    )
    def test_upcasting_user_buys_from_none_raises_missing_data_exception(self, kwargs):
        with pytest.raises(fct.IncompleteDataForCasting):
            ufcts.UserBuysProduct._cast(None, **kwargs)
        with pytest.raises(fct.IncompleteDataForCasting):
            ufcts.UsersBuysProduct._cast(None, **kwargs)

    def test_upcasting_user_buys_to_users_buys(self, user, product):
        fact = ufcts.UserBuysProduct(user, product, 0, 0)
        new_fact = ufcts.UsersBuysProduct._cast(fact)
        assert isinstance(new_fact, ufcts.UsersBuysProduct)
        assert new_fact.other_users is not None

    def test_upcasting_users_buys_products_to_itself_remains_identical(
        self, user, product
    ):
        fact = ufcts.UsersBuysProduct(user, {user: 10}, product, 1, 20)
        new_fact = ufcts.UsersBuysProduct._cast(fact)
        assert isinstance(new_fact, ufcts.UsersBuysProduct)
        assert new_fact.other_users == fact.other_users
