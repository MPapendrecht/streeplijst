import pytest

from streeplijst.api.payments.service import PaymentService
from streeplijst.api.users import service as uss


@pytest.fixture
def userapi(factstore, bank, vault):
    return uss.UserService(factstore, bank, vault)


@pytest.fixture
def payment_api(factstore, bank):
    return PaymentService(factstore, bank)
