"""

This package contains the tests for the payments api.

"""

from tests.api.payments import test_service

__all__ = [
    "test_service",
]
