import pytest

from streeplijst.api.payments import facts as fcts
from streeplijst.eventsourcing import fact as fct
from streeplijst.util import identifier as idd


class TestUserMakesPaymentFact:
    def test_fact_casted_to_itself_is_equal(self, payment, user):
        fact = fcts.UserMakesPayment(payment, user)
        casted_fact = fcts.UserMakesPayment.upcast(fact)
        assert fact == casted_fact

    def test_fact_casted_downcasted_equals_itself(self, payment, user):
        fact = fcts.UserMakesPayment(payment, user)
        casted_fact = fcts.UserMakesPaymentV0._cast(fact)
        upcasted_fact = fcts.UserMakesPayment.upcast(
            casted_fact, _payment_id=fact.payment_id
        )
        assert fact == upcasted_fact

    @pytest.mark.parametrize("data", [{"_amount": 5}, {"_user_id": idd.Identifier()}])
    def test_fact_upcasted_without_parameters_raises_exception(self, data, fact):
        with pytest.raises(fct.IncompleteDataForCasting):
            fcts.UserMakesPaymentV0._cast(fact, **data)

    def test_upcasting_to_itself_is_equal(self, present_date):
        fact = fcts.PaymentVerified(idd.Identifier(), present_date)
        assert fcts.PaymentVerified.upcast(fact) == fact

    def test_upcasting_with_missing_data_raises_missing_data_exception(self):
        fact = fcts.PaymentVerifiedV0(idd.Identifier())
        with pytest.raises(fct.IncompleteDataForCasting):
            fcts.PaymentVerified.upcast(fact)
