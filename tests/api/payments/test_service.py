import pytest

from streeplijst.api.payments import service
from streeplijst.domain.payment import InvalidPaymentVerification
from streeplijst.eventsourcing import aggregate as agg


class TestPaymentsAggregate:
    def test_unknown_fact_raises_exception(self, factstore, bank, fact):
        pay_agg = service.CreatedPaymentsAggregate(factstore, bank)
        with pytest.raises(agg.FactHandlerException):
            pay_agg.handle_fact(fact)


class TestPaymentService:
    def test_verifing_a_payment_lists_in_all(self, userapi, payment_api, present_date):
        user = userapi.create_new_user("testuser", 0, 0, 1, False, "")
        payment1 = userapi.make_payment(user.user().identifier, 10)
        payment2 = userapi.make_payment(user.user().identifier, 20)
        assert len(payment_api.all()) == 2
        payment_api.verify(payment1, present_date)
        payment_api.verify(payment2, present_date)
        assert len(payment_api.all()) == 2

    def test_verifing_a_payment_does_not_list_in_unverified_payments(
        self, userapi, payment_api, present_date
    ):
        user = userapi.create_new_user("testuser", 0, 0, 1, False, "")
        payment1 = userapi.make_payment(user.user().identifier, 10)
        payment2 = userapi.make_payment(user.user().identifier, 20)
        assert len(payment_api.unverified_payments()) == 2
        payment_api.verify(payment1, present_date)
        payment_api.verify(payment2, present_date)
        assert len(payment_api.unverified_payments()) == 0
        assert len(userapi.payments(user.user().identifier)) == 2

    def test_verifying_a_payment_twice_is_not_allowed(
        self, userapi, payment_api, present_date
    ):
        user = userapi.create_new_user("testuser", 0, 0, 1, False, "")
        payment = userapi.make_payment(user.user().identifier, 10)
        payment_api.verify(payment, present_date)
        with pytest.raises(InvalidPaymentVerification):
            payment_api.verify(payment, present_date)
