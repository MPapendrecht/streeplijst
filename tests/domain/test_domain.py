import pytest

from streeplijst.domain import bank as bnk
from streeplijst.domain import product as prd
from streeplijst.domain import user as usr
from streeplijst.util import identifier as idd
from streeplijst.util import secrets


def _create_user(vault) -> usr.User:
    return usr.User(
        identifier=idd.Identifier(),
        name="testuser",
        balance=0,
        balance_limit=0,
        age=0,
        allow_negative=False,
        mail="",
        vault=vault,
    )


def _create_product() -> prd.Product:
    return prd.Product(
        identifier=idd.Identifier(),
        name="testproduct",
        stock=10,
        stock_limit=0,
        req_adult=False,
        price=10,
        profit=100,
    )


@pytest.mark.business_0_0_1
def test_user_flow(present_date):
    vault = secrets.MemoryVault()
    user = _create_user(vault)
    product = _create_product()
    bank = bnk.Bank()
    # Payment
    payment = bank.prepare_payment(user, 1000)
    bank.process_payment(user, payment)
    assert len(bank.unverified_payments(user)) == 1
    # Buying
    user.buy(product, amount=3, price=800)
    assert user.balance == 1000 - 800
    assert product.stock == 10 - 3
    assert product.profit == 100 + 800
    # Refunding
    user.buy(product, amount=-2, price=-800)
    assert product.profit == 100
    assert product.stock == 7 + 2
    # Buying
    user.buy(product, amount=8, price=1000)
    assert product.stock == 1
    assert user.balance == 0
    # Verifying payment
    payment.verify(present_date)
    assert len(bank.unverified_payments(user)) == 0
    # Allowing negative balances
    user.allow_negative = True
    user.buy(product, amount=1, price=600)
    assert user.balance == -600
