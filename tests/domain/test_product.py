import pytest

from streeplijst.domain import product as prd
from streeplijst.util import identifier as idd
from tests.domain import test_entity as tnnt


@pytest.fixture
def product_arguments():
    return {
        "identifier": idd.Identifier(),
        "name": "testproduct",
        "stock": 0,
        "stock_limit": 0,
        "price": 0,
        "profit": 0,
        "req_adult": False,
    }


class TestProduct(tnnt.TestEntity):
    def test_product_arguments(self, product_arguments):
        product = prd.Product(**product_arguments)
        assert product.name == product_arguments["name"]
        assert product.stock == product_arguments["stock"]
        assert product.stock_limit == product_arguments["stock_limit"]
        assert product.req_adult == product_arguments["req_adult"]
        assert product.profit == product_arguments["profit"]

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_name", [3, "", "*", "  "])
    def test_invalid_name_raises_invalid_name(self, product_arguments, invalid_name):
        product_arguments["name"] = invalid_name
        with pytest.raises(prd.InvalidProductName):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_stock", ["1.00", 1.00, 1j])
    def test_invalid_stock_raises_invalid_stock(self, product_arguments, invalid_stock):
        product_arguments["stock"] = invalid_stock
        with pytest.raises(prd.InvalidProductStock):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_stock_limit", ["1.00", 1.00, 1j])
    def test_invalid_stock_limit_raises_invalid_stock_limit(
        self, product_arguments, invalid_stock_limit
    ):
        product_arguments["stock_limit"] = invalid_stock_limit
        with pytest.raises(prd.InvalidProductStockLimit):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_req_adult", [[], None, 1, "x"])
    def test_invalid_req_adult_raises_invalid_req(
        self, product_arguments, invalid_req_adult
    ):
        product_arguments["req_adult"] = invalid_req_adult
        with pytest.raises(prd.InvalidProductAdultRequirement):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_price", [[], None, 1j, "x"])
    def test_invalid_price_invalid_price(self, product_arguments, invalid_price):
        product_arguments["price"] = invalid_price
        with pytest.raises(prd.InvalidProductPrice):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    def test_when_stock_is_above_limit_stock_is_not_low(self, product_arguments):
        product_arguments["stock_limit"] = product_arguments["stock"] - 1
        product = prd.Product(**product_arguments)
        assert not product.has_low_stock

    @pytest.mark.business_0_0_1
    def test_when_stock_is_below_limit_stock_is_low(self, product_arguments):
        product_arguments["stock_limit"] = product_arguments["stock"] + 1
        product = prd.Product(**product_arguments)
        assert product.has_low_stock

    @pytest.mark.business_0_0_1
    def test_when_stock_equal_stock_limit_it_is_low(self, product_arguments):
        product_arguments["stock_limit"] = product_arguments["stock"]
        product = prd.Product(**product_arguments)
        assert product.has_low_stock

    @pytest.mark.business_0_0_1
    def test_invested_is_an_integer(self, product_arguments):
        product_arguments["profit"] = None
        with pytest.raises(prd.InvalidProductProfit):
            prd.Product(**product_arguments)

    @pytest.mark.business_0_0_1
    def test_value_is_profit_plus_sold_stock(self, product_arguments):
        product_arguments["profit"] = -7
        product_arguments["stock"] = 5
        product_arguments["price"] = 2
        product = prd.Product(**product_arguments)
        assert product.value == 3

    def test_equality(self):
        product1 = prd.Product(idd.Identifier(), "prod1", 1, 1, True, 0, 0)
        product2 = prd.Product(idd.Identifier(), "prod2", 2, 2, False, 10, 2)
        assert product1 == product1
        assert product1 != product2
        product2 = prd.Product(product1.identifier, "prod2", 2, 2, False, 10, 2)
        assert product1 != product2
        product2.name = "prod1"
        assert product1 != product2
        product2.stock = 1
        assert product1 != product2
        product2.stock_limit = 1
        assert product1 != product2
        product2.req_adult = True
        assert product1 != product2
        product2.price = 0
        assert product1 != product2
        product2.profit = 0
        assert product1 == product2
        assert product1 != 3

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize(["amount", "price"], [(10, 100)])
    def test_adding_new_products_stock_and_investment_is_updated(
        self, product_arguments, amount, price
    ):
        product = prd.Product(**product_arguments)
        old_stock, old_investment = product.stock, product.profit
        product.exchange(amount=amount, price=price)
        assert product.stock == old_stock - amount
        assert product.profit == old_investment + price

    @pytest.mark.business_0_0_1
    def test_product_can_be_deleted(self, product_arguments):
        product = prd.Product(**product_arguments)
        product.delete()

    @pytest.mark.business_0_0_1
    def test_product_cannot_be_deleted_when_it_has_stock(self, product_arguments):
        product_arguments["stock"] = 1
        product = prd.Product(**product_arguments)
        with pytest.raises(prd.CannotDeleteProduct):
            product.delete()

    @pytest.mark.business_0_0_1
    def test_product_cannot_be_deleted_when_it_has_profit(self, product_arguments):
        product_arguments["profit"] = 1
        product = prd.Product(**product_arguments)
        with pytest.raises(prd.CannotDeleteProduct):
            product.delete()
