import pytest

from streeplijst.configs.config import config
from streeplijst.domain import user as usr
from streeplijst.util import identifier as idd
from streeplijst.util.secrets import MemoryVault
from tests.domain import test_entity as tnnt


@pytest.fixture
def user_arguments():
    return {
        "identifier": idd.Identifier(),
        "name": "testuser",
        "balance": 0,
        "balance_limit": 0,
        "age": 1,
        "allow_negative": False,
        "mail": "",
        "vault": MemoryVault(),
    }


class TestUser(tnnt.TestEntity):
    def test_properties_of_user_exist(self, user_arguments):
        user = usr.User(**user_arguments)
        assert user.name == user_arguments["name"]
        assert user.balance == user_arguments["balance"]
        assert user.balance_limit == user_arguments["balance_limit"]
        assert user.age == user_arguments["age"]
        assert user.allow_negative == user_arguments["allow_negative"]

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_name", [3, "", "*", "  "])
    def test_invalid_name_raises_invalid_name(self, user_arguments, invalid_name):
        user_arguments["name"] = invalid_name
        with pytest.raises(usr.InvalidUserName):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_balance", ["1.00", 1.00, 1j])
    def test_invalid_balance_raises_invalid_balance(
        self, user_arguments, invalid_balance
    ):
        user_arguments["balance"] = invalid_balance
        with pytest.raises(usr.InvalidBalance):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_balance_limit", ["1.00", 1.00, 1j])
    def test_invalid_balance_limit_raises_invalid_balance_limit(
        self, user_arguments, invalid_balance_limit
    ):
        user_arguments["balance_limit"] = invalid_balance_limit
        with pytest.raises(usr.InvalidBalanceLimit):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("invalid_age", [-1, 1j])
    def test_invalid_age_raises_invalid_age(self, user_arguments, invalid_age):
        user_arguments["age"] = invalid_age
        with pytest.raises(usr.InvalidAge):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_1
    def test_age_above_limit_is_adult(self, user_arguments):
        user_arguments["age"] = config().users.adult_age
        assert usr.User(**user_arguments).is_adult

    @pytest.mark.business_0_0_1
    def test_age_below_limit_is_not_adult(self, user_arguments):
        user_arguments["age"] = config().users.adult_age - 1
        assert not usr.User(**user_arguments).is_adult

    @pytest.mark.business_0_0_1
    def test_mail_cannot_be_unlocked_when_cleared_from_vault(self, user_arguments):
        vault = user_arguments["vault"]
        user = usr.User(**user_arguments)
        assert user.mail is not None
        vault.delete_owner(user.identifier.value)
        assert user.mail is None

    def test_mail_cannot_be_integer(self, user_arguments):
        user_arguments["mail"] = 3
        with pytest.raises(TypeError):
            usr.User(**user_arguments)

    def test_equality(self, vault):
        user1 = usr.User(idd.Identifier(), "user1", 1, 1, 1, True, "testmail", vault)
        user2 = usr.User(idd.Identifier(), "user2", 2, 2, 2, False, "othermail", vault)
        assert user1 == user1
        assert user1 != user2
        user2 = usr.User(user1.identifier, "user2", 2, 2, 2, False, "othermail", vault)
        assert user1 != user2
        user2.name = "user1"
        assert user1 != user2
        user2.balance = 1
        assert user1 != user2
        user2.balance_limit = 1
        assert user1 != user2
        user2.age = 1
        assert user1 != user2
        user2.allow_negative = True
        assert user1 != user2
        user2.mail = user1.mail
        assert user1 == user2
        assert user1 != 3

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize(["amount", "price"], [(10, 10), (0, 0), (101, 0), (0, 23)])
    def test_user_can_buy_a_product(self, user_arguments, product, amount, price):
        user_arguments["balance"] = 23
        user = usr.User(**user_arguments)
        old_balance, old_stock = user.balance, product.stock
        user.buy(product=product, amount=amount, price=price)
        assert user.balance == old_balance - price
        assert product.stock == old_stock - amount

    @pytest.mark.business_0_0_1
    def test_user_cannot_buy_more_than_it_has(self, user_arguments, product):
        user_arguments["balance"] = 0
        user = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidTransaction):
            user.buy(product=product, amount=0, price=1)

    @pytest.mark.business_0_0_2
    def test_user_can_buy_more_if_allowed(self, user_arguments, product):
        user_arguments["balance"] = 0
        user_arguments["allow_negative"] = True
        user = usr.User(**user_arguments)
        user.buy(product=product, amount=0, price=1)
        assert user.balance == -1

    @pytest.mark.business_0_0_2
    def test_disallow_negative_not_allowed_when_balance_is_negative(
        self, user_arguments
    ):
        user_arguments["allow_negative"] = True
        user_arguments["balance"] = -2
        user = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidAllowNegative):
            user.allow_negative = False

    @pytest.mark.parametrize("values", ["true", 1, {}])
    def test_invalid_allow_negative_raises_invalid_allow_negative(
        self, user_arguments, values
    ):
        user_arguments["allow_negative"] = values
        with pytest.raises(usr.InvalidAllowNegative):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_1
    def test_user_cannot_refund_more_than_bought(self, user_arguments, product):
        transaction_limit = config().users.transaction_cache_size
        user = usr.User(**user_arguments)
        user.buy(product, 1, 0)
        for _ in range(transaction_limit - 1):
            user.buy(product, 0, 0)
        with pytest.raises(usr.InvalidTransaction):
            user.buy(product, -2, 0)
        user.buy(product, -1, 0)

    @pytest.mark.business_0_0_1
    def test_user_remembers_transactions(self, user_arguments, product):
        transaction_limit = config().users.transaction_cache_size
        user = usr.User(**user_arguments)
        user.buy(product, 1, 0)
        for _ in range(transaction_limit - 1):
            user.buy(product, 0, 0)
        assert len(user.transactions) == transaction_limit
        user.buy(product, 0, 0)
        assert len(user.transactions) == transaction_limit

    @pytest.mark.business_0_0_1
    @pytest.mark.parametrize("amount", [10, 0, 23])
    def test_user_can_pay_to_increase_balance(self, user_arguments, amount):
        user = usr.User(**user_arguments)
        old_balance = user.balance
        user.pay(amount)
        assert user.balance == old_balance + amount

    @pytest.mark.business_0_0_1
    def test_user_cannot_buy_if_balance_insufficient(self, user_arguments, product):
        user = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidTransaction):
            user.buy(product, 0, user.balance + 1)

    @pytest.mark.business_0_0_1
    def test_user_cannot_buy_if_product_stock_insufficient(
        self, user_arguments, product
    ):
        user = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidTransaction):
            user.buy(product, product.stock + 1, 0)

    @pytest.mark.business_0_0_1
    def test_cannot_buy_more_than_maximum(self, user_arguments, product):
        user = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidTransaction):
            user.buy(product, config().transactions.maximum_amount + 1, 0)

    @pytest.mark.business_0_0_1
    def test_can_delete_user(self, user_arguments):
        user = usr.User(**user_arguments)
        user.delete()

    @pytest.mark.business_0_0_1
    def test_cannot_delete_user_with_balance(self, user_arguments):
        user_arguments["balance"] = 10
        user = usr.User(**user_arguments)
        with pytest.raises(usr.CannotDeleteUser):
            user.delete()

    @pytest.mark.business_0_0_2
    def test_can_transfer_money_between_users(self, user_arguments):
        user2 = usr.User(**user_arguments)
        user_arguments["balance"] = 10
        user1 = usr.User(**user_arguments)
        with pytest.raises(usr.InvalidTransaction):
            user1.transfer(100, user2)
        user1.transfer(9, user2)
        assert user1.balance == 1
        assert user2.balance == 9

    @pytest.mark.business_0_0_2
    def test_can_have_negative_balance_if_allowed(self, user_arguments):
        user_arguments["allow_negative"] = True
        user_arguments["balance"] = -10
        user1 = usr.User(**user_arguments)
        assert user1.balance == -10

    @pytest.mark.business_0_0_2
    def test_cannot_have_negative_balance_if_not_allowed(self, user_arguments):
        user_arguments["allow_negative"] = False
        user_arguments["balance"] = -10
        with pytest.raises(usr.InvalidBalance):
            usr.User(**user_arguments)

    @pytest.mark.business_0_0_2
    def test_user_can_transfer_any_money_to_itself(self, user_arguments):
        user_arguments["balance"] = 0
        user = usr.User(**user_arguments)
        user.transfer(1, user)
