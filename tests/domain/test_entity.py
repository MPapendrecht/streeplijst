import pytest

from streeplijst.domain import entity as ntt
from streeplijst.util import frozen_property as fpt
from streeplijst.util import identifier as idd


class TestEntity:
    def test_entity_has_identifier(self):
        identifier = idd.Identifier()
        entity = ntt.Entity(identifier)
        assert entity.identifier == identifier

    def test_identifier_is_frozen(self):
        identifier = idd.Identifier()
        entity = ntt.Entity(identifier)
        with pytest.raises(fpt.FrozenPropertyException):
            entity.identifier = idd.Identifier()

    def test_identifier_type_cannot_be_int(self):
        with pytest.raises(ntt.InvalidIdentifierType):
            ntt.Entity(3)
