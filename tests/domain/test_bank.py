import pytest

from streeplijst.domain import bank as bnk
from streeplijst.domain import payment as pay
from streeplijst.domain.payment import InvalidPayment
from streeplijst.util import identifier as idd


@pytest.fixture
def payment():
    return pay.Payment(amount=1)


class TestBank:
    def test_unknown_payments_raises_exception(self, present_date):
        bank = bnk.Bank()
        with pytest.raises(bnk.UnknownPayment):
            bank.verify_payment(idd.Identifier(), present_date)

    def test_known_payment_can_be_verified_via_id(self, payment, user, present_date):
        bank = bnk.Bank()
        bank.process_payment(user, payment)
        bank.verify_payment(payment.identifier, present_date)
        assert payment.is_verified

    def test_deleting_a_user_automatically_verifies_payments(self, payment, user):
        bank = bnk.Bank()
        bank.process_payment(user, payment)
        bank.process_payment(user, pay.Payment(amount=-payment.amount))
        bank.delete_user(user)
        assert len(bank.payments(user)) == 0

    def test_known_payment_can_be_verified_via_payment(
        self, payment, user, present_date
    ):
        bank = bnk.Bank()
        bank.process_payment(user, payment)
        bank.verify_payment(payment, present_date)
        assert payment.is_verified

    def test_payment_cannot_put_a_user_in_debt(self, user):
        user.balance = 0
        user.allow_negative = False
        bank = bnk.Bank()
        with pytest.raises(InvalidPayment):
            bank.prepare_payment(user, -1)
