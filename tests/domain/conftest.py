import pytest

from streeplijst.domain import product as prd
from streeplijst.util import identifier as idd


@pytest.fixture
def product():
    return prd.Product(idd.Identifier(), "testproduct", 101, 15, False, 100, 100)
