import pytest

from streeplijst.configs.config import config
from streeplijst.domain import payment as pay
from streeplijst.util import frozen_property as fpt
from streeplijst.util import identifier as idd
from tests.domain import test_entity as tnnt


class TestPayment(tnnt.TestEntity):
    def test_payment_with_identifier(self):
        identifier = idd.Identifier()
        payment = pay.Payment(amount=10, identifier=identifier)
        assert payment.identifier == identifier

    def test_amount_is_frozen(self):
        payment = pay.Payment(amount=10)
        with pytest.raises(fpt.FrozenPropertyException):
            payment.amount = 10

    @pytest.mark.business_0_0_1
    def test_payment_is_unverified_by_default(self):
        payment = pay.Payment(amount=10)
        assert not payment.is_verified

    @pytest.mark.business_0_0_1
    def test_payment_can_be_verified(self, present_date):
        payment = pay.Payment(amount=10)
        payment.verify(present_date)
        assert payment.is_verified

    @pytest.mark.business_0_0_1
    def test_payment_cannot_exceed_maximum(self):
        with pytest.raises(pay.InvalidPayment):
            pay.Payment(amount=config().payments.maximum_amount + 1)

    def test_payment_validation_date(self, present_date):
        payment = pay.Payment(amount=1)
        payment.verify(present_date)
        assert payment.validated_on == present_date

    def test_payment_cannot_be_unverified(self, present_date):
        payment = pay.Payment(amount=1)
        with pytest.raises(pay.InvalidPaymentDate):
            payment.verify(None)
