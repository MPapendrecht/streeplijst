import pytest
from fastapi import HTTPException

from streeplijst.server.exception_handlers import catch


class TestExceptionHandlers:
    def test_exception_handler_raises_from_regular_exception(self):
        def func():
            raise TypeError

        func = catch(400, (TypeError,))(func)
        with pytest.raises(HTTPException):
            func()
