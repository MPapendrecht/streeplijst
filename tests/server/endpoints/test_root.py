from fastapi import FastAPI
from fastapi.testclient import TestClient
from pytest import fixture

from streeplijst.server.endpoints import root


@fixture
def client(tmp_path):
    application = FastAPI()
    static_dir = tmp_path / "static"
    static_dir.mkdir()
    return TestClient(root.include_root(application, static_dir))


class TestRootEndpoint:
    def test_root_redirects_to_index(self, client):
        res = client.get("/", follow_redirects=False)
        assert res.url
