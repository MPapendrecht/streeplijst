import pytest
from fastapi.testclient import TestClient

from streeplijst.api.products.service import ProductService
from streeplijst.server.endpoints import products as prd


@pytest.fixture
def client(factstore):
    service = ProductService(factstore)
    return TestClient(prd.create_products_router(service))


class TestProductEndpoint:
    def test_creating_a_product(self, client):
        response = client.post("/products/")
        assert response.status_code == 200

    def test_getting_all_products(self, client):
        response = client.get("/products/")
        assert response.status_code == 200
        data = response.json()
        assert isinstance(data, list)
        assert len(data) == 0

    def test_retrieving_specific_product(self, client):
        product = client.post("/products/").json()
        response = client.get(f"/products/{product['identifier']}")
        assert response.status_code == 200
        assert response.json() == product

    def test_retrieving_unknown_product_raises_httpexception(self, client):
        response = client.get("/product/unknown_product")
        assert response.status_code == 404

    def test_setting_name(self, client):
        product = client.post("/products/").json()
        response = client.patch(
            f"/products/{product['identifier']}/name", params={"name": "testproduct 2"}
        )
        assert response.status_code == 204
        name = client.get(f"/products/{product['identifier']}/name")
        assert name.status_code == 200
        assert name.json() == "testproduct 2"

    def test_setting_req_adult(self, client):
        product = client.post("/products/").json()
        response = client.patch(
            f'/products/{product["identifier"]}/req_adult', params={"req_adult": True}
        )
        assert response.status_code == 204
        age = client.get(f"/products/{product['identifier']}/req_adult")
        assert age.status_code == 200
        assert age.json() is True

    def test_setting_stock_limit(self, client):
        product = client.post("/products/").json()
        response = client.patch(
            f'/products/{product["identifier"]}/stock_limit', params={"stock_limit": 10}
        )
        assert response.status_code == 204
        stock_limit = client.get(f"/products/{product['identifier']}/stock_limit")
        assert stock_limit.status_code == 200
        assert stock_limit.json() == 10

    def test_setting_price(self, client):
        product = client.post("/products/").json()
        response = client.patch(
            f"/products/{product['identifier']}/price", params={"price": 81}
        )
        assert response.status_code == 204
        price = client.get(f"/products/{product['identifier']}/price")
        assert price.status_code == 200
        assert price.json() == 81

    def test_exchanging_goods(self, client):
        product = client.post("/products/").json()
        response = client.post(
            f'/products/{product["identifier"]}/exchange',
            params={"amount": -10, "price": -101},
        )
        assert response.status_code == 204
        stock = client.get(f"/products/{product['identifier']}/stock")
        assert stock.status_code == 200
        assert stock.json() == 10
        profit = client.get(f"/products/{product['identifier']}/profit")
        assert profit.status_code == 200
        assert profit.json() == -101

    def test_deleting_product(self, client):
        product = client.post("/products/").json()
        response = client.delete(f"/products/{product['identifier']}")
        assert response.status_code == 204
        products = client.get("/products/").json()
        assert len(products) == 0

    def test_total_profit(self, client):
        product = client.post("/products/").json()
        client.post(
            f"/products/{product['identifier']}/exchange",
            params={"amount": 0, "price": 10},
        )
        response = client.get("/products/totalprofit/")
        assert response.status_code == 200
        totalprofit = response.json()
        assert totalprofit == 10

    def test_total_value(self, client):
        product = client.post("/products/").json()
        client.patch(f"/products/{product['identifier']}/price", params={"price": 8})
        client.post(
            f"/products/{product['identifier']}/exchange",
            params={"amount": -10, "price": 10},
        )
        response = client.get("/products/totalvalue")
        assert response.status_code == 200
        totalvalue = response.json()
        assert totalvalue == 8 * 10 + 10
