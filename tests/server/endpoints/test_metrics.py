import pytest
from fastapi.testclient import TestClient

from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService
from streeplijst.server.endpoints import metrics as met
from streeplijst.server.endpoints import products as prd
from streeplijst.server.endpoints import users as usr


@pytest.fixture
def client(factstore, bank, vault):
    user_service = UserService(factstore, bank, vault)
    product_service = ProductService(factstore)
    payment_service = PaymentService(factstore, bank)
    return TestClient(
        met.create_metrics_router(user_service, product_service, payment_service)
    )


@pytest.fixture
def user_client(factstore, bank, vault):
    return TestClient(
        usr.create_users_router(
            UserService(factstore, bank, vault), ProductService(factstore)
        )
    )


@pytest.fixture
def product_client(factstore):
    return TestClient(prd.create_products_router(ProductService(factstore)))


class TestMetricsEndpoint:
    def test_endpoint_returns_json(self, client):
        response = client.get("/metrics")
        assert response.status_code == 200
        assert response.json()

    def testint_negative_balance_is_summed(self, client, user_client, product_client):
        user_client.post(
            "/users/", params={"balance": -10, "allow_negative": True}
        ).json()
        response = client.get("/metrics")
        assert response.status_code == 200
        data = response.json()
        assert data["negative_balance"] == -10
        assert data["total_value"] == 10
