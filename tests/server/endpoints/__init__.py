"""

This package contains the tests for the server endpoints.

"""

from tests.server.endpoints import test_payments, test_products, test_users

__all__ = [
    "test_products",
    "test_payments",
    "test_users",
]
