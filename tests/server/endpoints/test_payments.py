import pytest
from fastapi.testclient import TestClient

from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService
from streeplijst.server.endpoints import payments as pay
from streeplijst.server.endpoints import users as usr


@pytest.fixture
def user_client(factstore, bank, vault):
    return TestClient(
        usr.create_users_router(
            UserService(factstore, bank, vault), ProductService(factstore)
        )
    )


@pytest.fixture
def client(factstore, bank):
    service = PaymentService(factstore, bank)
    return TestClient(pay.create_payments_router(service))


class TestPaymentsEndpoint:
    def testint_verifying_payments(self, client, user_client, present_date):
        user = user_client.post("/users/").json()
        user_client.post(f'/users/{user["identifier"]}/payments', params={"amount": 10})
        payments = user_client.get(
            f"/users/{user['identifier']}/payments?skip_verified=true"
        ).json()
        response = client.post(
            f"/payments/{payments[0]['identifier']}/verify",
            params={"validation_datetime": present_date},
        )
        assert response.status_code == 204

    def test_all_payments(self, client, user_client):
        response = client.get("/payments/")
        assert response.status_code == 200
        assert len(response.json()) == 0
        user = user_client.post("/users/").json()
        user_client.post(f'/users/{user["identifier"]}/payments', params={"amount": 10})
        payments = client.get("/payments/").json()
        assert len(payments) == 1
        assert not payments[0]["verified"]

    def test_unverified_payments(self, client, user_client, present_date):
        response = client.get("/payments/")
        assert response.status_code == 200
        assert len(response.json()) == 0
        user = user_client.post("/users/").json()
        payment_id = user_client.post(
            f'/users/{user["identifier"]}/payments', params={"amount": 10}
        ).json()["payment_id"]
        client.post(
            f"/payments/{payment_id}/verify",
            params={"validation_datetime": present_date},
        )
        payments = client.get("/payments/").json()
        assert len(payments) == 1
        payments = client.get("/payments/?skip_verified=true").json()
        assert len(payments) == 0
