import datetime as dt

import pytest
from fastapi import HTTPException
from fastapi.testclient import TestClient

from streeplijst.api.payments.service import PaymentService
from streeplijst.api.products.service import ProductService
from streeplijst.api.users.service import UserService
from streeplijst.server.endpoints import payments as pay
from streeplijst.server.endpoints import products as prd
from streeplijst.server.endpoints import users as usr


@pytest.fixture
def product_service(factstore):
    return ProductService(factstore)


@pytest.fixture
def product_client(product_service):
    return TestClient(prd.create_products_router(product_service))


@pytest.fixture
def payment_client(factstore, bank):
    return TestClient(pay.create_payments_router(PaymentService(factstore, bank)))


@pytest.fixture
def client(factstore, product_service, bank, vault):
    service = UserService(factstore, bank, vault)
    return TestClient(usr.create_users_router(service, product_service))


class TestUsersEndpoint:
    def test_creating_a_user(self, client):
        response = client.post("/users/")
        assert response.status_code == 200

    def test_getting_all_users(self, client):
        response = client.get("/users/")
        assert response.status_code == 200
        data = response.json()
        assert isinstance(data, list)
        assert len(data) == 0

    def test_retrieving_specific_user(self, client):
        user = client.post("/users/").json()
        response = client.get(f"/users/{user['identifier']}")
        assert response.status_code == 200
        assert response.json() == user

    def test_retrieving_unknown_user_raises_httpexception(self, client):
        with pytest.raises(HTTPException) as exception:
            client.get("/users/unknown_user")
        exception = exception.value
        assert exception.status_code == 404
        assert "unknown_user" in exception.detail

    def test_setting_name(self, client):
        user = client.post("/users/").json()
        response = client.patch(
            f"/users/{user['identifier']}/name", params={"name": "testuser 2"}
        )
        assert response.status_code == 204
        name = client.get(f"/users/{user['identifier']}/name")
        assert name.status_code == 200
        assert name.json() == "testuser 2"

    def test_setting_age(self, client):
        user = client.post("/users/").json()
        response = client.patch(f'/users/{user["identifier"]}/age', params={"age": 10})
        assert response.status_code == 204
        age = client.get(f"/users/{user['identifier']}/age")
        assert age.status_code == 200
        assert age.json() == 10

    def test_setting_mail(self, client):
        user = client.post("/users/").json()
        response = client.patch(
            f"/users/{user['identifier']}/mail", params={"mail": "testmail"}
        )
        assert response.status_code == 204

    def test_setting_allow_negative(self, client):
        user = client.post("/users/").json()
        response = client.patch(
            f'/users/{user["identifier"]}/allow_negative',
            params={"allow_negative": True},
        )
        assert response.status_code == 204
        age = client.get(f"/users/{user['identifier']}/allow_negative")
        assert age.status_code == 200
        assert age.json() is True

    def test_setting_balance_limit(self, client):
        user = client.post("/users/").json()
        response = client.patch(
            f'/users/{user["identifier"]}/balance_limit', params={"balance_limit": 10}
        )
        assert response.status_code == 204
        balance_limit = client.get(f"/users/{user['identifier']}/balance_limit")
        assert balance_limit.status_code == 200
        assert balance_limit.json() == 10

    def testint_making_payment_increases_balance(self, client):
        user = client.post("/users/").json()
        response = client.post(
            f'/users/{user["identifier"]}/payments', params={"amount": 10}
        )
        assert response.status_code == 200
        balance = client.get(f"/users/{user['identifier']}/balance")
        assert balance.status_code == 200
        assert balance.json() == 10
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=true"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 1

    def testint_verified_payment_listed_when_requested(
        self, client, payment_client, present_date
    ):
        user = client.post("/users/").json()
        response = client.post(
            f'/users/{user["identifier"]}/payments', params={"amount": 10}
        )
        assert response.status_code == 200
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=false"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 1
        payment_id = payments.json()[0]["identifier"]
        assert (
            payment_client.post(
                f"payments/{payment_id}/verify",
                params={"validation_datetime": present_date},
            ).status_code
            == 204
        )
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=false"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 1

    def testint_verified_payment_not_listed(self, client, payment_client, present_date):
        user = client.post("/users/").json()
        response = client.post(
            f'/users/{user["identifier"]}/payments', params={"amount": 10}
        )
        assert response.status_code == 200
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=true"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 1
        payment_id = payments.json()[0]["identifier"]
        assert (
            payment_client.post(
                f"payments/{payment_id}/verify",
                params={"validation_datetime": present_date},
            ).status_code
            == 204
        )
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=true"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 0

    def testint_buying_products_decreases_balance(self, client, product_client):
        user = client.post("/users/", params={"balance": 101}).json()
        product = product_client.post("/products/", params={"stock": 10}).json()
        response = client.post(
            f'/users/{user["identifier"]}/buy/{product["identifier"]}',
            params={"amount": 10, "price": 101},
        )
        assert response.status_code == 204
        assert client.get(f"/users/{user['identifier']}/balance").json() == 0
        assert (
            product_client.get(f"/products/{product['identifier']}/stock").json() == 0
        )
        assert (
            product_client.get(f"/products/{product['identifier']}/profit").json()
            == 101
        )

    def test_deleting_user(self, client):
        user = client.post("/users/").json()
        response = client.delete(f"/users/{user['identifier']}")
        assert response.status_code == 204

    def test_deleting_user_with_balance(self, client):
        user = client.post("/users/", params={"balance": 101}).json()
        with pytest.raises(HTTPException):
            client.delete(f"/users/{user['identifier']}")

    def test_deleting_user_with_unverified_payments(self, client, present_date):
        user = client.post("/users/").json()
        client.post(f"/users/{user['identifier']}/payments", params={"amount": 1})
        with pytest.raises(HTTPException):
            client.delete(f"/users/{user['identifier']}")

    def test_bulk_buy(self, client, product_client):
        user1 = client.post("/users/", params={"balance": 101}).json()
        user2 = client.post("/users/", params={"balance": 35}).json()
        product = product_client.post("/products/", params={"stock": 23}).json()
        data = [
            {
                "user_id": user1["identifier"],
                "other_users": {},
                "product_id": product["identifier"],
                "amount": 0,
                "price": 0,
            },
            {
                "user_id": user2["identifier"],
                "other_users": {user1["identifier"]: 20},
                "product_id": product["identifier"],
                "amount": 1,
                "price": 1,
            },
        ]
        response = client.post("/users/bulk_buy", json=data)
        assert response.status_code == 204

    def test_bulk_buy_reports_errors(self, client, product_client):
        user1 = client.post("/users/", params={"balance": 0}).json()
        user2 = client.post("/users/", params={"balance": 0}).json()
        product = product_client.post("/products/", params={"stock": 23}).json()
        data = [
            {
                "user_id": user1["identifier"],
                "other_users": [],
                "product_id": product["identifier"],
                "amount": 1,
                "price": 1,
            },
            {
                "user_id": user2["identifier"],
                "other_users": [],
                "product_id": product["identifier"],
                "amount": 1,
                "price": 1,
            },
        ]
        with pytest.raises(HTTPException) as e:
            client.post("/users/bulk_buy", json=data)
        assert len(e.value.detail.split("\n")) == 2

    def test_retrieving_transactions_return_list_of_transactions(
        self, client, product_client
    ):
        user = client.post("/users/", params={"balance": 100}).json()
        product = product_client.post("/products/", params={"stock": 20}).json()
        client.post(
            f"/users/{user['identifier']}/buy/{product['identifier']}",
            params={"amount": 1, "price": 10},
        )
        client.post(
            f"/users/{user['identifier']}/buy/{product['identifier']}",
            params={"amount": 1, "price": 10},
        )
        client.post(
            f"/users/{user['identifier']}/buy/{product['identifier']}",
            params={"amount": 1, "price": 10},
        )
        transactions = client.get(f"/users/{user['identifier']}/transactions").json()
        assert len(transactions) == 3

    def test_payment_of_0_amount_is_skipped(self, client, payment_client):
        user = client.post("/users/", params={"balance": 100}).json()
        assert (
            client.post(
                f"/users/{user['identifier']}/payments", params={"amount": 0}
            ).status_code
            == 200
        )
        assert (
            client.post(
                f"/users/{user['identifier']}/payments", params={"amount": 1}
            ).status_code
            == 200
        )
        payments = client.get(
            f"/users/{user['identifier']}/payments?skip_verified=true"
        )
        assert payments.status_code == 200
        assert len(payments.json()) == 1

    def test_create_invoice(
        self, client, payment_client, requests_mock, billing_config
    ):
        requests_mock.post(billing_config.entrypoint, text="test", status_code=200)
        user = client.post("/users/").json()
        client.post(
            f"/users/{user['identifier']}/payments", params={"amount": 30}
        ).json()
        client.post(
            f"/users/{user['identifier']}/invoice",
            params={
                "start_date": str(dt.date(1990, 1, 1)),
                "end_date": str(dt.date(2000, 1, 1)),
            },
        )

    def test_create_invoice_with_error_raises_exception(
        self, client, payment_client, requests_mock, billing_config
    ):
        requests_mock.post(billing_config.entrypoint, text="test", status_code=500)
        user = client.post("/users/").json()
        client.post(
            f"/users/{user['identifier']}/payments", params={"amount": 30}
        ).json()
        with pytest.raises(HTTPException) as exception:
            client.post(
                f"/users/{user['identifier']}/invoice",
                params={
                    "start_date": str(dt.date(1990, 1, 1)),
                    "end_date": str(dt.date(2000, 1, 1)),
                },
            )
        assert exception.value.status_code == 500

    def test_create_invoice_without_config_fails(
        self, client, payment_client, requests_mock
    ):
        requests_mock.post("http://localhost/invoice", text="test", status_code=200)
        user = client.post("/users/").json()
        client.post(
            f"/users/{user['identifier']}/payments", params={"amount": 30}
        ).json()
        with pytest.raises(HTTPException) as exception:
            client.post(
                f"/users/{user['identifier']}/invoice",
                params={
                    "start_date": str(dt.date(1990, 1, 1)),
                    "end_date": str(dt.date(2000, 1, 1)),
                },
            )
        assert exception.value.status_code == 403
