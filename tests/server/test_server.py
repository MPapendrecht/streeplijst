import fastapi

from streeplijst.server.app import dev_app
from streeplijst.util import debug


def test_building_dev_app(tmp_path):
    static_dir = tmp_path / "src" / "streeplijst" / "server" / "static"
    static_dir.mkdir(parents=True)
    assert static_dir.exists()
    assert static_dir.is_dir()
    try:
        assert isinstance(dev_app(), fastapi.FastAPI)
    finally:
        debug.disable_logging()
