import pytest

from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.eventsourcing import messaging as msg
from tests.eventsourcing.conftest import _make_subscriber


class TestFactDescriptor:
    def test_factdescriptor_does_not_match_integers(self):
        descriptor = fcs.FactDescriptor()
        assert descriptor.match(1) is False

    def test_factdescriptor_requires_fact_type(self):
        with pytest.raises(TypeError):
            fcs.FactDescriptor(1)

    def test_factdescriptor_equals_itself(self):
        descriptor = fcs.FactDescriptor()
        assert descriptor == descriptor

    def test_factdescriptor_equals_same_arguments(self):
        assert fcs.FactDescriptor(name="name") == fcs.FactDescriptor(name="name")

    def test_factdescriptor_does_not_equal_integer(self):
        assert fcs.FactDescriptor() != 0

    def test_factdescriptor_does_not_equal_when_requirements_differ(self):
        assert fcs.FactDescriptor(name="name") != fcs.FactDescriptor(name="name2")
        assert fcs.FactDescriptor(name="name") != fcs.FactDescriptor(name2="name")
        assert fcs.FactDescriptor(name="name") != fcs.FactDescriptor(
            name="name", name2="name2"
        )

    def test_factdescriptor_has_repr(self):
        assert (
            repr(fcs.FactDescriptor(fct.Fact, name="name"))
            == "FactDescriptor(Fact, name=name)"
        )


class TestFactStore:
    def test_factstore_with_subscription_passes_facts(self, factstore, subscriber):
        class TestFact(fct.Fact, label="TestFact"):
            pass

        testfact = TestFact()
        factstore.add_subscription(
            subscriber, fcs.FactDescriptor(TestFact, subject=testfact.subject)
        )
        factstore.add_fact(testfact)
        factstore.add_fact(fct.Fact())
        assert len(subscriber.logs) == 1
        facts = subscriber.logs[0]
        assert facts == (testfact,)

    def test_factstore_does_not_pass_facts_after_unsubscription(
        self, factstore, subscriber, fact
    ):
        subscription = factstore.add_subscription(
            subscriber, fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        factstore.remove_subscription(subscription)
        factstore.add_fact(fact)
        assert len(subscriber.logs) == 0

    def test_factstore_doesnt_destroy_other_subscription_on_unsubscribe(
        self, factstore, subscriber, fact
    ):
        subscription = factstore.add_subscription(
            subscriber, fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        new_subscriber = _make_subscriber()
        factstore.add_subscription(
            new_subscriber, fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        factstore.remove_subscription(subscription)
        factstore.add_fact(fact)
        assert len(new_subscriber.logs) == 1

    def test_factstore_does_not_duplicate_facts_after_double_subscription(
        self, factstore, subscriber, fact
    ):
        factstore.add_subscription(
            subscriber, fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        factstore.add_subscription(
            subscriber, fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        factstore.add_fact(fact)
        assert len(subscriber.logs) == 1

    def test_factstore_only_passes_facts_to_subscribers(self, factstore, fact):
        class TestFact(fct.Fact, label="TestFact"):
            pass

        testfact = TestFact()
        subscribers = [_make_subscriber(), _make_subscriber(), _make_subscriber()]
        factstore.add_subscription(
            subscribers[0], fcs.FactDescriptor(fct.Fact, subject=fact.subject)
        )
        factstore.add_subscription(
            subscribers[1], fcs.FactDescriptor(TestFact, subject=testfact.subject)
        )
        factstore.add_fact(fact)
        factstore.add_fact(testfact)
        assert len(subscribers[0].logs) == 1
        assert len(subscribers[1].logs) == 1
        assert len(subscribers[2].logs) == 0

    def test_factstore_passes_any_fact(self, factstore, subscriber, fact):
        factstore.add_subscription(subscriber)
        factstore.add_fact(fact)
        assert len(subscriber.logs) == 1

    def test_factstore_fails_to_add_non_fact(self, factstore):
        with pytest.raises(TypeError):
            factstore.add_fact(None)


class TestStream:
    def test_stream_loads_from_repository(self, fact_repo, fact):
        fact_repo.add(fact)
        stream = fcs.Stream(fact_repo, (fcs.FactDescriptor(),))
        assert stream.get(1) == (fact,)

    def test_stream_gets_everything_by_default(self, fact_repo, fact):
        fact_repo.add(fact)
        fact_repo.add(fact)
        stream = fcs.Stream(fact_repo, (fcs.FactDescriptor(),))
        assert stream.get() == (
            fact,
            fact,
        )


@pytest.fixture
def stream(fact_repo):
    return fcs.Stream(fact_repo, (fcs.FactDescriptor(),))


class TestStreamSubscription:
    def test_subscription_differs_from_subscriber(self, subscriber, stream):
        subscription = fcs.StreamSubscription(subscriber, stream)
        assert subscription != subscriber

    def test_subscription_differs_from_subscription(self, subscriber, stream):
        subscription = fcs.StreamSubscription(subscriber, stream)
        assert subscription != msg.Subscription(subscriber)

    def test_subscriptions_differ_when_streams_differ(
        self, subscriber, stream, fact_repo
    ):
        stream2 = fcs.Stream(fact_repo, (fcs.FactDescriptor(),))
        assert stream != stream2
        assert fcs.StreamSubscription(subscriber, stream) != fcs.StreamSubscription(
            subscriber, stream2
        )


class TestReplay:
    def test_locked_repository_limits_access(self, fact, repository, subscriber):
        factstore = fcs.FactStore(repository)
        factstore.add_fact(fact)
        repository.lock_for_replay()
        factstore.add_subscription(subscriber, fcs.FactDescriptor())
        with pytest.raises(KeyError):
            factstore.add_fact(fact)

    def test_locked_repository_can_replay(self, fact, repository, subscriber):
        factstore = fcs.FactStore(repository)
        factstore.add_fact(fact)
        factstore.add_fact(fact)
        repository.lock_for_replay()
        subscriber = factstore.add_subscription(subscriber, fcs.FactDescriptor())
        factstore.replay(1)
        assert len(subscriber.get_facts()) == 1
