import pickle

import pytest

from streeplijst.eventsourcing import repository as rpy


class TestFileStorage:
    def test_storages_are_linked(self, tmp_path, fact):
        file = tmp_path / "test.repo"
        storage = rpy.FileStorage(file)
        storage.add(fact)
        assert len(storage.get_all()) == 1
        new_storage = rpy.FileStorage(file)
        assert len(new_storage.get_all()) == 1

    def test_storages_create_unknown_folders(self, tmp_path):
        file = tmp_path / "sub" / "test.repo"
        storage = rpy.FileStorage(file)
        assert len(storage.get_all()) == 0

    def test_loading_raises_exception(self, tmp_path):
        file = tmp_path / "test.repo"
        with open(file, "w") as f:
            f.write("hello world")
        with pytest.raises(pickle.UnpicklingError):
            rpy.FileStorage(file)
