from datetime import datetime

import pytest

from streeplijst.eventsourcing import fact as fct
from streeplijst.util import frozen_property as fpt
from streeplijst.util import identifier as idd


class TestFact:
    def test_subject_is_immutable(self):
        fact = fct.Fact()
        with pytest.raises(fpt.FrozenPropertyException):
            fact.subject = "new_subject"
        with pytest.raises(fpt.FrozenPropertyException):
            fact.datetime = "new_subject"

    def test_fact_has_subject(self):
        fact = fct.Fact()
        assert isinstance(fact.subject, idd.Identifier)

    def test_has_creation_time(self):
        fact = fct.Fact()
        assert isinstance(fact.datetime, datetime)
        assert (datetime.now() - fact.datetime).total_seconds() < 1

    def test_fact_subject_accessible(self):
        frozenproperty = fct.Fact.subject
        with pytest.raises(fpt.FrozenPropertyException):
            frozenproperty.__set__(None, None)
        with pytest.raises(fpt.FrozenPropertyException):
            frozenproperty.__delete__(None)

    def test_fact_label_accessible(self):
        label = fct.Fact.label
        assert isinstance(label, str)

    def test_fact_with_given_subject(self):
        fact = fct.Fact("subject")
        assert isinstance(fact.subject, idd.Identifier)

    def test_fact_with_given_creation_time(self):
        now = datetime.now()
        fact = fct.Fact(creation_datetime=now)
        assert fact.datetime == now

    def test_fact_with_incorrect_creation_type(self):
        with pytest.raises(TypeError):
            fct.Fact(creation_datetime="")

    def test_fact_equals_fact_if_parameters_are_equal(self):
        subject = "subject"
        creation_time = datetime.now()
        fact1 = fct.Fact(subject, creation_time)
        fact2 = fct.Fact(subject, creation_time)
        assert fact1 == fact2

    def test_fact_not_equal_when_subjects_differ(self):
        fact1 = fct.Fact("1")
        fact2 = fct.Fact("2")
        assert fact1 != fact2

    def test_fact_not_equal_when_creation_times_differ(self):
        fact1 = fct.Fact(subject="", creation_datetime=datetime(1990, 1, 1, 10, 10, 10))
        fact2 = fct.Fact(subject="", creation_datetime=datetime(1990, 1, 1, 11, 10, 10))
        assert fact1 != fact2

    def test_fact_equals_itself(self):
        fact = fct.Fact()
        assert fact == fact

    def test_fact_does_not_equal_integer(self):
        fact = fct.Fact()
        assert fact != 1

    def test_label_must_be_string(self):
        with pytest.raises(TypeError):

            class TestFact(fct.Fact, label=0):
                pass

    def test_label_cannot_be_empty(self):
        with pytest.raises(ValueError):

            class TestFact(fct.Fact, label=""):
                pass

    def test_version_must_be_integer(self):
        with pytest.raises(TypeError):

            class TestFact(fct.Fact, label="testlabel", version="version"):
                pass

    def test_casting_to_self_is_equal(self):
        fact = fct.Fact()
        assert fct.Fact.upcast(fact) == fact

    def test_fact_label_is_not_allowed(self):
        with pytest.raises(ValueError):

            class TestFact(fct.Fact, label=None):
                pass

    def test_upcasting_to_subclass_returns_upcasted_class(self):
        class NewFact(fct.Fact, label="NewFact"):
            pass

        class NewFact2(NewFact):
            pass

        fact = NewFact()
        assert isinstance(NewFact2.upcast(fact), NewFact2)

    def test_upcasting_with_different_labels_is_not_allowed(self):
        class NewFact(fct.Fact, label="NewFact"):
            pass

        with pytest.raises(AssertionError):
            NewFact.upcast(fct.Fact())

    def test_equal_facts_have_equal_label(self):
        class TestFact(fct.Fact, label="test", version=1):
            pass

        class TestFact2(fct.Fact, label="test2", version=1):
            pass

        timestamp = datetime(1990, 1, 1, 10, 10, 10)
        fact1 = TestFact("subject", timestamp)
        fact2 = TestFact2("subject", timestamp)
        assert fact1 != fact2

    def test_equal_facts_have_equal_version(self):
        class TestFact(fct.Fact, label="test", version=1):
            pass

        class TestFact2(fct.Fact, label="test", version=2):
            pass

        timestamp = datetime(1990, 1, 1, 10, 10, 10)
        fact1 = TestFact("subject", timestamp)
        fact2 = TestFact2("subject", timestamp)
        assert fact1 != fact2

    def test_unregistered_facts_raise_error(self):
        with pytest.raises(fct.FactTypeNotRegisteredError):
            fct.get_fact_definition("unknown", 101)

    def test_negative_version_raise_error(self):
        with pytest.raises(ValueError):

            class TestFact(fct.Fact, label="TestFact", version=-1):
                pass

    def test_duplicate_classes_are_not_allowed(self):
        class TestFact(fct.Fact, label="TestFact", version=1):
            pass

        class TestFact2(fct.Fact, label="TestFact", version=1):
            pass

        fct.register_fact_definition(TestFact)
        with pytest.raises(KeyError):
            fct.register_fact_definition(TestFact2)

    def test_latest_version_is_updated_on_registration(self):
        class TestFact(fct.Fact, label="testFact", version=1):
            pass

        class TestFact2(fct.Fact, label="testFact", version=2):
            pass

        try:
            fct.register_fact_definition(TestFact)
            fct.register_fact_definition(TestFact2)
            assert fct.get_latest_version("testFact") == 2
        finally:
            fct.deregister_fact_definition(TestFact)
            fct.deregister_fact_definition(TestFact2)

    def test_latest_version_is_not_overwritten_on_registration(self):
        class TestFact(fct.Fact, label="testFact", version=1):
            pass

        class TestFact2(fct.Fact, label="testFact", version=2):
            pass

        try:
            fct.register_fact_definition(TestFact2)
            fct.register_fact_definition(TestFact)
            assert fct.get_latest_version("testFact") == 2
        finally:
            fct.deregister_fact_definition(TestFact)
            fct.deregister_fact_definition(TestFact2)

    def test_cannot_deregister_unregistered_fact(self):
        class TestFact(fct.Fact, label="testFact"):
            pass

        with pytest.raises(fct.FactTypeNotRegisteredError):
            fct.deregister_fact_definition(TestFact)
