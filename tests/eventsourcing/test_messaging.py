from streeplijst.eventsourcing import messaging as msg
from tests.eventsourcing.conftest import MockSubscriber


class TestSubscription:
    def test_subscription_equals_itself(self, subscriber):
        subscription = msg.Subscription(subscriber)
        assert subscription == subscription

    def test_subscription_not_equals_subscriber(self, subscriber):
        assert msg.Subscription(subscriber) != subscriber

    def test_subscription_equal_with_same_subscriber(self, subscriber):
        assert msg.Subscription(subscriber) == msg.Subscription(subscriber)

    def test_subscription_differs_with_different_subscriber(self, subscriber):
        assert msg.Subscription(subscriber) != msg.Subscription(MockSubscriber())

    def test_subscription_is_hashable(self, subscriber):
        assert len({msg.Subscription(subscriber)}) == 1
