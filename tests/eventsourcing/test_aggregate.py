import pytest

from streeplijst.eventsourcing import aggregate as agg
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.eventsourcing import messaging as msg
from streeplijst.util.translate import LANGUAGES


class Add(fct.Fact, label="Add"):
    def __init__(self, amount: int):
        super().__init__()
        self.amount = amount


class CountingAggregate(agg.AutoAggregate):
    def __init__(self, factstore: fcs.FactStore):
        super(CountingAggregate, self).__init__(factstore, (fcs.FactDescriptor(),))
        self._total = 0

    def handle_fact(self, fact: fct.Fact):
        if isinstance(fact, Add):
            self._total += fact.amount
            return
        raise agg.FactHandlerException().with_error_msg(
            LANGUAGES.ENGLISH, f"Received an unknown fact {fact}."
        )

    def total(self):
        return self._total


class TestAggregate:
    def test_aggregate_can_handle_facts(self, factstore):
        aggregate = CountingAggregate(factstore)
        assert aggregate.total() == 0
        addition = Add(3)
        factstore.add_fact(addition)
        assert aggregate.total() == 3
        factstore.add_fact(addition)
        assert aggregate.total() == 6

    def test_aggregate_raises_on_unknown_fact(self, factstore, fact):
        CountingAggregate(factstore)
        with pytest.raises(agg.FactHandlerException):
            factstore.add_fact(fact)

    def test_aggregate_skips_unknown_subscriptions(self, factstore, subscriber):
        aggregate = CountingAggregate(factstore)
        aggregate.notify(msg.Subscription(subscriber), fcs.Notification(3))

    def test_aggregate_skips_unknown_notifications(
        self, factstore, subscriber, fact_repo
    ):
        aggregate = CountingAggregate(factstore)
        aggregate.notify(
            fcs.StreamSubscription(
                subscriber, fcs.Stream(fact_repo, (fcs.FactDescriptor(),))
            ),
            (),
        )

    def test_repr(self, factstore):
        aggregate = CountingAggregate(factstore)
        assert (
            repr(aggregate) == f"CountingAggregate(factstore={factstore!r}, "
            f"fact_descriptors=(FactDescriptor(Fact, ),))"
        )

    def test_aggregate_without_subscription_is_not_notified(self, factstore):
        aggregate = CountingAggregate(factstore)
        aggregate.unsubscribe()
        factstore.add_fact(Add(3))
        assert aggregate.total() == 0

    def test_aggregate_can_unsubscribe_twice(self, factstore):
        aggregate = CountingAggregate(factstore)
        aggregate.unsubscribe()
        aggregate.unsubscribe()
        factstore.add_fact(Add(3))
        assert aggregate.total() == 0
