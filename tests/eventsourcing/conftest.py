import pytest

from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.eventsourcing import messaging as msg
from streeplijst.eventsourcing import repository as rpy


@pytest.fixture(scope="module")
def fact():
    return fct.Fact()


@pytest.fixture(scope="module")
def facts():
    return tuple(fct.Fact() for _ in range(5))


@pytest.fixture
def fact_repo():
    return rpy.ListStorage()


@pytest.fixture
def factstore(fact_repo):
    return fcs.FactStore(fact_repo)


class MockSubscriber(msg.Subscriber):
    def __init__(self):
        self.logs = []

    def notify(
        self,
        subscription: msg.Subscription,
        notification: tuple,
    ) -> None:
        assert isinstance(subscription, fcs.StreamSubscription)
        assert isinstance(notification, fcs.Notification)
        facts = subscription.get_facts(notification.last_index)
        if len(facts) > 0:
            self.logs.append(facts)


def _make_subscriber():
    return MockSubscriber()


@pytest.fixture
def subscriber():
    return _make_subscriber()
