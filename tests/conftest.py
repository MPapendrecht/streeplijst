import datetime as dt
import logging
from dataclasses import asdict

import pytest

from streeplijst.configs import config as cfg
from streeplijst.domain import bank as bnk
from streeplijst.domain import payment as pay
from streeplijst.domain import product as prd
from streeplijst.domain import user as usr
from streeplijst.eventsourcing import fact as fct
from streeplijst.eventsourcing import factstore as fcs
from streeplijst.eventsourcing import repository as rpy
from streeplijst.util import identifier as idd
from streeplijst.util import secrets

logging.disable(logging.CRITICAL)


@pytest.fixture(autouse=True)
def mock_config(request, monkeypatch, tmp_path):
    if "no_mock_config" in request.keywords:
        return
    monkeypatch.setattr(cfg, "project_dir", lambda: tmp_path)
    monkeypatch.setattr(cfg, "_CONFIG", cfg.config(rebuild=True))


def pytest_collection_modifyitems(items):
    for item in items:
        if "::testint_" in item.nodeid or "::TestInt" in item.nodeid:
            item.add_marker("integtest")


def pytest_sessionfinish(session, exitstatus):
    """We are fine with no tests found if we are filtering on markers"""
    if "-m" in session.config.invocation_params.args and exitstatus == 5:
        session.exitstatus = 0


@pytest.fixture
def present_date():
    return dt.datetime(2000, 4, 5, 19, 54, 31)


@pytest.fixture
def bank():
    return bnk.Bank()


@pytest.fixture
def vault():
    return secrets.MemoryVault()


@pytest.fixture
def repository():
    return rpy.ListStorage()


@pytest.fixture
def factstore(repository):
    return fcs.FactStore(repository)


@pytest.fixture(scope="module")
def fact():
    return fct.Fact()


@pytest.fixture
def user(vault):
    return usr.User(
        identifier=idd.Identifier(),
        name="testuser",
        balance=100,
        balance_limit=0,
        age=cfg.config().users.adult_age,
        allow_negative=False,
        mail="",
        vault=vault,
    )


@pytest.fixture
def product():
    return prd.Product(
        identifier=idd.Identifier(),
        name="testproduct",
        stock=50,
        stock_limit=0,
        req_adult=False,
        price=0,
        profit=0,
    )


@pytest.fixture
def payment():
    return pay.Payment(amount=1)


@pytest.fixture
def payment_factory():
    def factory(amount):
        return pay.Payment(amount=amount)

    return factory


@pytest.fixture
def billing_config():
    config = cfg.BillingConfig("testuser", "password", "http://localhost/invoice", 60)
    cfg.extend_config({"billing": asdict(config)})
    return config
