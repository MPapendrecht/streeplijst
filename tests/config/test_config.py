from pathlib import Path

import pytest
import yaml

from streeplijst.configs import config


class TestConfig:
    def test_empty_config(self):
        with pytest.raises(config.InvalidConfig):
            config.make_config({}, {})

    def test_no_config_yaml(self, monkeypatch):
        def config_missing_open(file_path: Path):
            if file_path.name.endswith("config.yaml"):
                raise FileNotFoundError
            with open(file_path, "r") as file:
                return yaml.safe_load(file)

        monkeypatch.setattr(config, "_load_yaml", config_missing_open)
        assert config.make_config(config.load_parameters())

    def test_path_resolver(self, tmp_path):
        assert config._path_resolver(".", tmp_path) == tmp_path

    def test_path_resolver_absolute(self, tmp_path):
        assert config._path_resolver(tmp_path) == tmp_path

    def test_retrieving_config(self):
        settings = config.config()
        assert isinstance(settings, config.Config)
        assert settings is config.config()
        config._CONFIG = None
        assert settings is not config.config()

    @pytest.mark.no_mock_config
    def test_project_dir(self, tmp_path):
        assert config.project_dir() != tmp_path

    def test_init_config(self, tmp_path):
        config.init_config_file()
        assert (tmp_path / "config" / "config.yaml").exists()
