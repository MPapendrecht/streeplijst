import pytest

from streeplijst.configs.modules import payments


class TestPaymentConfig:
    def test_creating_payment_config(self):
        config = payments.PaymentsConfig(maximum_amount=0)
        assert config.maximum_amount == 0

    def test_maximum_amount_cannot_be_negative(self):
        with pytest.raises(ValueError):
            payments.PaymentsConfig(maximum_amount=-1)
