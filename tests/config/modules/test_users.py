import pytest

from streeplijst.configs.modules import users


class TestUserConfig:
    def test_creating_user_config(self):
        config = users.UsersConfig(adult_age=0, transaction_cache_size=0)
        assert config.adult_age == 0
        assert config.transaction_cache_size == 0

    def test_adult_age_cannot_be_negative(self):
        with pytest.raises(ValueError):
            users.UsersConfig(adult_age=-1, transaction_cache_size=0)

    def test_transaction_cache_size_cannot_be_negative(self):
        with pytest.raises(ValueError):
            users.UsersConfig(adult_age=1, transaction_cache_size=-1)
