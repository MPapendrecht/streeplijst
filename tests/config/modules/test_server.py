import pytest

from streeplijst.configs.modules import server


class TestServerConfig:
    def test_creating_server_config(self):
        config = server.ServerConfig(
            data_file_path=".",
            port=1,
            ip="127.0.0.1",
            vault_file_path="."
        )
        assert config.data_file_path == "."
        assert config.port == 1
        assert config.ip == "127.0.0.1"

    def test_ip_needs_digits_and_dots(self):
        kwargs = {
            "data_file_path": ".",
            "port": 1,
            "vault_file_path": ".",
        }
        with pytest.raises(TypeError):
            server.ServerConfig(ip="", **kwargs)
        with pytest.raises(TypeError):
            server.ServerConfig(ip="127.0.0.", **kwargs)
        with pytest.raises(TypeError):
            server.ServerConfig(ip="", **kwargs)

    def test_ip_numbers_below_256_raise_error(self):
        kwargs = {
            "data_file_path": ".",
            "port": 1,
            "vault_file_path": ".",
        }
        with pytest.raises(ValueError):
            server.ServerConfig(ip="256.0.0.1", **kwargs)

    def test_port_number_is_below_maximum(self):
        kwargs = {
            "data_file_path": ".",
            "ip": "127.0.0.1",
            "vault_file_path": ".",
        }
        with pytest.raises(ValueError):
            server.ServerConfig(port=65536, **kwargs)

    def test_port_number_is_positive(self):
        kwargs = {
            "data_file_path": ".",
            "ip": "127.0.0.1",
            "vault_file_path": ".",
        }
        with pytest.raises(ValueError):
            server.ServerConfig(port=-1, **kwargs)
