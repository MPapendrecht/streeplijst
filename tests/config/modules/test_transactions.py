import pytest

from streeplijst.configs.modules import transactions


class TestTransactionConfig:
    def test_create_transactions(self):
        config = transactions.TransactionsConfig(maximum_amount=0)
        assert config.maximum_amount == 0

    def test_negative_maximum_raises_error(self):
        with pytest.raises(ValueError):
            transactions.TransactionsConfig(maximum_amount=-1)
