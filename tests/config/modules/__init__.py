"""

This package contains the tests for the modules package.

"""
from tests.config.modules import (
    test_payments,
    test_server,
    test_transactions,
    test_users,
)

__all__ = [
    "test_users",
    "test_transactions",
    "test_server",
    "test_payments",
]
