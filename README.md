# Streeplijst

Een digitaal streeplijst en inventarisatie systeem voor drinken op Scouting.
Het systeem gaat uit van het positief saldo principe. 
Alle gebruikers hebben een saldo, en deze kunnen ze opwaarderen.
De gebruiker is zelf verantwoordelijk voor het overmaken van het geld naar de beheerder van het systeem.
Via het systeem kan de beheerder makkelijk een overzicht printen van (wan)betalers.
Op deze manier werkt het systeem volledig offline, en onafhankelijk van de beheerder.

# Design van de interface

De eerste interface is de gebruikers interface. 
Op deze pagina staan drie kolommen.
De eerste kolom is een lijst van namen, van welke meerdere aangevinkt kunnen worden.
De tweede kolom is een lijst van koopwaar.
Wanneer op deze geklikt wordt, wordt voor iedere geselecteerde persoon dit artikel toegevoegd aan de winkelmand.
Dit is tevens de derde kolom, waar nog kleine aanpassingen gemaakt kunnen worden.
Per koopwaar staan de namen genoteerd, met een - (min), aantal, + (plus) en delete knop. 
Via deze knoppen kunnen per persoon kleine aanpassingen gemaakt worden.
Tevens zijn negatieve getallen toegestaan, zodat verkeerd toegevoegde transacties kunnen worden gecorrigeerd.
Het systeem houdt de historie in de gaten om te voorkomen dat er meer geretourneerd wordt dan gekocht.
Onder op de pagina kan dan met een knop de winkelmand geleegd worden, of bevestigd worden.

De lijst met namen geeft ook aan wanneer iemand laag, of geen saldo meer heeft. 
Wanneer de persoon laag saldo heeft, zal er een waarschuwing komen te staan.
Wanneer de persoon geen saldo meer heeft, zal de naam niet meer te selecteren zijn.
Mocht er een persoon worden toegevoegd die bepaalde artikelen niet mag, dan worden deze artikelen niet weergegeven wanneer deze persoon is geselecteerd.

# Gebruiker administratie

Behalve strepen moeten gebruikers ook beheerd worden.
Dit gebeurd op een tweede tabblad. 
Hier kunnen gebruikers hun naam veranderen, hun saldo opwaarderen, een waarschuwingslimiet kunnen instellen, dat soort zaken.
In een kolom aan de rechter kant, hetzelfde als bij de eerste pagina, kan een naam geselecteerd worden.
Aan de linkerkant kan dan gekozen worden voor een optie. 

# Beheerders administratie (Future work)

Deze pagina is niet verwerkt binnen het interface, maar heeft een apart tabblad.
Deze pagina is beveiligd voor beheerders met een wachtwoord. 
Op deze pagina staan de actuele voorraden, kan een overzicht worden geprint van de betalingen, en kan voorraad worden aangevuld.
Het systeem corrigeerd de prijs per artikel automatisch, zodat missende voorraad wordt terugbetaald.
Dit doet het systeem door de balans per artikel te onthouden, en de prijs te veranderen om de balans terug naar 0 te brengen. 
Door beter te strepen gaat de prijs dus automatisch naar zijn laagste mogelijkheid, de inkoopprijs.
De beheerder kan een globale correctiefactor toevoegen om het eigen vermogen geleidelijk te laten groeien of dalen.

Daarnaast kan de beheerder accounts op non-actief zetten, zodat ze niet meer worden getoond. 
Dit kan alleen als het saldo van het account op 0 staat.

Op het overzicht van betalingen kan de beheerder betalingen afvinken, zodat er bekend is dat deze zijn betaald. 


Er zijn in ieder geval twee interfaces nodig: De UI en de AI (admin interface, niet artificial intelligence).

Het is een plus als de interfaces werken op desktop en mobile sized apparaten.

# Installatie

## Docker

Er is een dockerfile en docker-compose bestand beschikbaar. 
De configuratie en data zijn extern beschikbaar via docker volumes.
