PYTHON3 = python3
PIPFLAG_USER = $(shell $(PYTHON3) -c 'import sys; print ("--user" if not (hasattr(sys, "real_prefix") or sys.base_prefix != sys.prefix) else "")')
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_path := $(shell pwd)
IP ?= $(shell $(PYTHON3) -c "from streeplijst.configs.config import CONFIG; print(CONFIG.server.ip)")
PORT ?= $(shell $(PYTHON3) -c "from streeplijst.configs.config import CONFIG; print(CONFIG.server.port)")


default:
	@echo "*** Welcome to the Streeplijst project."
	@echo
	@echo "You can perform the following options:"
	@echo ""
	@echo "Utility:"
	@echo "\tdocs:  \tBuild the documentation."
	@echo ""
	@echo "Application setup:"
	@echo "\tinstall: \tInstall the application on your system."
	@echo "\tuninstall: \tRemove the application from your system."
	@echo "\tbackup: \tBackup the server data."
	@echo "\tupcast: \tUpcast all current facts to their latest version."
	@echo ""
	@echo "Developer tools:"
	@echo "\tswagger: \tRun a swagger page for all the endpoints."
	@echo "\tstatistics: \tPrint the statistics of this repository."

###################
# Utility targets #
###################

docs:
	@echo "*** Creating documentation"
	@sphinx-apidoc -q -f -M -o docs/source streeplijst
	@echo "*** Building documentation"
	@cd docs && make html SPHINXOPTS=-q
	@echo "Build documentation successfully"

.PHONY: docs

#######################
# Application targets #
#######################


init:
	@echo "Initialize config files."
	mkdir -p "$(current_path)/config/"
	cp -np "$(current_path)/src/streeplijst/configs/nginx_config.example.conf" "$(current_path)/config/nginx_config.conf"
	cp -np "$(current_path)/src/streeplijst/configs/service_config.example.service" "$(current_path)/config/service_config.service"

install: init
	@echo "Install all the application as a service"
	cp "$(current_path)/config/service_config.service" /etc/systemd/system/streeplijst.service
	systemctl daemon-reload
	@echo "Link NGINX config files"
	- ln -s "$(current_path)/config/nginx_config.conf" /etc/nginx/sites-enabled/streeplijst.conf
	@echo "Starting streeplijst service"
	systemctl start streeplijst.service
	systemctl enable streeplijst.service
	@echo "Restarting NGINX"
	nginx -t
	nginx -s reload

uninstall:
	@echo "Uninstalling and removing links"
	- systemctl stop streeplijst.service
	- rm /etc/systemd/system/streeplijst.service
	- unlink /etc/nginx/sites-enabled/streeplijst.conf
	- nginx -s reload
	- systemctl daemon-reload


.PHONY: install, uninstall, backup, upcast

###################
# Developer tools #
###################

swagger:
	@echo "***Running swagger via docker."
	docker run -e URL='http://127.0.0.1:8000/openapi.json' --network host swaggerapi/swagger-ui
	open localhost:8080

statistics:
	@echo "Streeplijst"
	@echo "\tLines of code: `find ./streeplijst -name '*.py' | xargs wc -l | grep -Po '(\d*)(?= total)'`"
	@echo "\tNumber of files: `find ./streeplijst -name '*.py' -printf '.' | wc -m`"
	@echo ''
	@echo "Tests"
	@echo "\tNumber of tests: `$(PYTHON3) -m pytest --collect-only -q | grep -Po '(\d*)(?= tests collected)'`"
	@echo "\tLines of code: `find ./tests -name '*.py' | xargs wc -l | grep -Po '(\d*)(?= total)'`"
	@echo "\tNumber of files: `find ./tests -name '*.py' -printf '.' | wc -m`"

.PHONY: swagger statistics
